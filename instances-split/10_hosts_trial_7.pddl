(define (problem p10_hosts_trial_7) (:domain CALDERA)
(:objects
	ID_CFTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BYTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CMTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DOTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ECTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CTTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DATIMEDELTA - OBSERVEDTIMEDELTA
	ID_DHTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DVTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ETFILE - OBSERVEDFILE
	ID_ENFILE - OBSERVEDFILE
	ID_ESFILE - OBSERVEDFILE
	ID_EKFILE - OBSERVEDFILE
	ID_EQFILE - OBSERVEDFILE
	ID_EOFILE - OBSERVEDFILE
	ID_ERFILE - OBSERVEDFILE
	ID_EPFILE - OBSERVEDFILE
	ID_EMFILE - OBSERVEDFILE
	ID_ELFILE - OBSERVEDFILE
	NUM__80 - NUM
	NUM__58 - NUM
	NUM__94 - NUM
	NUM__65 - NUM
	NUM__86 - NUM
	NUM__59 - NUM
	NUM__108 - NUM
	NUM__93 - NUM
	NUM__107 - NUM
	NUM__45 - NUM
	NUM__51 - NUM
	NUM__52 - NUM
	NUM__66 - NUM
	NUM__73 - NUM
	NUM__79 - NUM
	NUM__87 - NUM
	NUM__101 - NUM
	NUM__100 - NUM
	NUM__44 - NUM
	NUM__72 - NUM
	ID_FBSHARE - OBSERVEDSHARE
	ID_EYSHARE - OBSERVEDSHARE
	ID_EZSHARE - OBSERVEDSHARE
	ID_FASHARE - OBSERVEDSHARE
	ID_FCSHARE - OBSERVEDSHARE
	ID_EVSHARE - OBSERVEDSHARE
	ID_EWSHARE - OBSERVEDSHARE
	ID_FDSHARE - OBSERVEDSHARE
	ID_EUSHARE - OBSERVEDSHARE
	ID_EXSHARE - OBSERVEDSHARE
	ID_FLRAT - OBSERVEDRAT
	ID_FKRAT - OBSERVEDRAT
	ID_FMRAT - OBSERVEDRAT
	ID_FERAT - OBSERVEDRAT
	ID_FGRAT - OBSERVEDRAT
	ID_FIRAT - OBSERVEDRAT
	ID_FHRAT - OBSERVEDRAT
	ID_EIRAT - OBSERVEDRAT
	ID_FFRAT - OBSERVEDRAT
	ID_FJRAT - OBSERVEDRAT
	ID_FNRAT - OBSERVEDRAT
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CSHOST - OBSERVEDHOST
	ID_BQHOST - OBSERVEDHOST
	ID_DGHOST - OBSERVEDHOST
	ID_CLHOST - OBSERVEDHOST
	ID_DUHOST - OBSERVEDHOST
	ID_CZHOST - OBSERVEDHOST
	ID_DNHOST - OBSERVEDHOST
	ID_CEHOST - OBSERVEDHOST
	ID_EBHOST - OBSERVEDHOST
	ID_BXHOST - OBSERVEDHOST
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FUSCHTASK - OBSERVEDSCHTASK
	ID_FPSCHTASK - OBSERVEDSCHTASK
	ID_FQSCHTASK - OBSERVEDSCHTASK
	ID_FOSCHTASK - OBSERVEDSCHTASK
	ID_FVSCHTASK - OBSERVEDSCHTASK
	ID_FRSCHTASK - OBSERVEDSCHTASK
	ID_FTSCHTASK - OBSERVEDSCHTASK
	ID_FSSCHTASK - OBSERVEDSCHTASK
	ID_FWSCHTASK - OBSERVEDSCHTASK
	ID_FXSCHTASK - OBSERVEDSCHTASK
	ID_ADOMAIN - OBSERVEDDOMAIN
	STR__DS - STRING
	STR__E - STRING
	STR__BW - STRING
	STR__DL - STRING
	STR__ROBERT - STRING
	STR__BO - STRING
	STR__BK - STRING
	STR__DE - STRING
	STR__I - STRING
	STR__BG - STRING
	STR__U - STRING
	STR__DT - STRING
	STR__CX - STRING
	STR__EA - STRING
	STR__Z - STRING
	STR__Q - STRING
	STR__PATRICIA - STRING
	STR__CB - STRING
	STR__M - STRING
	STR__DY - STRING
	STR__BP - STRING
	STR__CD - STRING
	STR__CQ - STRING
	STR__CW - STRING
	STR__LINDA - STRING
	STR__BC - STRING
	STR__DK - STRING
	STR__BARBARA - STRING
	STR__Y - STRING
	STR__BU - STRING
	STR__EH - STRING
	STR__J - STRING
	STR__WILLIAM - STRING
	STR__BV - STRING
	STR__MICHAEL - STRING
	STR__ALPHA - STRING
	STR__CC - STRING
	STR__V - STRING
	STR__ELIZABETH - STRING
	STR__BD - STRING
	STR__DZ - STRING
	STR__CY - STRING
	STR__F - STRING
	STR__N - STRING
	STR__CI - STRING
	STR__BL - STRING
	STR__EF - STRING
	STR__JOHN - STRING
	STR__CJ - STRING
	STR__EG - STRING
	STR__MARY - STRING
	STR__BH - STRING
	STR__R - STRING
	STR__EJ - STRING
	STR__JAMES - STRING
	STR__DM - STRING
	STR__CP - STRING
	STR__CK - STRING
	STR__B - STRING
	STR__CR - STRING
	STR__DF - STRING
	STR__DR - STRING
	STR__DD - STRING
)
(:init
	(KNOWS ID_DGHOST)
	(KNOWS ID_EIRAT)
	(KNOWS_PROPERTY ID_DGHOST PFQDN)
	(KNOWS_PROPERTY ID_EIRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_EIRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_TDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_KDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BQHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BXHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CEHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CLHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CSHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CZHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DGHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DNHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DUHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EBHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_BQHOST NO)
	(PROP_DC ID_BXHOST NO)
	(PROP_DC ID_CEHOST NO)
	(PROP_DC ID_CLHOST NO)
	(PROP_DC ID_CSHOST YES)
	(PROP_DC ID_CZHOST NO)
	(PROP_DC ID_DGHOST NO)
	(PROP_DC ID_DNHOST NO)
	(PROP_DC ID_DUHOST NO)
	(PROP_DC ID_EBHOST YES)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BQHOST STR__BU)
	(PROP_DNS_DOMAIN_NAME ID_BXHOST STR__CB)
	(PROP_DNS_DOMAIN_NAME ID_CEHOST STR__CI)
	(PROP_DNS_DOMAIN_NAME ID_CLHOST STR__CP)
	(PROP_DNS_DOMAIN_NAME ID_CSHOST STR__CW)
	(PROP_DNS_DOMAIN_NAME ID_CZHOST STR__DD)
	(PROP_DNS_DOMAIN_NAME ID_DGHOST STR__DK)
	(PROP_DNS_DOMAIN_NAME ID_DNHOST STR__DR)
	(PROP_DNS_DOMAIN_NAME ID_DUHOST STR__DY)
	(PROP_DNS_DOMAIN_NAME ID_EBHOST STR__EF)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BXHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CEHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CLHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CSHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CZHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DGHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DNHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DUHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_EIRAT YES)
	(PROP_EXECUTABLE ID_EIRAT STR__EJ)
	(PROP_FQDN ID_BQHOST STR__BV)
	(PROP_FQDN ID_BXHOST STR__CC)
	(PROP_FQDN ID_CEHOST STR__CJ)
	(PROP_FQDN ID_CLHOST STR__CQ)
	(PROP_FQDN ID_CSHOST STR__CX)
	(PROP_FQDN ID_CZHOST STR__DE)
	(PROP_FQDN ID_DGHOST STR__DL)
	(PROP_FQDN ID_DNHOST STR__DS)
	(PROP_FQDN ID_DUHOST STR__DZ)
	(PROP_FQDN ID_EBHOST STR__EG)
	(PROP_HOST ID_BRTIMEDELTA ID_BQHOST)
	(PROP_HOST ID_BYTIMEDELTA ID_BXHOST)
	(PROP_HOST ID_CFTIMEDELTA ID_CEHOST)
	(PROP_HOST ID_CMTIMEDELTA ID_CLHOST)
	(PROP_HOST ID_CTTIMEDELTA ID_CSHOST)
	(PROP_HOST ID_DATIMEDELTA ID_CZHOST)
	(PROP_HOST ID_DHTIMEDELTA ID_DGHOST)
	(PROP_HOST ID_DOTIMEDELTA ID_DNHOST)
	(PROP_HOST ID_DVTIMEDELTA ID_DUHOST)
	(PROP_HOST ID_ECTIMEDELTA ID_EBHOST)
	(PROP_HOST ID_EIRAT ID_DGHOST)
	(PROP_HOSTNAME ID_BQHOST STR__BW)
	(PROP_HOSTNAME ID_BXHOST STR__CD)
	(PROP_HOSTNAME ID_CEHOST STR__CK)
	(PROP_HOSTNAME ID_CLHOST STR__CR)
	(PROP_HOSTNAME ID_CSHOST STR__CY)
	(PROP_HOSTNAME ID_CZHOST STR__DF)
	(PROP_HOSTNAME ID_DGHOST STR__DM)
	(PROP_HOSTNAME ID_DNHOST STR__DT)
	(PROP_HOSTNAME ID_DUHOST STR__EA)
	(PROP_HOSTNAME ID_EBHOST STR__EH)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BRTIMEDELTA NUM__45)
	(PROP_MICROSECONDS ID_BYTIMEDELTA NUM__52)
	(PROP_MICROSECONDS ID_CFTIMEDELTA NUM__59)
	(PROP_MICROSECONDS ID_CMTIMEDELTA NUM__66)
	(PROP_MICROSECONDS ID_CTTIMEDELTA NUM__73)
	(PROP_MICROSECONDS ID_DATIMEDELTA NUM__80)
	(PROP_MICROSECONDS ID_DHTIMEDELTA NUM__87)
	(PROP_MICROSECONDS ID_DOTIMEDELTA NUM__94)
	(PROP_MICROSECONDS ID_DVTIMEDELTA NUM__101)
	(PROP_MICROSECONDS ID_ECTIMEDELTA NUM__108)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_BRTIMEDELTA NUM__44)
	(PROP_SECONDS ID_BYTIMEDELTA NUM__51)
	(PROP_SECONDS ID_CFTIMEDELTA NUM__58)
	(PROP_SECONDS ID_CMTIMEDELTA NUM__65)
	(PROP_SECONDS ID_CTTIMEDELTA NUM__72)
	(PROP_SECONDS ID_DATIMEDELTA NUM__79)
	(PROP_SECONDS ID_DHTIMEDELTA NUM__86)
	(PROP_SECONDS ID_DOTIMEDELTA NUM__93)
	(PROP_SECONDS ID_DVTIMEDELTA NUM__100)
	(PROP_SECONDS ID_ECTIMEDELTA NUM__107)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_BQHOST ID_BRTIMEDELTA)
	(PROP_TIMEDELTA ID_BXHOST ID_BYTIMEDELTA)
	(PROP_TIMEDELTA ID_CEHOST ID_CFTIMEDELTA)
	(PROP_TIMEDELTA ID_CLHOST ID_CMTIMEDELTA)
	(PROP_TIMEDELTA ID_CSHOST ID_CTTIMEDELTA)
	(PROP_TIMEDELTA ID_CZHOST ID_DATIMEDELTA)
	(PROP_TIMEDELTA ID_DGHOST ID_DHTIMEDELTA)
	(PROP_TIMEDELTA ID_DNHOST ID_DOTIMEDELTA)
	(PROP_TIMEDELTA ID_DUHOST ID_DVTIMEDELTA)
	(PROP_TIMEDELTA ID_EBHOST ID_ECTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_FFRAT ID_EBHOST)(PROP_HOST ID_FHRAT ID_CEHOST)(PROP_HOST ID_FIRAT ID_DNHOST)(PROP_HOST ID_FGRAT ID_CZHOST)(PROP_HOST ID_FERAT ID_DUHOST)(PROP_HOST ID_FMRAT ID_CLHOST)(PROP_HOST ID_FKRAT ID_BQHOST)(PROP_HOST ID_FLRAT ID_CSHOST)(PROP_HOST ID_FNRAT ID_BXHOST)))
(:metric minimize (total-cost))

)
