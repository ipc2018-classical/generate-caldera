(define (problem p18_hosts_trial_6) (:domain CALDERA)
(:objects
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FHTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DKTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EFTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ETTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CITIMEDELTA - OBSERVEDTIMEDELTA
	ID_FATIMEDELTA - OBSERVEDTIMEDELTA
	ID_CBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FVTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FOTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GCTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EMTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DYTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CWTIMEDELTA - OBSERVEDTIMEDELTA
	NUM__55 - NUM
	NUM__118 - NUM
	NUM__111 - NUM
	NUM__124 - NUM
	NUM__82 - NUM
	NUM__131 - NUM
	NUM__40 - NUM
	NUM__132 - NUM
	NUM__153 - NUM
	NUM__103 - NUM
	NUM__97 - NUM
	NUM__117 - NUM
	NUM__48 - NUM
	NUM__145 - NUM
	NUM__146 - NUM
	NUM__47 - NUM
	NUM__138 - NUM
	NUM__152 - NUM
	NUM__159 - NUM
	NUM__160 - NUM
	NUM__83 - NUM
	NUM__68 - NUM
	NUM__110 - NUM
	NUM__62 - NUM
	NUM__125 - NUM
	NUM__90 - NUM
	NUM__69 - NUM
	NUM__76 - NUM
	NUM__61 - NUM
	NUM__89 - NUM
	NUM__54 - NUM
	NUM__104 - NUM
	NUM__139 - NUM
	NUM__41 - NUM
	NUM__75 - NUM
	NUM__96 - NUM
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_IDSCHTASK - OBSERVEDSCHTASK
	ID_IFSCHTASK - OBSERVEDSCHTASK
	ID_HYSCHTASK - OBSERVEDSCHTASK
	ID_ILSCHTASK - OBSERVEDSCHTASK
	ID_IASCHTASK - OBSERVEDSCHTASK
	ID_IJSCHTASK - OBSERVEDSCHTASK
	ID_IESCHTASK - OBSERVEDSCHTASK
	ID_IGSCHTASK - OBSERVEDSCHTASK
	ID_IKSCHTASK - OBSERVEDSCHTASK
	ID_ICSCHTASK - OBSERVEDSCHTASK
	ID_HXSCHTASK - OBSERVEDSCHTASK
	ID_HUSCHTASK - OBSERVEDSCHTASK
	ID_IHSCHTASK - OBSERVEDSCHTASK
	ID_IISCHTASK - OBSERVEDSCHTASK
	ID_HWSCHTASK - OBSERVEDSCHTASK
	ID_HZSCHTASK - OBSERVEDSCHTASK
	ID_IBSCHTASK - OBSERVEDSCHTASK
	ID_HVSCHTASK - OBSERVEDSCHTASK
	ID_GZFILE - OBSERVEDFILE
	ID_HAFILE - OBSERVEDFILE
	ID_GPFILE - OBSERVEDFILE
	ID_GLFILE - OBSERVEDFILE
	ID_GQFILE - OBSERVEDFILE
	ID_GRFILE - OBSERVEDFILE
	ID_GOFILE - OBSERVEDFILE
	ID_GMFILE - OBSERVEDFILE
	ID_GWFILE - OBSERVEDFILE
	ID_GKFILE - OBSERVEDFILE
	ID_GYFILE - OBSERVEDFILE
	ID_GSFILE - OBSERVEDFILE
	ID_GVFILE - OBSERVEDFILE
	ID_HBFILE - OBSERVEDFILE
	ID_GUFILE - OBSERVEDFILE
	ID_GNFILE - OBSERVEDFILE
	ID_GXFILE - OBSERVEDFILE
	ID_GTFILE - OBSERVEDFILE
	STR__EC - STRING
	STR__Z - STRING
	STR__E - STRING
	STR__BS - STRING
	STR__CF - STRING
	STR__FY - STRING
	STR__GG - STRING
	STR__ROBERT - STRING
	STR__FK - STRING
	STR__CM - STRING
	STR__DW - STRING
	STR__BC - STRING
	STR__FR - STRING
	STR__GH - STRING
	STR__DN - STRING
	STR__EB - STRING
	STR__FZ - STRING
	STR__BD - STRING
	STR__CZ - STRING
	STR__F - STRING
	STR__FD - STRING
	STR__J - STRING
	STR__JOHN - STRING
	STR__ER - STRING
	STR__Y - STRING
	STR__EK - STRING
	STR__FS - STRING
	STR__EI - STRING
	STR__CT - STRING
	STR__DO - STRING
	STR__JAMES - STRING
	STR__V - STRING
	STR__CE - STRING
	STR__CU - STRING
	STR__EW - STRING
	STR__BQ - STRING
	STR__BZ - STRING
	STR__PATRICIA - STRING
	STR__GF - STRING
	STR__DA - STRING
	STR__FE - STRING
	STR__DB - STRING
	STR__B - STRING
	STR__GA - STRING
	STR__DH - STRING
	STR__N - STRING
	STR__BL - STRING
	STR__BX - STRING
	STR__DG - STRING
	STR__MARY - STRING
	STR__ALPHA - STRING
	STR__R - STRING
	STR__M - STRING
	STR__U - STRING
	STR__BK - STRING
	STR__CL - STRING
	STR__DI - STRING
	STR__CS - STRING
	STR__DU - STRING
	STR__I - STRING
	STR__CG - STRING
	STR__FL - STRING
	STR__FT - STRING
	STR__BARBARA - STRING
	STR__EJ - STRING
	STR__FM - STRING
	STR__EQ - STRING
	STR__BR - STRING
	STR__EY - STRING
	STR__LINDA - STRING
	STR__MICHAEL - STRING
	STR__DV - STRING
	STR__GJ - STRING
	STR__BG - STRING
	STR__Q - STRING
	STR__CN - STRING
	STR__DP - STRING
	STR__EP - STRING
	STR__EX - STRING
	STR__BH - STRING
	STR__FF - STRING
	STR__WILLIAM - STRING
	STR__ED - STRING
	STR__BY - STRING
	ID_HQSHARE - OBSERVEDSHARE
	ID_HFSHARE - OBSERVEDSHARE
	ID_HTSHARE - OBSERVEDSHARE
	ID_HESHARE - OBSERVEDSHARE
	ID_HHSHARE - OBSERVEDSHARE
	ID_HMSHARE - OBSERVEDSHARE
	ID_HJSHARE - OBSERVEDSHARE
	ID_HPSHARE - OBSERVEDSHARE
	ID_HISHARE - OBSERVEDSHARE
	ID_HLSHARE - OBSERVEDSHARE
	ID_HOSHARE - OBSERVEDSHARE
	ID_HNSHARE - OBSERVEDSHARE
	ID_HCSHARE - OBSERVEDSHARE
	ID_HGSHARE - OBSERVEDSHARE
	ID_HDSHARE - OBSERVEDSHARE
	ID_HKSHARE - OBSERVEDSHARE
	ID_HRSHARE - OBSERVEDSHARE
	ID_HSSHARE - OBSERVEDSHARE
	ID_FUHOST - OBSERVEDHOST
	ID_DJHOST - OBSERVEDHOST
	ID_CVHOST - OBSERVEDHOST
	ID_CAHOST - OBSERVEDHOST
	ID_DQHOST - OBSERVEDHOST
	ID_ESHOST - OBSERVEDHOST
	ID_EZHOST - OBSERVEDHOST
	ID_FGHOST - OBSERVEDHOST
	ID_BTHOST - OBSERVEDHOST
	ID_FNHOST - OBSERVEDHOST
	ID_ELHOST - OBSERVEDHOST
	ID_GBHOST - OBSERVEDHOST
	ID_COHOST - OBSERVEDHOST
	ID_BMHOST - OBSERVEDHOST
	ID_EEHOST - OBSERVEDHOST
	ID_CHHOST - OBSERVEDHOST
	ID_DXHOST - OBSERVEDHOST
	ID_DCHOST - OBSERVEDHOST
	ID_JARAT - OBSERVEDRAT
	ID_IVRAT - OBSERVEDRAT
	ID_IZRAT - OBSERVEDRAT
	ID_IORAT - OBSERVEDRAT
	ID_INRAT - OBSERVEDRAT
	ID_IURAT - OBSERVEDRAT
	ID_IXRAT - OBSERVEDRAT
	ID_IWRAT - OBSERVEDRAT
	ID_IYRAT - OBSERVEDRAT
	ID_IPRAT - OBSERVEDRAT
	ID_JCRAT - OBSERVEDRAT
	ID_IMRAT - OBSERVEDRAT
	ID_ITRAT - OBSERVEDRAT
	ID_JBRAT - OBSERVEDRAT
	ID_IRRAT - OBSERVEDRAT
	ID_GIRAT - OBSERVEDRAT
	ID_JDRAT - OBSERVEDRAT
	ID_ISRAT - OBSERVEDRAT
	ID_IQRAT - OBSERVEDRAT
)
(:init
	(KNOWS ID_BTHOST)
	(KNOWS ID_GIRAT)
	(KNOWS_PROPERTY ID_BTHOST PFQDN)
	(KNOWS_PROPERTY ID_GIRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_GIRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DQHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DQHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DQHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DQHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DXHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DXHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DXHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DXHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EEHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EEHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EEHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EEHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ELHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ELHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ELHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ELHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ESHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ESHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ESHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ESHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EZHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EZHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EZHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EZHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FGHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FGHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FGHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FGHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FNHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FNHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FNHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FNHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FUHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FUHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FUHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FUHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GBHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GBHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GBHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GBHOST ID_XDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DQHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DQHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DQHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DQHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DXHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DXHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DXHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DXHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EEHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EEHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EEHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EEHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ELHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ELHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ELHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ELHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ESHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ESHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ESHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ESHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EZHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EZHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EZHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EZHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FGHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FGHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FGHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FGHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FNHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FNHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FNHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FNHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FUHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FUHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FUHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FUHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GBHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GBHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GBHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GBHOST ID_SDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_COHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DCHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DJHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DQHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DXHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EEHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ELHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ESHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EZHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FGHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FNHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FUHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GBHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_BMHOST NO)
	(PROP_DC ID_BTHOST NO)
	(PROP_DC ID_CAHOST NO)
	(PROP_DC ID_CHHOST NO)
	(PROP_DC ID_COHOST NO)
	(PROP_DC ID_CVHOST NO)
	(PROP_DC ID_DCHOST NO)
	(PROP_DC ID_DJHOST NO)
	(PROP_DC ID_DQHOST YES)
	(PROP_DC ID_DXHOST NO)
	(PROP_DC ID_EEHOST NO)
	(PROP_DC ID_ELHOST NO)
	(PROP_DC ID_ESHOST NO)
	(PROP_DC ID_EZHOST NO)
	(PROP_DC ID_FGHOST YES)
	(PROP_DC ID_FNHOST NO)
	(PROP_DC ID_FUHOST NO)
	(PROP_DC ID_GBHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BMHOST STR__BQ)
	(PROP_DNS_DOMAIN_NAME ID_BTHOST STR__BX)
	(PROP_DNS_DOMAIN_NAME ID_CAHOST STR__CE)
	(PROP_DNS_DOMAIN_NAME ID_CHHOST STR__CL)
	(PROP_DNS_DOMAIN_NAME ID_COHOST STR__CS)
	(PROP_DNS_DOMAIN_NAME ID_CVHOST STR__CZ)
	(PROP_DNS_DOMAIN_NAME ID_DCHOST STR__DG)
	(PROP_DNS_DOMAIN_NAME ID_DJHOST STR__DN)
	(PROP_DNS_DOMAIN_NAME ID_DQHOST STR__DU)
	(PROP_DNS_DOMAIN_NAME ID_DXHOST STR__EB)
	(PROP_DNS_DOMAIN_NAME ID_EEHOST STR__EI)
	(PROP_DNS_DOMAIN_NAME ID_ELHOST STR__EP)
	(PROP_DNS_DOMAIN_NAME ID_ESHOST STR__EW)
	(PROP_DNS_DOMAIN_NAME ID_EZHOST STR__FD)
	(PROP_DNS_DOMAIN_NAME ID_FGHOST STR__FK)
	(PROP_DNS_DOMAIN_NAME ID_FNHOST STR__FR)
	(PROP_DNS_DOMAIN_NAME ID_FUHOST STR__FY)
	(PROP_DNS_DOMAIN_NAME ID_GBHOST STR__GF)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_COHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DCHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DJHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DXHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EEHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ELHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ESHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EZHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FGHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FNHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FUHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_GIRAT YES)
	(PROP_EXECUTABLE ID_GIRAT STR__GJ)
	(PROP_FQDN ID_BMHOST STR__BS)
	(PROP_FQDN ID_BTHOST STR__BZ)
	(PROP_FQDN ID_CAHOST STR__CG)
	(PROP_FQDN ID_CHHOST STR__CN)
	(PROP_FQDN ID_COHOST STR__CU)
	(PROP_FQDN ID_CVHOST STR__DB)
	(PROP_FQDN ID_DCHOST STR__DI)
	(PROP_FQDN ID_DJHOST STR__DP)
	(PROP_FQDN ID_DQHOST STR__DW)
	(PROP_FQDN ID_DXHOST STR__ED)
	(PROP_FQDN ID_EEHOST STR__EK)
	(PROP_FQDN ID_ELHOST STR__ER)
	(PROP_FQDN ID_ESHOST STR__EY)
	(PROP_FQDN ID_EZHOST STR__FF)
	(PROP_FQDN ID_FGHOST STR__FM)
	(PROP_FQDN ID_FNHOST STR__FT)
	(PROP_FQDN ID_FUHOST STR__GA)
	(PROP_FQDN ID_GBHOST STR__GH)
	(PROP_HOST ID_BNTIMEDELTA ID_BMHOST)
	(PROP_HOST ID_BUTIMEDELTA ID_BTHOST)
	(PROP_HOST ID_CBTIMEDELTA ID_CAHOST)
	(PROP_HOST ID_CITIMEDELTA ID_CHHOST)
	(PROP_HOST ID_CPTIMEDELTA ID_COHOST)
	(PROP_HOST ID_CWTIMEDELTA ID_CVHOST)
	(PROP_HOST ID_DDTIMEDELTA ID_DCHOST)
	(PROP_HOST ID_DKTIMEDELTA ID_DJHOST)
	(PROP_HOST ID_DRTIMEDELTA ID_DQHOST)
	(PROP_HOST ID_DYTIMEDELTA ID_DXHOST)
	(PROP_HOST ID_EFTIMEDELTA ID_EEHOST)
	(PROP_HOST ID_EMTIMEDELTA ID_ELHOST)
	(PROP_HOST ID_ETTIMEDELTA ID_ESHOST)
	(PROP_HOST ID_FATIMEDELTA ID_EZHOST)
	(PROP_HOST ID_FHTIMEDELTA ID_FGHOST)
	(PROP_HOST ID_FOTIMEDELTA ID_FNHOST)
	(PROP_HOST ID_FVTIMEDELTA ID_FUHOST)
	(PROP_HOST ID_GCTIMEDELTA ID_GBHOST)
	(PROP_HOST ID_GIRAT ID_BTHOST)
	(PROP_HOSTNAME ID_BMHOST STR__BR)
	(PROP_HOSTNAME ID_BTHOST STR__BY)
	(PROP_HOSTNAME ID_CAHOST STR__CF)
	(PROP_HOSTNAME ID_CHHOST STR__CM)
	(PROP_HOSTNAME ID_COHOST STR__CT)
	(PROP_HOSTNAME ID_CVHOST STR__DA)
	(PROP_HOSTNAME ID_DCHOST STR__DH)
	(PROP_HOSTNAME ID_DJHOST STR__DO)
	(PROP_HOSTNAME ID_DQHOST STR__DV)
	(PROP_HOSTNAME ID_DXHOST STR__EC)
	(PROP_HOSTNAME ID_EEHOST STR__EJ)
	(PROP_HOSTNAME ID_ELHOST STR__EQ)
	(PROP_HOSTNAME ID_ESHOST STR__EX)
	(PROP_HOSTNAME ID_EZHOST STR__FE)
	(PROP_HOSTNAME ID_FGHOST STR__FL)
	(PROP_HOSTNAME ID_FNHOST STR__FS)
	(PROP_HOSTNAME ID_FUHOST STR__FZ)
	(PROP_HOSTNAME ID_GBHOST STR__GG)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BNTIMEDELTA NUM__41)
	(PROP_MICROSECONDS ID_BUTIMEDELTA NUM__48)
	(PROP_MICROSECONDS ID_CBTIMEDELTA NUM__55)
	(PROP_MICROSECONDS ID_CITIMEDELTA NUM__62)
	(PROP_MICROSECONDS ID_CPTIMEDELTA NUM__69)
	(PROP_MICROSECONDS ID_CWTIMEDELTA NUM__76)
	(PROP_MICROSECONDS ID_DDTIMEDELTA NUM__83)
	(PROP_MICROSECONDS ID_DKTIMEDELTA NUM__90)
	(PROP_MICROSECONDS ID_DRTIMEDELTA NUM__97)
	(PROP_MICROSECONDS ID_DYTIMEDELTA NUM__104)
	(PROP_MICROSECONDS ID_EFTIMEDELTA NUM__111)
	(PROP_MICROSECONDS ID_EMTIMEDELTA NUM__118)
	(PROP_MICROSECONDS ID_ETTIMEDELTA NUM__125)
	(PROP_MICROSECONDS ID_FATIMEDELTA NUM__132)
	(PROP_MICROSECONDS ID_FHTIMEDELTA NUM__139)
	(PROP_MICROSECONDS ID_FOTIMEDELTA NUM__146)
	(PROP_MICROSECONDS ID_FVTIMEDELTA NUM__153)
	(PROP_MICROSECONDS ID_GCTIMEDELTA NUM__160)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_BNTIMEDELTA NUM__40)
	(PROP_SECONDS ID_BUTIMEDELTA NUM__47)
	(PROP_SECONDS ID_CBTIMEDELTA NUM__54)
	(PROP_SECONDS ID_CITIMEDELTA NUM__61)
	(PROP_SECONDS ID_CPTIMEDELTA NUM__68)
	(PROP_SECONDS ID_CWTIMEDELTA NUM__75)
	(PROP_SECONDS ID_DDTIMEDELTA NUM__82)
	(PROP_SECONDS ID_DKTIMEDELTA NUM__89)
	(PROP_SECONDS ID_DRTIMEDELTA NUM__96)
	(PROP_SECONDS ID_DYTIMEDELTA NUM__103)
	(PROP_SECONDS ID_EFTIMEDELTA NUM__110)
	(PROP_SECONDS ID_EMTIMEDELTA NUM__117)
	(PROP_SECONDS ID_ETTIMEDELTA NUM__124)
	(PROP_SECONDS ID_FATIMEDELTA NUM__131)
	(PROP_SECONDS ID_FHTIMEDELTA NUM__138)
	(PROP_SECONDS ID_FOTIMEDELTA NUM__145)
	(PROP_SECONDS ID_FVTIMEDELTA NUM__152)
	(PROP_SECONDS ID_GCTIMEDELTA NUM__159)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_BMHOST ID_BNTIMEDELTA)
	(PROP_TIMEDELTA ID_BTHOST ID_BUTIMEDELTA)
	(PROP_TIMEDELTA ID_CAHOST ID_CBTIMEDELTA)
	(PROP_TIMEDELTA ID_CHHOST ID_CITIMEDELTA)
	(PROP_TIMEDELTA ID_COHOST ID_CPTIMEDELTA)
	(PROP_TIMEDELTA ID_CVHOST ID_CWTIMEDELTA)
	(PROP_TIMEDELTA ID_DCHOST ID_DDTIMEDELTA)
	(PROP_TIMEDELTA ID_DJHOST ID_DKTIMEDELTA)
	(PROP_TIMEDELTA ID_DQHOST ID_DRTIMEDELTA)
	(PROP_TIMEDELTA ID_DXHOST ID_DYTIMEDELTA)
	(PROP_TIMEDELTA ID_EEHOST ID_EFTIMEDELTA)
	(PROP_TIMEDELTA ID_ELHOST ID_EMTIMEDELTA)
	(PROP_TIMEDELTA ID_ESHOST ID_ETTIMEDELTA)
	(PROP_TIMEDELTA ID_EZHOST ID_FATIMEDELTA)
	(PROP_TIMEDELTA ID_FGHOST ID_FHTIMEDELTA)
	(PROP_TIMEDELTA ID_FNHOST ID_FOTIMEDELTA)
	(PROP_TIMEDELTA ID_FUHOST ID_FVTIMEDELTA)
	(PROP_TIMEDELTA ID_GBHOST ID_GCTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_JBRAT ID_DCHOST)(PROP_HOST ID_IYRAT ID_FNHOST)(PROP_HOST ID_IWRAT ID_EZHOST)(PROP_HOST ID_IXRAT ID_CAHOST)(PROP_HOST ID_INRAT ID_CVHOST)(PROP_HOST ID_IORAT ID_DJHOST)(PROP_HOST ID_IZRAT ID_CHHOST)(PROP_HOST ID_IVRAT ID_EEHOST)(PROP_HOST ID_IQRAT ID_GBHOST)(PROP_HOST ID_ISRAT ID_BMHOST)(PROP_HOST ID_ITRAT ID_COHOST)(PROP_HOST ID_IMRAT ID_ELHOST)(PROP_HOST ID_IPRAT ID_FGHOST)(PROP_HOST ID_JCRAT ID_ESHOST)(PROP_HOST ID_IURAT ID_DQHOST)(PROP_HOST ID_JARAT ID_FUHOST)(PROP_HOST ID_IRRAT ID_DXHOST)))
(:metric minimize (total-cost))

)
