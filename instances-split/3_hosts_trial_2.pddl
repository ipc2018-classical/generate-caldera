(define (problem p3_hosts_trial_2) (:domain CALDERA)
(:objects
	STR__I - STRING
	STR__J - STRING
	STR__B - STRING
	STR__Q - STRING
	STR__P - STRING
	STR__W - STRING
	STR__MARY - STRING
	STR__ALPHA - STRING
	STR__BC - STRING
	STR__BG - STRING
	STR__JAMES - STRING
	STR__BE - STRING
	STR__X - STRING
	STR__V - STRING
	STR__O - STRING
	STR__E - STRING
	STR__F - STRING
	STR__BD - STRING
	ID_YHOST - OBSERVEDHOST
	ID_KHOST - OBSERVEDHOST
	ID_RHOST - OBSERVEDHOST
	ID_BOFILE - OBSERVEDFILE
	ID_BNFILE - OBSERVEDFILE
	ID_BPFILE - OBSERVEDFILE
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	NUM__20 - NUM
	NUM__19 - NUM
	NUM__13 - NUM
	NUM__27 - NUM
	NUM__12 - NUM
	NUM__26 - NUM
	ID_BHSCHTASK - OBSERVEDSCHTASK
	ID_BJSCHTASK - OBSERVEDSCHTASK
	ID_BISCHTASK - OBSERVEDSCHTASK
	ID_BKSHARE - OBSERVEDSHARE
	ID_BLSHARE - OBSERVEDSHARE
	ID_BMSHARE - OBSERVEDSHARE
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_BFRAT - OBSERVEDRAT
	ID_BSRAT - OBSERVEDRAT
	ID_BRRAT - OBSERVEDRAT
	ID_BQRAT - OBSERVEDRAT
	ID_ZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_STIMEDELTA - OBSERVEDTIMEDELTA
	ID_LTIMEDELTA - OBSERVEDTIMEDELTA
)
(:init
	(KNOWS ID_BFRAT)
	(KNOWS ID_RHOST)
	(KNOWS_PROPERTY ID_BFRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_BFRAT PHOST)
	(KNOWS_PROPERTY ID_RHOST PFQDN)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_GDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_KHOST)
	(MEM_HOSTS ID_ADOMAIN ID_RHOST)
	(MEM_HOSTS ID_ADOMAIN ID_YHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_DC ID_KHOST YES)
	(PROP_DC ID_RHOST NO)
	(PROP_DC ID_YHOST YES)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_KHOST STR__Q)
	(PROP_DNS_DOMAIN_NAME ID_RHOST STR__X)
	(PROP_DNS_DOMAIN_NAME ID_YHOST STR__BE)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_RHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_YHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_BFRAT YES)
	(PROP_EXECUTABLE ID_BFRAT STR__BG)
	(PROP_FQDN ID_KHOST STR__P)
	(PROP_FQDN ID_RHOST STR__W)
	(PROP_FQDN ID_YHOST STR__BD)
	(PROP_HOST ID_BFRAT ID_RHOST)
	(PROP_HOST ID_LTIMEDELTA ID_KHOST)
	(PROP_HOST ID_STIMEDELTA ID_RHOST)
	(PROP_HOST ID_ZTIMEDELTA ID_YHOST)
	(PROP_HOSTNAME ID_KHOST STR__O)
	(PROP_HOSTNAME ID_RHOST STR__V)
	(PROP_HOSTNAME ID_YHOST STR__BC)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_MICROSECONDS ID_LTIMEDELTA NUM__12)
	(PROP_MICROSECONDS ID_STIMEDELTA NUM__19)
	(PROP_MICROSECONDS ID_ZTIMEDELTA NUM__26)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_SECONDS ID_LTIMEDELTA NUM__13)
	(PROP_SECONDS ID_STIMEDELTA NUM__20)
	(PROP_SECONDS ID_ZTIMEDELTA NUM__27)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_TIMEDELTA ID_KHOST ID_LTIMEDELTA)
	(PROP_TIMEDELTA ID_RHOST ID_STIMEDELTA)
	(PROP_TIMEDELTA ID_YHOST ID_ZTIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_BSRAT ID_YHOST)(PROP_HOST ID_BQRAT ID_KHOST)))
(:metric minimize (total-cost))

)
