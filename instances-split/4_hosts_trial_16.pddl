(define (problem p4_hosts_trial_16) (:domain CALDERA)
(:objects
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BIHOST - OBSERVEDHOST
	ID_CDHOST - OBSERVEDHOST
	ID_BWHOST - OBSERVEDHOST
	ID_BPHOST - OBSERVEDHOST
	ID_CTSCHTASK - OBSERVEDSCHTASK
	ID_CRSCHTASK - OBSERVEDSCHTASK
	ID_CSSCHTASK - OBSERVEDSCHTASK
	ID_CQSCHTASK - OBSERVEDSCHTASK
	ID_DBFILE - OBSERVEDFILE
	ID_CZFILE - OBSERVEDFILE
	ID_DAFILE - OBSERVEDFILE
	ID_CYFILE - OBSERVEDFILE
	STR__PATRICIA - STRING
	STR__BG - STRING
	STR__V - STRING
	STR__CC - STRING
	STR__F - STRING
	STR__BD - STRING
	STR__U - STRING
	STR__ROBERT - STRING
	STR__Q - STRING
	STR__JAMES - STRING
	STR__LINDA - STRING
	STR__BT - STRING
	STR__BARBARA - STRING
	STR__CI - STRING
	STR__M - STRING
	STR__CB - STRING
	STR__CJ - STRING
	STR__CA - STRING
	STR__MARY - STRING
	STR__BU - STRING
	STR__ALPHA - STRING
	STR__J - STRING
	STR__BV - STRING
	STR__CH - STRING
	STR__N - STRING
	STR__Z - STRING
	STR__BH - STRING
	STR__BM - STRING
	STR__R - STRING
	STR__B - STRING
	STR__BN - STRING
	STR__I - STRING
	STR__BC - STRING
	STR__MICHAEL - STRING
	STR__JOHN - STRING
	STR__BO - STRING
	STR__Y - STRING
	STR__CL - STRING
	STR__E - STRING
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_CKRAT - OBSERVEDRAT
	ID_CORAT - OBSERVEDRAT
	ID_CPRAT - OBSERVEDRAT
	ID_CNRAT - OBSERVEDRAT
	ID_CMRAT - OBSERVEDRAT
	ID_CETIMEDELTA - OBSERVEDTIMEDELTA
	ID_BJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CWSHARE - OBSERVEDSHARE
	ID_CUSHARE - OBSERVEDSHARE
	ID_CXSHARE - OBSERVEDSHARE
	ID_CVSHARE - OBSERVEDSHARE
	NUM__43 - NUM
	NUM__51 - NUM
	NUM__44 - NUM
	NUM__58 - NUM
	NUM__57 - NUM
	NUM__36 - NUM
	NUM__50 - NUM
	NUM__37 - NUM
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
)
(:init
	(KNOWS ID_CDHOST)
	(KNOWS ID_CKRAT)
	(KNOWS_PROPERTY ID_CDHOST PFQDN)
	(KNOWS_PROPERTY ID_CKRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_CKRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BIHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BIHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BPHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BPHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BWHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BWHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CDHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CDHOST ID_XDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BIHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BIHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BPHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BPHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BWHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BWHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CDHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CDHOST ID_SDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CDHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_BIHOST NO)
	(PROP_DC ID_BPHOST NO)
	(PROP_DC ID_BWHOST NO)
	(PROP_DC ID_CDHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BIHOST STR__BM)
	(PROP_DNS_DOMAIN_NAME ID_BPHOST STR__BT)
	(PROP_DNS_DOMAIN_NAME ID_BWHOST STR__CA)
	(PROP_DNS_DOMAIN_NAME ID_CDHOST STR__CH)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_CKRAT YES)
	(PROP_EXECUTABLE ID_CKRAT STR__CL)
	(PROP_FQDN ID_BIHOST STR__BN)
	(PROP_FQDN ID_BPHOST STR__BU)
	(PROP_FQDN ID_BWHOST STR__CB)
	(PROP_FQDN ID_CDHOST STR__CI)
	(PROP_HOST ID_BJTIMEDELTA ID_BIHOST)
	(PROP_HOST ID_BQTIMEDELTA ID_BPHOST)
	(PROP_HOST ID_BXTIMEDELTA ID_BWHOST)
	(PROP_HOST ID_CETIMEDELTA ID_CDHOST)
	(PROP_HOST ID_CKRAT ID_CDHOST)
	(PROP_HOSTNAME ID_BIHOST STR__BO)
	(PROP_HOSTNAME ID_BPHOST STR__BV)
	(PROP_HOSTNAME ID_BWHOST STR__CC)
	(PROP_HOSTNAME ID_CDHOST STR__CJ)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BJTIMEDELTA NUM__36)
	(PROP_MICROSECONDS ID_BQTIMEDELTA NUM__43)
	(PROP_MICROSECONDS ID_BXTIMEDELTA NUM__50)
	(PROP_MICROSECONDS ID_CETIMEDELTA NUM__57)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_BJTIMEDELTA NUM__37)
	(PROP_SECONDS ID_BQTIMEDELTA NUM__44)
	(PROP_SECONDS ID_BXTIMEDELTA NUM__51)
	(PROP_SECONDS ID_CETIMEDELTA NUM__58)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_BIHOST ID_BJTIMEDELTA)
	(PROP_TIMEDELTA ID_BPHOST ID_BQTIMEDELTA)
	(PROP_TIMEDELTA ID_BWHOST ID_BXTIMEDELTA)
	(PROP_TIMEDELTA ID_CDHOST ID_CETIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_CPRAT ID_BWHOST)(PROP_HOST ID_CORAT ID_BIHOST)(PROP_HOST ID_CMRAT ID_BPHOST)))
(:metric minimize (total-cost))

)
