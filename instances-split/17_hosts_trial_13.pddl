(define (problem p17_hosts_trial_13) (:domain CALDERA)
(:objects
	ID_KVSCHTASK - OBSERVEDSCHTASK
	ID_KWSCHTASK - OBSERVEDSCHTASK
	ID_KSSCHTASK - OBSERVEDSCHTASK
	ID_LGSCHTASK - OBSERVEDSCHTASK
	ID_KUSCHTASK - OBSERVEDSCHTASK
	ID_KYSCHTASK - OBSERVEDSCHTASK
	ID_KQSCHTASK - OBSERVEDSCHTASK
	ID_LESCHTASK - OBSERVEDSCHTASK
	ID_LDSCHTASK - OBSERVEDSCHTASK
	ID_KZSCHTASK - OBSERVEDSCHTASK
	ID_KXSCHTASK - OBSERVEDSCHTASK
	ID_LASCHTASK - OBSERVEDSCHTASK
	ID_KRSCHTASK - OBSERVEDSCHTASK
	ID_LFSCHTASK - OBSERVEDSCHTASK
	ID_LCSCHTASK - OBSERVEDSCHTASK
	ID_KTSCHTASK - OBSERVEDSCHTASK
	ID_LBSCHTASK - OBSERVEDSCHTASK
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_DYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ESDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_ECDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GRHOST - OBSERVEDHOST
	ID_FPHOST - OBSERVEDHOST
	ID_IHHOST - OBSERVEDHOST
	ID_FIHOST - OBSERVEDHOST
	ID_GKHOST - OBSERVEDHOST
	ID_GYHOST - OBSERVEDHOST
	ID_IAHOST - OBSERVEDHOST
	ID_IOHOST - OBSERVEDHOST
	ID_FWHOST - OBSERVEDHOST
	ID_IVHOST - OBSERVEDHOST
	ID_GDHOST - OBSERVEDHOST
	ID_HTHOST - OBSERVEDHOST
	ID_JQHOST - OBSERVEDHOST
	ID_HMHOST - OBSERVEDHOST
	ID_HFHOST - OBSERVEDHOST
	ID_JJHOST - OBSERVEDHOST
	ID_JCHOST - OBSERVEDHOST
	ID_GETIMEDELTA - OBSERVEDTIMEDELTA
	ID_FJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IITIMEDELTA - OBSERVEDTIMEDELTA
	ID_GZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JKTIMEDELTA - OBSERVEDTIMEDELTA
	STR__NANCY - STRING
	STR__GI - STRING
	STR__CN - STRING
	STR__JY - STRING
	STR__ER - STRING
	STR__IF - STRING
	STR__DD - STRING
	STR__FO - STRING
	STR__MARK - STRING
	STR__FM - STRING
	STR__EF - STRING
	STR__EU - STRING
	STR__CHRISTOPHER - STRING
	STR__HQ - STRING
	STR__BARBARA - STRING
	STR__DO - STRING
	STR__JOSEPH - STRING
	STR__WILLIAM - STRING
	STR__MARGARET - STRING
	STR__V - STRING
	STR__U - STRING
	STR__ELIZABETH - STRING
	STR__R - STRING
	STR__BL - STRING
	STR__Q - STRING
	STR__CQ - STRING
	STR__IU - STRING
	STR__JP - STRING
	STR__JG - STRING
	STR__IT - STRING
	STR__CZ - STRING
	STR__EZ - STRING
	STR__BG - STRING
	STR__DH - STRING
	STR__N - STRING
	STR__JAMES - STRING
	STR__LINDA - STRING
	STR__EI - STRING
	STR__GH - STRING
	STR__JI - STRING
	STR__DK - STRING
	STR__JOHN - STRING
	STR__DAVID - STRING
	STR__IN - STRING
	STR__HR - STRING
	STR__HS - STRING
	STR__HJ - STRING
	STR__SANDRA - STRING
	STR__F - STRING
	STR__BD - STRING
	STR__FC - STRING
	STR__JV - STRING
	STR__GJ - STRING
	STR__HK - STRING
	STR__BP - STRING
	STR__EV - STRING
	STR__DG - STRING
	STR__GC - STRING
	STR__DP - STRING
	STR__CU - STRING
	STR__GX - STRING
	STR__JB - STRING
	STR__HL - STRING
	STR__MICHAEL - STRING
	STR__B - STRING
	STR__FV - STRING
	STR__EE - STRING
	STR__ROBERT - STRING
	STR__CHARLES - STRING
	STR__GP - STRING
	STR__DS - STRING
	STR__JO - STRING
	STR__GEORGE - STRING
	STR__IE - STRING
	STR__BH - STRING
	STR__M - STRING
	STR__BW - STRING
	STR__BT - STRING
	STR__CE - STRING
	STR__FG - STRING
	STR__HZ - STRING
	STR__Z - STRING
	STR__PAUL - STRING
	STR__HE - STRING
	STR__HX - STRING
	STR__EM - STRING
	STR__JU - STRING
	STR__CM - STRING
	STR__FH - STRING
	STR__EN - STRING
	STR__DC - STRING
	STR__JENNIFER - STRING
	STR__IZ - STRING
	STR__GA - STRING
	STR__CR - STRING
	STR__JN - STRING
	STR__IS - STRING
	STR__J - STRING
	STR__DX - STRING
	STR__BC - STRING
	STR__BX - STRING
	STR__PATRICIA - STRING
	STR__EQ - STRING
	STR__DT - STRING
	STR__BO - STRING
	STR__EB - STRING
	STR__GW - STRING
	STR__IL - STRING
	STR__FN - STRING
	STR__EJ - STRING
	STR__I - STRING
	STR__FD - STRING
	STR__DOROTHY - STRING
	STR__CJ - STRING
	STR__KAREN - STRING
	STR__FT - STRING
	STR__JA - STRING
	STR__DANIEL - STRING
	STR__LISA - STRING
	STR__CY - STRING
	STR__MARIA - STRING
	STR__KENNETH - STRING
	STR__IG - STRING
	STR__HY - STRING
	STR__THOMAS - STRING
	STR__DL - STRING
	STR__HD - STRING
	STR__JH - STRING
	STR__GO - STRING
	STR__RICHARD - STRING
	STR__DONNA - STRING
	STR__CB - STRING
	STR__DONALD - STRING
	STR__GV - STRING
	STR__E - STRING
	STR__CI - STRING
	STR__SUSAN - STRING
	STR__MARY - STRING
	STR__IM - STRING
	STR__EY - STRING
	STR__CF - STRING
	STR__BETTY - STRING
	STR__DW - STRING
	STR__FU - STRING
	STR__Y - STRING
	STR__EA - STRING
	STR__HC - STRING
	STR__HELEN - STRING
	STR__GQ - STRING
	STR__JW - STRING
	STR__CA - STRING
	STR__CV - STRING
	STR__GB - STRING
	STR__BS - STRING
	STR__BK - STRING
	STR__ALPHA - STRING
	ID_KISHARE - OBSERVEDSHARE
	ID_KFSHARE - OBSERVEDSHARE
	ID_JZSHARE - OBSERVEDSHARE
	ID_KHSHARE - OBSERVEDSHARE
	ID_KNSHARE - OBSERVEDSHARE
	ID_KDSHARE - OBSERVEDSHARE
	ID_KMSHARE - OBSERVEDSHARE
	ID_KCSHARE - OBSERVEDSHARE
	ID_KJSHARE - OBSERVEDSHARE
	ID_KOSHARE - OBSERVEDSHARE
	ID_KKSHARE - OBSERVEDSHARE
	ID_KPSHARE - OBSERVEDSHARE
	ID_KLSHARE - OBSERVEDSHARE
	ID_KBSHARE - OBSERVEDSHARE
	ID_KGSHARE - OBSERVEDSHARE
	ID_KESHARE - OBSERVEDSHARE
	ID_KASHARE - OBSERVEDSHARE
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_LQFILE - OBSERVEDFILE
	ID_LHFILE - OBSERVEDFILE
	ID_LVFILE - OBSERVEDFILE
	ID_LKFILE - OBSERVEDFILE
	ID_LSFILE - OBSERVEDFILE
	ID_LTFILE - OBSERVEDFILE
	ID_LUFILE - OBSERVEDFILE
	ID_LIFILE - OBSERVEDFILE
	ID_LMFILE - OBSERVEDFILE
	ID_LWFILE - OBSERVEDFILE
	ID_LNFILE - OBSERVEDFILE
	ID_LXFILE - OBSERVEDFILE
	ID_LPFILE - OBSERVEDFILE
	ID_LLFILE - OBSERVEDFILE
	ID_LOFILE - OBSERVEDFILE
	ID_LJFILE - OBSERVEDFILE
	ID_LRFILE - OBSERVEDFILE
	ID_MARAT - OBSERVEDRAT
	ID_MGRAT - OBSERVEDRAT
	ID_MCRAT - OBSERVEDRAT
	ID_MDRAT - OBSERVEDRAT
	ID_LZRAT - OBSERVEDRAT
	ID_LYRAT - OBSERVEDRAT
	ID_MKRAT - OBSERVEDRAT
	ID_JXRAT - OBSERVEDRAT
	ID_MJRAT - OBSERVEDRAT
	ID_MLRAT - OBSERVEDRAT
	ID_MBRAT - OBSERVEDRAT
	ID_MFRAT - OBSERVEDRAT
	ID_MHRAT - OBSERVEDRAT
	ID_MMRAT - OBSERVEDRAT
	ID_MNRAT - OBSERVEDRAT
	ID_MORAT - OBSERVEDRAT
	ID_MERAT - OBSERVEDRAT
	ID_MIRAT - OBSERVEDRAT
	NUM__169 - NUM
	NUM__217 - NUM
	NUM__175 - NUM
	NUM__210 - NUM
	NUM__225 - NUM
	NUM__148 - NUM
	NUM__154 - NUM
	NUM__189 - NUM
	NUM__176 - NUM
	NUM__196 - NUM
	NUM__204 - NUM
	NUM__168 - NUM
	NUM__140 - NUM
	NUM__155 - NUM
	NUM__231 - NUM
	NUM__232 - NUM
	NUM__253 - NUM
	NUM__245 - NUM
	NUM__162 - NUM
	NUM__197 - NUM
	NUM__141 - NUM
	NUM__190 - NUM
	NUM__147 - NUM
	NUM__183 - NUM
	NUM__224 - NUM
	NUM__239 - NUM
	NUM__218 - NUM
	NUM__252 - NUM
	NUM__211 - NUM
	NUM__161 - NUM
	NUM__246 - NUM
	NUM__238 - NUM
	NUM__182 - NUM
	NUM__203 - NUM
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ETDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_FFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ELDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_FBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
)
(:init
	(KNOWS ID_JJHOST)
	(KNOWS ID_JXRAT)
	(KNOWS_PROPERTY ID_JJHOST PFQDN)
	(KNOWS_PROPERTY ID_JXRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_JXRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_DZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_FBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GRHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GRHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GYHOST ID_EXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GYHOST ID_FFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HFHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HFHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HMHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HMHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HTHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HTHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IAHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IAHOST ID_ELDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IHHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IHHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IOHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IOHOST ID_FBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IVHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IVHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JCHOST ID_ETDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JCHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JJHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JJHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JQHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JQHOST ID_DRDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_ESDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_FADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_ESDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_EODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GRHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GRHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GYHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GYHOST ID_DQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HFHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HFHOST ID_ESDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HMHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HMHOST ID_BYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HTHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HTHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IAHOST ID_ESDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IAHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IHHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IHHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IOHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IOHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IVHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IVHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JCHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JCHOST ID_EODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JJHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JJHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JQHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JQHOST ID_GDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_FIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IOHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JCHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JJHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JQHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_DUDOMAINUSER ID_DVDOMAINCREDENTIAL)
	(PROP_CRED ID_DYDOMAINUSER ID_DZDOMAINCREDENTIAL)
	(PROP_CRED ID_ECDOMAINUSER ID_EDDOMAINCREDENTIAL)
	(PROP_CRED ID_EGDOMAINUSER ID_EHDOMAINCREDENTIAL)
	(PROP_CRED ID_EKDOMAINUSER ID_ELDOMAINCREDENTIAL)
	(PROP_CRED ID_EODOMAINUSER ID_EPDOMAINCREDENTIAL)
	(PROP_CRED ID_ESDOMAINUSER ID_ETDOMAINCREDENTIAL)
	(PROP_CRED ID_EWDOMAINUSER ID_EXDOMAINCREDENTIAL)
	(PROP_CRED ID_FADOMAINUSER ID_FBDOMAINCREDENTIAL)
	(PROP_CRED ID_FEDOMAINUSER ID_FFDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_FIHOST NO)
	(PROP_DC ID_FPHOST YES)
	(PROP_DC ID_FWHOST YES)
	(PROP_DC ID_GDHOST YES)
	(PROP_DC ID_GKHOST NO)
	(PROP_DC ID_GRHOST NO)
	(PROP_DC ID_GYHOST NO)
	(PROP_DC ID_HFHOST NO)
	(PROP_DC ID_HMHOST NO)
	(PROP_DC ID_HTHOST NO)
	(PROP_DC ID_IAHOST NO)
	(PROP_DC ID_IHHOST NO)
	(PROP_DC ID_IOHOST NO)
	(PROP_DC ID_IVHOST NO)
	(PROP_DC ID_JCHOST YES)
	(PROP_DC ID_JJHOST YES)
	(PROP_DC ID_JQHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_FIHOST STR__FM)
	(PROP_DNS_DOMAIN_NAME ID_FPHOST STR__FT)
	(PROP_DNS_DOMAIN_NAME ID_FWHOST STR__GA)
	(PROP_DNS_DOMAIN_NAME ID_GDHOST STR__GH)
	(PROP_DNS_DOMAIN_NAME ID_GKHOST STR__GO)
	(PROP_DNS_DOMAIN_NAME ID_GRHOST STR__GV)
	(PROP_DNS_DOMAIN_NAME ID_GYHOST STR__HC)
	(PROP_DNS_DOMAIN_NAME ID_HFHOST STR__HJ)
	(PROP_DNS_DOMAIN_NAME ID_HMHOST STR__HQ)
	(PROP_DNS_DOMAIN_NAME ID_HTHOST STR__HX)
	(PROP_DNS_DOMAIN_NAME ID_IAHOST STR__IE)
	(PROP_DNS_DOMAIN_NAME ID_IHHOST STR__IL)
	(PROP_DNS_DOMAIN_NAME ID_IOHOST STR__IS)
	(PROP_DNS_DOMAIN_NAME ID_IVHOST STR__IZ)
	(PROP_DNS_DOMAIN_NAME ID_JCHOST STR__JG)
	(PROP_DNS_DOMAIN_NAME ID_JJHOST STR__JN)
	(PROP_DNS_DOMAIN_NAME ID_JQHOST STR__JU)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ECDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ELDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ESDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ETDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_FBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_FFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_HFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JCHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JJHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_JXRAT YES)
	(PROP_EXECUTABLE ID_JXRAT STR__JY)
	(PROP_FQDN ID_FIHOST STR__FO)
	(PROP_FQDN ID_FPHOST STR__FV)
	(PROP_FQDN ID_FWHOST STR__GC)
	(PROP_FQDN ID_GDHOST STR__GJ)
	(PROP_FQDN ID_GKHOST STR__GQ)
	(PROP_FQDN ID_GRHOST STR__GX)
	(PROP_FQDN ID_GYHOST STR__HE)
	(PROP_FQDN ID_HFHOST STR__HL)
	(PROP_FQDN ID_HMHOST STR__HS)
	(PROP_FQDN ID_HTHOST STR__HZ)
	(PROP_FQDN ID_IAHOST STR__IG)
	(PROP_FQDN ID_IHHOST STR__IN)
	(PROP_FQDN ID_IOHOST STR__IU)
	(PROP_FQDN ID_IVHOST STR__JB)
	(PROP_FQDN ID_JCHOST STR__JI)
	(PROP_FQDN ID_JJHOST STR__JP)
	(PROP_FQDN ID_JQHOST STR__JW)
	(PROP_HOST ID_FJTIMEDELTA ID_FIHOST)
	(PROP_HOST ID_FQTIMEDELTA ID_FPHOST)
	(PROP_HOST ID_FXTIMEDELTA ID_FWHOST)
	(PROP_HOST ID_GETIMEDELTA ID_GDHOST)
	(PROP_HOST ID_GLTIMEDELTA ID_GKHOST)
	(PROP_HOST ID_GSTIMEDELTA ID_GRHOST)
	(PROP_HOST ID_GZTIMEDELTA ID_GYHOST)
	(PROP_HOST ID_HGTIMEDELTA ID_HFHOST)
	(PROP_HOST ID_HNTIMEDELTA ID_HMHOST)
	(PROP_HOST ID_HUTIMEDELTA ID_HTHOST)
	(PROP_HOST ID_IBTIMEDELTA ID_IAHOST)
	(PROP_HOST ID_IITIMEDELTA ID_IHHOST)
	(PROP_HOST ID_IPTIMEDELTA ID_IOHOST)
	(PROP_HOST ID_IWTIMEDELTA ID_IVHOST)
	(PROP_HOST ID_JDTIMEDELTA ID_JCHOST)
	(PROP_HOST ID_JKTIMEDELTA ID_JJHOST)
	(PROP_HOST ID_JRTIMEDELTA ID_JQHOST)
	(PROP_HOST ID_JXRAT ID_JJHOST)
	(PROP_HOSTNAME ID_FIHOST STR__FN)
	(PROP_HOSTNAME ID_FPHOST STR__FU)
	(PROP_HOSTNAME ID_FWHOST STR__GB)
	(PROP_HOSTNAME ID_GDHOST STR__GI)
	(PROP_HOSTNAME ID_GKHOST STR__GP)
	(PROP_HOSTNAME ID_GRHOST STR__GW)
	(PROP_HOSTNAME ID_GYHOST STR__HD)
	(PROP_HOSTNAME ID_HFHOST STR__HK)
	(PROP_HOSTNAME ID_HMHOST STR__HR)
	(PROP_HOSTNAME ID_HTHOST STR__HY)
	(PROP_HOSTNAME ID_IAHOST STR__IF)
	(PROP_HOSTNAME ID_IHHOST STR__IM)
	(PROP_HOSTNAME ID_IOHOST STR__IT)
	(PROP_HOSTNAME ID_IVHOST STR__JA)
	(PROP_HOSTNAME ID_JCHOST STR__JH)
	(PROP_HOSTNAME ID_JJHOST STR__JO)
	(PROP_HOSTNAME ID_JQHOST STR__JV)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_DUDOMAINUSER NO)
	(PROP_IS_GROUP ID_DYDOMAINUSER NO)
	(PROP_IS_GROUP ID_ECDOMAINUSER NO)
	(PROP_IS_GROUP ID_EGDOMAINUSER NO)
	(PROP_IS_GROUP ID_EKDOMAINUSER NO)
	(PROP_IS_GROUP ID_EODOMAINUSER NO)
	(PROP_IS_GROUP ID_ESDOMAINUSER NO)
	(PROP_IS_GROUP ID_EWDOMAINUSER NO)
	(PROP_IS_GROUP ID_FADOMAINUSER NO)
	(PROP_IS_GROUP ID_FEDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_FJTIMEDELTA NUM__140)
	(PROP_MICROSECONDS ID_FQTIMEDELTA NUM__147)
	(PROP_MICROSECONDS ID_FXTIMEDELTA NUM__154)
	(PROP_MICROSECONDS ID_GETIMEDELTA NUM__161)
	(PROP_MICROSECONDS ID_GLTIMEDELTA NUM__168)
	(PROP_MICROSECONDS ID_GSTIMEDELTA NUM__175)
	(PROP_MICROSECONDS ID_GZTIMEDELTA NUM__182)
	(PROP_MICROSECONDS ID_HGTIMEDELTA NUM__189)
	(PROP_MICROSECONDS ID_HNTIMEDELTA NUM__196)
	(PROP_MICROSECONDS ID_HUTIMEDELTA NUM__203)
	(PROP_MICROSECONDS ID_IBTIMEDELTA NUM__210)
	(PROP_MICROSECONDS ID_IITIMEDELTA NUM__217)
	(PROP_MICROSECONDS ID_IPTIMEDELTA NUM__224)
	(PROP_MICROSECONDS ID_IWTIMEDELTA NUM__231)
	(PROP_MICROSECONDS ID_JDTIMEDELTA NUM__238)
	(PROP_MICROSECONDS ID_JKTIMEDELTA NUM__245)
	(PROP_MICROSECONDS ID_JRTIMEDELTA NUM__252)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_DVDOMAINCREDENTIAL STR__DW)
	(PROP_PASSWORD ID_DZDOMAINCREDENTIAL STR__EA)
	(PROP_PASSWORD ID_EDDOMAINCREDENTIAL STR__EE)
	(PROP_PASSWORD ID_EHDOMAINCREDENTIAL STR__EI)
	(PROP_PASSWORD ID_ELDOMAINCREDENTIAL STR__EM)
	(PROP_PASSWORD ID_EPDOMAINCREDENTIAL STR__EQ)
	(PROP_PASSWORD ID_ETDOMAINCREDENTIAL STR__EU)
	(PROP_PASSWORD ID_EXDOMAINCREDENTIAL STR__EY)
	(PROP_PASSWORD ID_FBDOMAINCREDENTIAL STR__FC)
	(PROP_PASSWORD ID_FFDOMAINCREDENTIAL STR__FG)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_FJTIMEDELTA NUM__141)
	(PROP_SECONDS ID_FQTIMEDELTA NUM__148)
	(PROP_SECONDS ID_FXTIMEDELTA NUM__155)
	(PROP_SECONDS ID_GETIMEDELTA NUM__162)
	(PROP_SECONDS ID_GLTIMEDELTA NUM__169)
	(PROP_SECONDS ID_GSTIMEDELTA NUM__176)
	(PROP_SECONDS ID_GZTIMEDELTA NUM__183)
	(PROP_SECONDS ID_HGTIMEDELTA NUM__190)
	(PROP_SECONDS ID_HNTIMEDELTA NUM__197)
	(PROP_SECONDS ID_HUTIMEDELTA NUM__204)
	(PROP_SECONDS ID_IBTIMEDELTA NUM__211)
	(PROP_SECONDS ID_IITIMEDELTA NUM__218)
	(PROP_SECONDS ID_IPTIMEDELTA NUM__225)
	(PROP_SECONDS ID_IWTIMEDELTA NUM__232)
	(PROP_SECONDS ID_JDTIMEDELTA NUM__239)
	(PROP_SECONDS ID_JKTIMEDELTA NUM__246)
	(PROP_SECONDS ID_JRTIMEDELTA NUM__253)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_DUDOMAINUSER STR__DX)
	(PROP_SID ID_DYDOMAINUSER STR__EB)
	(PROP_SID ID_ECDOMAINUSER STR__EF)
	(PROP_SID ID_EGDOMAINUSER STR__EJ)
	(PROP_SID ID_EKDOMAINUSER STR__EN)
	(PROP_SID ID_EODOMAINUSER STR__ER)
	(PROP_SID ID_ESDOMAINUSER STR__EV)
	(PROP_SID ID_EWDOMAINUSER STR__EZ)
	(PROP_SID ID_FADOMAINUSER STR__FD)
	(PROP_SID ID_FEDOMAINUSER STR__FH)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_FIHOST ID_FJTIMEDELTA)
	(PROP_TIMEDELTA ID_FPHOST ID_FQTIMEDELTA)
	(PROP_TIMEDELTA ID_FWHOST ID_FXTIMEDELTA)
	(PROP_TIMEDELTA ID_GDHOST ID_GETIMEDELTA)
	(PROP_TIMEDELTA ID_GKHOST ID_GLTIMEDELTA)
	(PROP_TIMEDELTA ID_GRHOST ID_GSTIMEDELTA)
	(PROP_TIMEDELTA ID_GYHOST ID_GZTIMEDELTA)
	(PROP_TIMEDELTA ID_HFHOST ID_HGTIMEDELTA)
	(PROP_TIMEDELTA ID_HMHOST ID_HNTIMEDELTA)
	(PROP_TIMEDELTA ID_HTHOST ID_HUTIMEDELTA)
	(PROP_TIMEDELTA ID_IAHOST ID_IBTIMEDELTA)
	(PROP_TIMEDELTA ID_IHHOST ID_IITIMEDELTA)
	(PROP_TIMEDELTA ID_IOHOST ID_IPTIMEDELTA)
	(PROP_TIMEDELTA ID_IVHOST ID_IWTIMEDELTA)
	(PROP_TIMEDELTA ID_JCHOST ID_JDTIMEDELTA)
	(PROP_TIMEDELTA ID_JJHOST ID_JKTIMEDELTA)
	(PROP_TIMEDELTA ID_JQHOST ID_JRTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_DVDOMAINCREDENTIAL ID_DUDOMAINUSER)
	(PROP_USER ID_DZDOMAINCREDENTIAL ID_DYDOMAINUSER)
	(PROP_USER ID_EDDOMAINCREDENTIAL ID_ECDOMAINUSER)
	(PROP_USER ID_EHDOMAINCREDENTIAL ID_EGDOMAINUSER)
	(PROP_USER ID_ELDOMAINCREDENTIAL ID_EKDOMAINUSER)
	(PROP_USER ID_EPDOMAINCREDENTIAL ID_EODOMAINUSER)
	(PROP_USER ID_ETDOMAINCREDENTIAL ID_ESDOMAINUSER)
	(PROP_USER ID_EXDOMAINCREDENTIAL ID_EWDOMAINUSER)
	(PROP_USER ID_FBDOMAINCREDENTIAL ID_FADOMAINUSER)
	(PROP_USER ID_FFDOMAINCREDENTIAL ID_FEDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_DUDOMAINUSER STR__PAUL)
	(PROP_USERNAME ID_DYDOMAINUSER STR__KAREN)
	(PROP_USERNAME ID_ECDOMAINUSER STR__MARK)
	(PROP_USERNAME ID_EGDOMAINUSER STR__BETTY)
	(PROP_USERNAME ID_EKDOMAINUSER STR__DONALD)
	(PROP_USERNAME ID_EODOMAINUSER STR__HELEN)
	(PROP_USERNAME ID_ESDOMAINUSER STR__GEORGE)
	(PROP_USERNAME ID_EWDOMAINUSER STR__SANDRA)
	(PROP_USERNAME ID_FADOMAINUSER STR__KENNETH)
	(PROP_USERNAME ID_FEDOMAINUSER STR__DONNA)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_MFRAT ID_HTHOST)(PROP_HOST ID_MBRAT ID_GKHOST)(PROP_HOST ID_MKRAT ID_IHHOST)(PROP_HOST ID_MDRAT ID_GRHOST)(PROP_HOST ID_MCRAT ID_JCHOST)(PROP_HOST ID_MERAT ID_HMHOST)(PROP_HOST ID_MMRAT ID_JQHOST)(PROP_HOST ID_MNRAT ID_GDHOST)(PROP_HOST ID_MHRAT ID_FWHOST)(PROP_HOST ID_MLRAT ID_IVHOST)(PROP_HOST ID_MJRAT ID_IOHOST)(PROP_HOST ID_LYRAT ID_IAHOST)(PROP_HOST ID_LZRAT ID_GYHOST)(PROP_HOST ID_MGRAT ID_FIHOST)(PROP_HOST ID_MARAT ID_FPHOST)(PROP_HOST ID_MIRAT ID_HFHOST)))
(:metric minimize (total-cost))

)
