(define (problem p19_hosts_trial_9) (:domain CALDERA)
(:objects
	ID_IRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JTTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IKTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HITIMEDELTA - OBSERVEDTIMEDELTA
	ID_GGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JFTIMEDELTA - OBSERVEDTIMEDELTA
	ID_JMTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IYTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FETIMEDELTA - OBSERVEDTIMEDELTA
	ID_KGRAT - OBSERVEDRAT
	ID_KLRAT - OBSERVEDRAT
	ID_KORAT - OBSERVEDRAT
	ID_KJRAT - OBSERVEDRAT
	ID_KPRAT - OBSERVEDRAT
	ID_KTRAT - OBSERVEDRAT
	ID_KHRAT - OBSERVEDRAT
	ID_KMRAT - OBSERVEDRAT
	ID_KNRAT - OBSERVEDRAT
	ID_KERAT - OBSERVEDRAT
	ID_KBRAT - OBSERVEDRAT
	ID_KRRAT - OBSERVEDRAT
	ID_JZRAT - OBSERVEDRAT
	ID_KKRAT - OBSERVEDRAT
	ID_KQRAT - OBSERVEDRAT
	ID_KSRAT - OBSERVEDRAT
	ID_KFRAT - OBSERVEDRAT
	ID_KDRAT - OBSERVEDRAT
	ID_KCRAT - OBSERVEDRAT
	ID_KIRAT - OBSERVEDRAT
	NUM__192 - NUM
	NUM__227 - NUM
	NUM__255 - NUM
	NUM__150 - NUM
	NUM__184 - NUM
	NUM__164 - NUM
	NUM__234 - NUM
	NUM__240 - NUM
	NUM__171 - NUM
	NUM__136 - NUM
	NUM__212 - NUM
	NUM__247 - NUM
	NUM__198 - NUM
	NUM__129 - NUM
	NUM__128 - NUM
	NUM__248 - NUM
	NUM__226 - NUM
	NUM__206 - NUM
	NUM__219 - NUM
	NUM__178 - NUM
	NUM__254 - NUM
	NUM__157 - NUM
	NUM__191 - NUM
	NUM__185 - NUM
	NUM__177 - NUM
	NUM__142 - NUM
	NUM__135 - NUM
	NUM__199 - NUM
	NUM__163 - NUM
	NUM__170 - NUM
	NUM__241 - NUM
	NUM__213 - NUM
	NUM__205 - NUM
	NUM__233 - NUM
	NUM__149 - NUM
	NUM__156 - NUM
	NUM__220 - NUM
	NUM__143 - NUM
	ID_LEFILE - OBSERVEDFILE
	ID_LGFILE - OBSERVEDFILE
	ID_LIFILE - OBSERVEDFILE
	ID_LLFILE - OBSERVEDFILE
	ID_LBFILE - OBSERVEDFILE
	ID_KYFILE - OBSERVEDFILE
	ID_KXFILE - OBSERVEDFILE
	ID_LAFILE - OBSERVEDFILE
	ID_LCFILE - OBSERVEDFILE
	ID_LHFILE - OBSERVEDFILE
	ID_LJFILE - OBSERVEDFILE
	ID_LKFILE - OBSERVEDFILE
	ID_KVFILE - OBSERVEDFILE
	ID_KZFILE - OBSERVEDFILE
	ID_KUFILE - OBSERVEDFILE
	ID_LFFILE - OBSERVEDFILE
	ID_LDFILE - OBSERVEDFILE
	ID_KWFILE - OBSERVEDFILE
	ID_LMFILE - OBSERVEDFILE
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_LTSHARE - OBSERVEDSHARE
	ID_LRSHARE - OBSERVEDSHARE
	ID_LSSHARE - OBSERVEDSHARE
	ID_LUSHARE - OBSERVEDSHARE
	ID_MESHARE - OBSERVEDSHARE
	ID_LXSHARE - OBSERVEDSHARE
	ID_MCSHARE - OBSERVEDSHARE
	ID_LNSHARE - OBSERVEDSHARE
	ID_LPSHARE - OBSERVEDSHARE
	ID_MASHARE - OBSERVEDSHARE
	ID_LWSHARE - OBSERVEDSHARE
	ID_LVSHARE - OBSERVEDSHARE
	ID_MDSHARE - OBSERVEDSHARE
	ID_LQSHARE - OBSERVEDSHARE
	ID_MFSHARE - OBSERVEDSHARE
	ID_LOSHARE - OBSERVEDSHARE
	ID_LYSHARE - OBSERVEDSHARE
	ID_LZSHARE - OBSERVEDSHARE
	ID_MBSHARE - OBSERVEDSHARE
	ID_EKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ECDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ESDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ETDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ELDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_GFHOST - OBSERVEDHOST
	ID_GTHOST - OBSERVEDHOST
	ID_HVHOST - OBSERVEDHOST
	ID_HOHOST - OBSERVEDHOST
	ID_GMHOST - OBSERVEDHOST
	ID_HAHOST - OBSERVEDHOST
	ID_IQHOST - OBSERVEDHOST
	ID_JEHOST - OBSERVEDHOST
	ID_JLHOST - OBSERVEDHOST
	ID_FYHOST - OBSERVEDHOST
	ID_FDHOST - OBSERVEDHOST
	ID_IJHOST - OBSERVEDHOST
	ID_JSHOST - OBSERVEDHOST
	ID_FKHOST - OBSERVEDHOST
	ID_EWHOST - OBSERVEDHOST
	ID_FRHOST - OBSERVEDHOST
	ID_HHHOST - OBSERVEDHOST
	ID_IXHOST - OBSERVEDHOST
	ID_ICHOST - OBSERVEDHOST
	ID_MSSCHTASK - OBSERVEDSCHTASK
	ID_MUSCHTASK - OBSERVEDSCHTASK
	ID_MISCHTASK - OBSERVEDSCHTASK
	ID_MMSCHTASK - OBSERVEDSCHTASK
	ID_MQSCHTASK - OBSERVEDSCHTASK
	ID_MPSCHTASK - OBSERVEDSCHTASK
	ID_MYSCHTASK - OBSERVEDSCHTASK
	ID_MTSCHTASK - OBSERVEDSCHTASK
	ID_MWSCHTASK - OBSERVEDSCHTASK
	ID_MLSCHTASK - OBSERVEDSCHTASK
	ID_MXSCHTASK - OBSERVEDSCHTASK
	ID_MJSCHTASK - OBSERVEDSCHTASK
	ID_MKSCHTASK - OBSERVEDSCHTASK
	ID_MGSCHTASK - OBSERVEDSCHTASK
	ID_MNSCHTASK - OBSERVEDSCHTASK
	ID_MOSCHTASK - OBSERVEDSCHTASK
	ID_MRSCHTASK - OBSERVEDSCHTASK
	ID_MVSCHTASK - OBSERVEDSCHTASK
	ID_MHSCHTASK - OBSERVEDSCHTASK
	STR__DG - STRING
	STR__DS - STRING
	STR__HELEN - STRING
	STR__GEORGE - STRING
	STR__GC - STRING
	STR__IU - STRING
	STR__CM - STRING
	STR__DP - STRING
	STR__EU - STRING
	STR__MICHAEL - STRING
	STR__ROBERT - STRING
	STR__HE - STRING
	STR__U - STRING
	STR__JX - STRING
	STR__BP - STRING
	STR__BS - STRING
	STR__N - STRING
	STR__JP - STRING
	STR__BC - STRING
	STR__DW - STRING
	STR__EA - STRING
	STR__CQ - STRING
	STR__J - STRING
	STR__Z - STRING
	STR__HF - STRING
	STR__Q - STRING
	STR__JJ - STRING
	STR__BW - STRING
	STR__IP - STRING
	STR__HS - STRING
	STR__JY - STRING
	STR__I - STRING
	STR__BX - STRING
	STR__DX - STRING
	STR__BARBARA - STRING
	STR__IN - STRING
	STR__GY - STRING
	STR__DL - STRING
	STR__II - STRING
	STR__FH - STRING
	STR__PAUL - STRING
	STR__CI - STRING
	STR__EJ - STRING
	STR__ER - STRING
	STR__PATRICIA - STRING
	STR__ALPHA - STRING
	STR__CY - STRING
	STR__JI - STRING
	STR__F - STRING
	STR__GQ - STRING
	STR__EE - STRING
	STR__GS - STRING
	STR__BT - STRING
	STR__BH - STRING
	STR__IG - STRING
	STR__EM - STRING
	STR__E - STRING
	STR__GX - STRING
	STR__EQ - STRING
	STR__THOMAS - STRING
	STR__BK - STRING
	STR__EN - STRING
	STR__FB - STRING
	STR__HZ - STRING
	STR__CR - STRING
	STR__IV - STRING
	STR__MARIA - STRING
	STR__GZ - STRING
	STR__IW - STRING
	STR__IB - STRING
	STR__DANIEL - STRING
	STR__CE - STRING
	STR__GR - STRING
	STR__CA - STRING
	STR__HL - STRING
	STR__BL - STRING
	STR__JB - STRING
	STR__FA - STRING
	STR__MARY - STRING
	STR__B - STRING
	STR__BO - STRING
	STR__MARK - STRING
	STR__EB - STRING
	STR__BETTY - STRING
	STR__HM - STRING
	STR__JQ - STRING
	STR__IA - STRING
	STR__CJ - STRING
	STR__DK - STRING
	STR__MARGARET - STRING
	STR__V - STRING
	STR__EV - STRING
	STR__GK - STRING
	STR__FX - STRING
	STR__M - STRING
	STR__HG - STRING
	STR__CHRISTOPHER - STRING
	STR__JOSEPH - STRING
	STR__FI - STRING
	STR__DD - STRING
	STR__CZ - STRING
	STR__IO - STRING
	STR__DT - STRING
	STR__EF - STRING
	STR__KA - STRING
	STR__Y - STRING
	STR__FP - STRING
	STR__JC - STRING
	STR__R - STRING
	STR__DH - STRING
	STR__DAVID - STRING
	STR__FO - STRING
	STR__WILLIAM - STRING
	STR__HN - STRING
	STR__JAMES - STRING
	STR__LINDA - STRING
	STR__KAREN - STRING
	STR__FW - STRING
	STR__GJ - STRING
	STR__GE - STRING
	STR__JD - STRING
	STR__JOHN - STRING
	STR__CU - STRING
	STR__JK - STRING
	STR__CF - STRING
	STR__ELIZABETH - STRING
	STR__NANCY - STRING
	STR__RICHARD - STRING
	STR__DC - STRING
	STR__HT - STRING
	STR__EI - STRING
	STR__FJ - STRING
	STR__BD - STRING
	STR__CHARLES - STRING
	STR__FQ - STRING
	STR__GL - STRING
	STR__CN - STRING
	STR__JENNIFER - STRING
	STR__DONALD - STRING
	STR__FV - STRING
	STR__BG - STRING
	STR__JW - STRING
	STR__CB - STRING
	STR__CV - STRING
	STR__FC - STRING
	STR__SUSAN - STRING
	STR__DO - STRING
	STR__HU - STRING
	STR__LISA - STRING
	STR__GD - STRING
	STR__IH - STRING
	STR__DOROTHY - STRING
	STR__JR - STRING
)
(:init
	(KNOWS ID_HOHOST)
	(KNOWS ID_JZRAT)
	(KNOWS_PROPERTY ID_HOHOST PFQDN)
	(KNOWS_PROPERTY ID_JZRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_JZRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_EHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_DZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HAHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HAHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HAHOST ID_EHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HAHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HAHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HHHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HHHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HHHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HHHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HHHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HOHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HOHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HOHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HOHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HOHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HVHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HVHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HVHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HVHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HVHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ICHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ICHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ICHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ICHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ICHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IJHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IJHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IJHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IJHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IJHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IQHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IQHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IQHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IQHOST ID_EHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IQHOST ID_ETDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IXHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IXHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IXHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IXHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_IXHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JEHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JEHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JEHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JEHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JEHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JLHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JLHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JLHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JLHOST ID_ELDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JLHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JSHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JSHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JSHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JSHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_JSHOST ID_LDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_CKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_CODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_CCDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_DIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_EODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_EODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HAHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HAHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HAHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HHHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HHHOST ID_EGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HHHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HOHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HOHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HOHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HVHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HVHOST ID_DQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HVHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ICHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ICHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ICHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IJHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IJHOST ID_DQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IJHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IQHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IQHOST ID_BYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IQHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IXHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IXHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_IXHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JEHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JEHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JEHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JLHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JLHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JLHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JSHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JSHOST ID_EGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_JSHOST ID_WDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_EWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HOHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ICHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IJHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IQHOST)
	(MEM_HOSTS ID_ADOMAIN ID_IXHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JEHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JLHOST)
	(MEM_HOSTS ID_ADOMAIN ID_JSHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_DUDOMAINUSER ID_DVDOMAINCREDENTIAL)
	(PROP_CRED ID_DYDOMAINUSER ID_DZDOMAINCREDENTIAL)
	(PROP_CRED ID_ECDOMAINUSER ID_EDDOMAINCREDENTIAL)
	(PROP_CRED ID_EGDOMAINUSER ID_EHDOMAINCREDENTIAL)
	(PROP_CRED ID_EKDOMAINUSER ID_ELDOMAINCREDENTIAL)
	(PROP_CRED ID_EODOMAINUSER ID_EPDOMAINCREDENTIAL)
	(PROP_CRED ID_ESDOMAINUSER ID_ETDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_EWHOST NO)
	(PROP_DC ID_FDHOST YES)
	(PROP_DC ID_FKHOST NO)
	(PROP_DC ID_FRHOST NO)
	(PROP_DC ID_FYHOST NO)
	(PROP_DC ID_GFHOST NO)
	(PROP_DC ID_GMHOST YES)
	(PROP_DC ID_GTHOST NO)
	(PROP_DC ID_HAHOST YES)
	(PROP_DC ID_HHHOST NO)
	(PROP_DC ID_HOHOST NO)
	(PROP_DC ID_HVHOST NO)
	(PROP_DC ID_ICHOST NO)
	(PROP_DC ID_IJHOST NO)
	(PROP_DC ID_IQHOST NO)
	(PROP_DC ID_IXHOST NO)
	(PROP_DC ID_JEHOST YES)
	(PROP_DC ID_JLHOST NO)
	(PROP_DC ID_JSHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_EWHOST STR__FC)
	(PROP_DNS_DOMAIN_NAME ID_FDHOST STR__FJ)
	(PROP_DNS_DOMAIN_NAME ID_FKHOST STR__FQ)
	(PROP_DNS_DOMAIN_NAME ID_FRHOST STR__FX)
	(PROP_DNS_DOMAIN_NAME ID_FYHOST STR__GE)
	(PROP_DNS_DOMAIN_NAME ID_GFHOST STR__GL)
	(PROP_DNS_DOMAIN_NAME ID_GMHOST STR__GS)
	(PROP_DNS_DOMAIN_NAME ID_GTHOST STR__GZ)
	(PROP_DNS_DOMAIN_NAME ID_HAHOST STR__HG)
	(PROP_DNS_DOMAIN_NAME ID_HHHOST STR__HN)
	(PROP_DNS_DOMAIN_NAME ID_HOHOST STR__HU)
	(PROP_DNS_DOMAIN_NAME ID_HVHOST STR__IB)
	(PROP_DNS_DOMAIN_NAME ID_ICHOST STR__II)
	(PROP_DNS_DOMAIN_NAME ID_IJHOST STR__IP)
	(PROP_DNS_DOMAIN_NAME ID_IQHOST STR__IW)
	(PROP_DNS_DOMAIN_NAME ID_IXHOST STR__JD)
	(PROP_DNS_DOMAIN_NAME ID_JEHOST STR__JK)
	(PROP_DNS_DOMAIN_NAME ID_JLHOST STR__JR)
	(PROP_DNS_DOMAIN_NAME ID_JSHOST STR__JY)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ECDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ELDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ESDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ETDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_HAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ICHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IJHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_IXHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JEHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JLHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_JSHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_JZRAT YES)
	(PROP_EXECUTABLE ID_JZRAT STR__KA)
	(PROP_FQDN ID_EWHOST STR__FB)
	(PROP_FQDN ID_FDHOST STR__FI)
	(PROP_FQDN ID_FKHOST STR__FP)
	(PROP_FQDN ID_FRHOST STR__FW)
	(PROP_FQDN ID_FYHOST STR__GD)
	(PROP_FQDN ID_GFHOST STR__GK)
	(PROP_FQDN ID_GMHOST STR__GR)
	(PROP_FQDN ID_GTHOST STR__GY)
	(PROP_FQDN ID_HAHOST STR__HF)
	(PROP_FQDN ID_HHHOST STR__HM)
	(PROP_FQDN ID_HOHOST STR__HT)
	(PROP_FQDN ID_HVHOST STR__IA)
	(PROP_FQDN ID_ICHOST STR__IH)
	(PROP_FQDN ID_IJHOST STR__IO)
	(PROP_FQDN ID_IQHOST STR__IV)
	(PROP_FQDN ID_IXHOST STR__JC)
	(PROP_FQDN ID_JEHOST STR__JJ)
	(PROP_FQDN ID_JLHOST STR__JQ)
	(PROP_FQDN ID_JSHOST STR__JX)
	(PROP_HOST ID_EXTIMEDELTA ID_EWHOST)
	(PROP_HOST ID_FETIMEDELTA ID_FDHOST)
	(PROP_HOST ID_FLTIMEDELTA ID_FKHOST)
	(PROP_HOST ID_FSTIMEDELTA ID_FRHOST)
	(PROP_HOST ID_FZTIMEDELTA ID_FYHOST)
	(PROP_HOST ID_GGTIMEDELTA ID_GFHOST)
	(PROP_HOST ID_GNTIMEDELTA ID_GMHOST)
	(PROP_HOST ID_GUTIMEDELTA ID_GTHOST)
	(PROP_HOST ID_HBTIMEDELTA ID_HAHOST)
	(PROP_HOST ID_HITIMEDELTA ID_HHHOST)
	(PROP_HOST ID_HPTIMEDELTA ID_HOHOST)
	(PROP_HOST ID_HWTIMEDELTA ID_HVHOST)
	(PROP_HOST ID_IDTIMEDELTA ID_ICHOST)
	(PROP_HOST ID_IKTIMEDELTA ID_IJHOST)
	(PROP_HOST ID_IRTIMEDELTA ID_IQHOST)
	(PROP_HOST ID_IYTIMEDELTA ID_IXHOST)
	(PROP_HOST ID_JFTIMEDELTA ID_JEHOST)
	(PROP_HOST ID_JMTIMEDELTA ID_JLHOST)
	(PROP_HOST ID_JTTIMEDELTA ID_JSHOST)
	(PROP_HOST ID_JZRAT ID_HOHOST)
	(PROP_HOSTNAME ID_EWHOST STR__FA)
	(PROP_HOSTNAME ID_FDHOST STR__FH)
	(PROP_HOSTNAME ID_FKHOST STR__FO)
	(PROP_HOSTNAME ID_FRHOST STR__FV)
	(PROP_HOSTNAME ID_FYHOST STR__GC)
	(PROP_HOSTNAME ID_GFHOST STR__GJ)
	(PROP_HOSTNAME ID_GMHOST STR__GQ)
	(PROP_HOSTNAME ID_GTHOST STR__GX)
	(PROP_HOSTNAME ID_HAHOST STR__HE)
	(PROP_HOSTNAME ID_HHHOST STR__HL)
	(PROP_HOSTNAME ID_HOHOST STR__HS)
	(PROP_HOSTNAME ID_HVHOST STR__HZ)
	(PROP_HOSTNAME ID_ICHOST STR__IG)
	(PROP_HOSTNAME ID_IJHOST STR__IN)
	(PROP_HOSTNAME ID_IQHOST STR__IU)
	(PROP_HOSTNAME ID_IXHOST STR__JB)
	(PROP_HOSTNAME ID_JEHOST STR__JI)
	(PROP_HOSTNAME ID_JLHOST STR__JP)
	(PROP_HOSTNAME ID_JSHOST STR__JW)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_DUDOMAINUSER NO)
	(PROP_IS_GROUP ID_DYDOMAINUSER NO)
	(PROP_IS_GROUP ID_ECDOMAINUSER NO)
	(PROP_IS_GROUP ID_EGDOMAINUSER NO)
	(PROP_IS_GROUP ID_EKDOMAINUSER NO)
	(PROP_IS_GROUP ID_EODOMAINUSER NO)
	(PROP_IS_GROUP ID_ESDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_EXTIMEDELTA NUM__129)
	(PROP_MICROSECONDS ID_FETIMEDELTA NUM__136)
	(PROP_MICROSECONDS ID_FLTIMEDELTA NUM__143)
	(PROP_MICROSECONDS ID_FSTIMEDELTA NUM__150)
	(PROP_MICROSECONDS ID_FZTIMEDELTA NUM__157)
	(PROP_MICROSECONDS ID_GGTIMEDELTA NUM__164)
	(PROP_MICROSECONDS ID_GNTIMEDELTA NUM__171)
	(PROP_MICROSECONDS ID_GUTIMEDELTA NUM__178)
	(PROP_MICROSECONDS ID_HBTIMEDELTA NUM__185)
	(PROP_MICROSECONDS ID_HITIMEDELTA NUM__192)
	(PROP_MICROSECONDS ID_HPTIMEDELTA NUM__199)
	(PROP_MICROSECONDS ID_HWTIMEDELTA NUM__206)
	(PROP_MICROSECONDS ID_IDTIMEDELTA NUM__213)
	(PROP_MICROSECONDS ID_IKTIMEDELTA NUM__220)
	(PROP_MICROSECONDS ID_IRTIMEDELTA NUM__227)
	(PROP_MICROSECONDS ID_IYTIMEDELTA NUM__234)
	(PROP_MICROSECONDS ID_JFTIMEDELTA NUM__241)
	(PROP_MICROSECONDS ID_JMTIMEDELTA NUM__248)
	(PROP_MICROSECONDS ID_JTTIMEDELTA NUM__255)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_DVDOMAINCREDENTIAL STR__DW)
	(PROP_PASSWORD ID_DZDOMAINCREDENTIAL STR__EA)
	(PROP_PASSWORD ID_EDDOMAINCREDENTIAL STR__EE)
	(PROP_PASSWORD ID_EHDOMAINCREDENTIAL STR__EI)
	(PROP_PASSWORD ID_ELDOMAINCREDENTIAL STR__EM)
	(PROP_PASSWORD ID_EPDOMAINCREDENTIAL STR__EQ)
	(PROP_PASSWORD ID_ETDOMAINCREDENTIAL STR__EU)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_EXTIMEDELTA NUM__128)
	(PROP_SECONDS ID_FETIMEDELTA NUM__135)
	(PROP_SECONDS ID_FLTIMEDELTA NUM__142)
	(PROP_SECONDS ID_FSTIMEDELTA NUM__149)
	(PROP_SECONDS ID_FZTIMEDELTA NUM__156)
	(PROP_SECONDS ID_GGTIMEDELTA NUM__163)
	(PROP_SECONDS ID_GNTIMEDELTA NUM__170)
	(PROP_SECONDS ID_GUTIMEDELTA NUM__177)
	(PROP_SECONDS ID_HBTIMEDELTA NUM__184)
	(PROP_SECONDS ID_HITIMEDELTA NUM__191)
	(PROP_SECONDS ID_HPTIMEDELTA NUM__198)
	(PROP_SECONDS ID_HWTIMEDELTA NUM__205)
	(PROP_SECONDS ID_IDTIMEDELTA NUM__212)
	(PROP_SECONDS ID_IKTIMEDELTA NUM__219)
	(PROP_SECONDS ID_IRTIMEDELTA NUM__226)
	(PROP_SECONDS ID_IYTIMEDELTA NUM__233)
	(PROP_SECONDS ID_JFTIMEDELTA NUM__240)
	(PROP_SECONDS ID_JMTIMEDELTA NUM__247)
	(PROP_SECONDS ID_JTTIMEDELTA NUM__254)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_DUDOMAINUSER STR__DX)
	(PROP_SID ID_DYDOMAINUSER STR__EB)
	(PROP_SID ID_ECDOMAINUSER STR__EF)
	(PROP_SID ID_EGDOMAINUSER STR__EJ)
	(PROP_SID ID_EKDOMAINUSER STR__EN)
	(PROP_SID ID_EODOMAINUSER STR__ER)
	(PROP_SID ID_ESDOMAINUSER STR__EV)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_EWHOST ID_EXTIMEDELTA)
	(PROP_TIMEDELTA ID_FDHOST ID_FETIMEDELTA)
	(PROP_TIMEDELTA ID_FKHOST ID_FLTIMEDELTA)
	(PROP_TIMEDELTA ID_FRHOST ID_FSTIMEDELTA)
	(PROP_TIMEDELTA ID_FYHOST ID_FZTIMEDELTA)
	(PROP_TIMEDELTA ID_GFHOST ID_GGTIMEDELTA)
	(PROP_TIMEDELTA ID_GMHOST ID_GNTIMEDELTA)
	(PROP_TIMEDELTA ID_GTHOST ID_GUTIMEDELTA)
	(PROP_TIMEDELTA ID_HAHOST ID_HBTIMEDELTA)
	(PROP_TIMEDELTA ID_HHHOST ID_HITIMEDELTA)
	(PROP_TIMEDELTA ID_HOHOST ID_HPTIMEDELTA)
	(PROP_TIMEDELTA ID_HVHOST ID_HWTIMEDELTA)
	(PROP_TIMEDELTA ID_ICHOST ID_IDTIMEDELTA)
	(PROP_TIMEDELTA ID_IJHOST ID_IKTIMEDELTA)
	(PROP_TIMEDELTA ID_IQHOST ID_IRTIMEDELTA)
	(PROP_TIMEDELTA ID_IXHOST ID_IYTIMEDELTA)
	(PROP_TIMEDELTA ID_JEHOST ID_JFTIMEDELTA)
	(PROP_TIMEDELTA ID_JLHOST ID_JMTIMEDELTA)
	(PROP_TIMEDELTA ID_JSHOST ID_JTTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_DVDOMAINCREDENTIAL ID_DUDOMAINUSER)
	(PROP_USER ID_DZDOMAINCREDENTIAL ID_DYDOMAINUSER)
	(PROP_USER ID_EDDOMAINCREDENTIAL ID_ECDOMAINUSER)
	(PROP_USER ID_EHDOMAINCREDENTIAL ID_EGDOMAINUSER)
	(PROP_USER ID_ELDOMAINCREDENTIAL ID_EKDOMAINUSER)
	(PROP_USER ID_EPDOMAINCREDENTIAL ID_EODOMAINUSER)
	(PROP_USER ID_ETDOMAINCREDENTIAL ID_ESDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_DUDOMAINUSER STR__PAUL)
	(PROP_USERNAME ID_DYDOMAINUSER STR__KAREN)
	(PROP_USERNAME ID_ECDOMAINUSER STR__MARK)
	(PROP_USERNAME ID_EGDOMAINUSER STR__BETTY)
	(PROP_USERNAME ID_EKDOMAINUSER STR__DONALD)
	(PROP_USERNAME ID_EODOMAINUSER STR__HELEN)
	(PROP_USERNAME ID_ESDOMAINUSER STR__GEORGE)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_KSRAT ID_FRHOST)(PROP_HOST ID_KKRAT ID_FKHOST)(PROP_HOST ID_KRRAT ID_JSHOST)(PROP_HOST ID_KBRAT ID_FDHOST)(PROP_HOST ID_KMRAT ID_IQHOST)(PROP_HOST ID_KTRAT ID_GMHOST)(PROP_HOST ID_KPRAT ID_GTHOST)(PROP_HOST ID_KJRAT ID_JEHOST)(PROP_HOST ID_KORAT ID_ICHOST)(PROP_HOST ID_KCRAT ID_IXHOST)(PROP_HOST ID_KFRAT ID_EWHOST)(PROP_HOST ID_KQRAT ID_IJHOST)(PROP_HOST ID_KIRAT ID_FYHOST)(PROP_HOST ID_KERAT ID_JLHOST)(PROP_HOST ID_KHRAT ID_HAHOST)(PROP_HOST ID_KLRAT ID_HVHOST)(PROP_HOST ID_KGRAT ID_GFHOST)(PROP_HOST ID_KNRAT ID_HHHOST)))
(:metric minimize (total-cost))

)
