(define (problem p5_hosts_trial_6) (:domain CALDERA)
(:objects
	STR__B - STRING
	STR__BK - STRING
	STR__E - STRING
	STR__JAMES - STRING
	STR__X - STRING
	STR__J - STRING
	STR__O - STRING
	STR__P - STRING
	STR__BL - STRING
	STR__BR - STRING
	STR__BQ - STRING
	STR__F - STRING
	STR__Q - STRING
	STR__I - STRING
	STR__BD - STRING
	STR__BJ - STRING
	STR__V - STRING
	STR__W - STRING
	STR__BC - STRING
	STR__BS - STRING
	STR__MARY - STRING
	STR__BU - STRING
	STR__ALPHA - STRING
	STR__BE - STRING
	NUM__27 - NUM
	NUM__19 - NUM
	NUM__13 - NUM
	NUM__40 - NUM
	NUM__20 - NUM
	NUM__41 - NUM
	NUM__34 - NUM
	NUM__12 - NUM
	NUM__33 - NUM
	NUM__26 - NUM
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CJRAT - OBSERVEDRAT
	ID_CFRAT - OBSERVEDRAT
	ID_BTRAT - OBSERVEDRAT
	ID_CHRAT - OBSERVEDRAT
	ID_CIRAT - OBSERVEDRAT
	ID_CGRAT - OBSERVEDRAT
	ID_BFHOST - OBSERVEDHOST
	ID_YHOST - OBSERVEDHOST
	ID_RHOST - OBSERVEDHOST
	ID_KHOST - OBSERVEDHOST
	ID_BMHOST - OBSERVEDHOST
	ID_BZSHARE - OBSERVEDSHARE
	ID_BYSHARE - OBSERVEDSHARE
	ID_BXSHARE - OBSERVEDSHARE
	ID_BVSHARE - OBSERVEDSHARE
	ID_BWSHARE - OBSERVEDSHARE
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_STIMEDELTA - OBSERVEDTIMEDELTA
	ID_ZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_LTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CDFILE - OBSERVEDFILE
	ID_CAFILE - OBSERVEDFILE
	ID_CEFILE - OBSERVEDFILE
	ID_CBFILE - OBSERVEDFILE
	ID_CCFILE - OBSERVEDFILE
	ID_CLSCHTASK - OBSERVEDSCHTASK
	ID_CMSCHTASK - OBSERVEDSCHTASK
	ID_CKSCHTASK - OBSERVEDSCHTASK
	ID_COSCHTASK - OBSERVEDSCHTASK
	ID_CNSCHTASK - OBSERVEDSCHTASK
)
(:init
	(KNOWS ID_BTRAT)
	(KNOWS ID_RHOST)
	(KNOWS_PROPERTY ID_BTRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_BTRAT PHOST)
	(KNOWS_PROPERTY ID_RHOST PFQDN)
	(MEM_CACHED_DOMAIN_CREDS ID_BFHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BFHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_GDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_KHOST)
	(MEM_HOSTS ID_ADOMAIN ID_RHOST)
	(MEM_HOSTS ID_ADOMAIN ID_YHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_DC ID_BFHOST NO)
	(PROP_DC ID_BMHOST NO)
	(PROP_DC ID_KHOST NO)
	(PROP_DC ID_RHOST NO)
	(PROP_DC ID_YHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BFHOST STR__BK)
	(PROP_DNS_DOMAIN_NAME ID_BMHOST STR__BR)
	(PROP_DNS_DOMAIN_NAME ID_KHOST STR__P)
	(PROP_DNS_DOMAIN_NAME ID_RHOST STR__W)
	(PROP_DNS_DOMAIN_NAME ID_YHOST STR__BD)
	(PROP_DOMAIN ID_BFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_RHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_YHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_BTRAT YES)
	(PROP_EXECUTABLE ID_BTRAT STR__BU)
	(PROP_FQDN ID_BFHOST STR__BL)
	(PROP_FQDN ID_BMHOST STR__BS)
	(PROP_FQDN ID_KHOST STR__Q)
	(PROP_FQDN ID_RHOST STR__X)
	(PROP_FQDN ID_YHOST STR__BE)
	(PROP_HOST ID_BGTIMEDELTA ID_BFHOST)
	(PROP_HOST ID_BNTIMEDELTA ID_BMHOST)
	(PROP_HOST ID_BTRAT ID_RHOST)
	(PROP_HOST ID_LTIMEDELTA ID_KHOST)
	(PROP_HOST ID_STIMEDELTA ID_RHOST)
	(PROP_HOST ID_ZTIMEDELTA ID_YHOST)
	(PROP_HOSTNAME ID_BFHOST STR__BJ)
	(PROP_HOSTNAME ID_BMHOST STR__BQ)
	(PROP_HOSTNAME ID_KHOST STR__O)
	(PROP_HOSTNAME ID_RHOST STR__V)
	(PROP_HOSTNAME ID_YHOST STR__BC)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BGTIMEDELTA NUM__33)
	(PROP_MICROSECONDS ID_BNTIMEDELTA NUM__40)
	(PROP_MICROSECONDS ID_LTIMEDELTA NUM__12)
	(PROP_MICROSECONDS ID_STIMEDELTA NUM__19)
	(PROP_MICROSECONDS ID_ZTIMEDELTA NUM__26)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_SECONDS ID_BGTIMEDELTA NUM__34)
	(PROP_SECONDS ID_BNTIMEDELTA NUM__41)
	(PROP_SECONDS ID_LTIMEDELTA NUM__13)
	(PROP_SECONDS ID_STIMEDELTA NUM__20)
	(PROP_SECONDS ID_ZTIMEDELTA NUM__27)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_TIMEDELTA ID_BFHOST ID_BGTIMEDELTA)
	(PROP_TIMEDELTA ID_BMHOST ID_BNTIMEDELTA)
	(PROP_TIMEDELTA ID_KHOST ID_LTIMEDELTA)
	(PROP_TIMEDELTA ID_RHOST ID_STIMEDELTA)
	(PROP_TIMEDELTA ID_YHOST ID_ZTIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_CFRAT ID_YHOST)(PROP_HOST ID_CHRAT ID_BFHOST)(PROP_HOST ID_CJRAT ID_KHOST)(PROP_HOST ID_CGRAT ID_BMHOST)))
(:metric minimize (total-cost))

)
