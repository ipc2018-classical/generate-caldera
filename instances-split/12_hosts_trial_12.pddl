(define (problem p12_hosts_trial_12) (:domain CALDERA)
(:objects
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_GUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DVTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ECTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FETIMEDELTA - OBSERVEDTIMEDELTA
	ID_GNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EPHOST - OBSERVEDHOST
	ID_FYHOST - OBSERVEDHOST
	ID_FKHOST - OBSERVEDHOST
	ID_EIHOST - OBSERVEDHOST
	ID_EWHOST - OBSERVEDHOST
	ID_DUHOST - OBSERVEDHOST
	ID_GMHOST - OBSERVEDHOST
	ID_GFHOST - OBSERVEDHOST
	ID_FDHOST - OBSERVEDHOST
	ID_GTHOST - OBSERVEDHOST
	ID_FRHOST - OBSERVEDHOST
	ID_EBHOST - OBSERVEDHOST
	STR__SUSAN - STRING
	STR__GZ - STRING
	STR__MICHAEL - STRING
	STR__EG - STRING
	STR__BD - STRING
	STR__ELIZABETH - STRING
	STR__E - STRING
	STR__CE - STRING
	STR__CHARLES - STRING
	STR__LISA - STRING
	STR__DK - STRING
	STR__BG - STRING
	STR__THOMAS - STRING
	STR__FV - STRING
	STR__CZ - STRING
	STR__GS - STRING
	STR__R - STRING
	STR__EO - STRING
	STR__ROBERT - STRING
	STR__DAVID - STRING
	STR__FI - STRING
	STR__BP - STRING
	STR__BX - STRING
	STR__CY - STRING
	STR__MARIA - STRING
	STR__CI - STRING
	STR__GJ - STRING
	STR__CM - STRING
	STR__GE - STRING
	STR__GR - STRING
	STR__FW - STRING
	STR__EF - STRING
	STR__DD - STRING
	STR__FX - STRING
	STR__DH - STRING
	STR__CU - STRING
	STR__CV - STRING
	STR__CB - STRING
	STR__FH - STRING
	STR__JOHN - STRING
	STR__DOROTHY - STRING
	STR__DANIEL - STRING
	STR__BARBARA - STRING
	STR__ET - STRING
	STR__BC - STRING
	STR__EN - STRING
	STR__FJ - STRING
	STR__FC - STRING
	STR__GQ - STRING
	STR__DY - STRING
	STR__CQ - STRING
	STR__BS - STRING
	STR__BT - STRING
	STR__FQ - STRING
	STR__I - STRING
	STR__U - STRING
	STR__FB - STRING
	STR__GL - STRING
	STR__GK - STRING
	STR__EM - STRING
	STR__DT - STRING
	STR__DO - STRING
	STR__M - STRING
	STR__BO - STRING
	STR__FA - STRING
	STR__GC - STRING
	STR__BL - STRING
	STR__RICHARD - STRING
	STR__DS - STRING
	STR__DP - STRING
	STR__DL - STRING
	STR__PATRICIA - STRING
	STR__DZ - STRING
	STR__FO - STRING
	STR__F - STRING
	STR__Q - STRING
	STR__DG - STRING
	STR__JENNIFER - STRING
	STR__DC - STRING
	STR__Z - STRING
	STR__CA - STRING
	STR__BW - STRING
	STR__MARY - STRING
	STR__GY - STRING
	STR__V - STRING
	STR__EU - STRING
	STR__Y - STRING
	STR__EV - STRING
	STR__CJ - STRING
	STR__JAMES - STRING
	STR__ALPHA - STRING
	STR__CHRISTOPHER - STRING
	STR__GD - STRING
	STR__BK - STRING
	STR__EA - STRING
	STR__LINDA - STRING
	STR__BH - STRING
	STR__EH - STRING
	STR__J - STRING
	STR__CN - STRING
	STR__FP - STRING
	STR__MARGARET - STRING
	STR__CR - STRING
	STR__CF - STRING
	STR__NANCY - STRING
	STR__GX - STRING
	STR__WILLIAM - STRING
	STR__B - STRING
	STR__N - STRING
	STR__JOSEPH - STRING
	STR__HB - STRING
	ID_IKSHARE - OBSERVEDSHARE
	ID_IESHARE - OBSERVEDSHARE
	ID_IJSHARE - OBSERVEDSHARE
	ID_IGSHARE - OBSERVEDSHARE
	ID_IDSHARE - OBSERVEDSHARE
	ID_IISHARE - OBSERVEDSHARE
	ID_IHSHARE - OBSERVEDSHARE
	ID_IASHARE - OBSERVEDSHARE
	ID_IFSHARE - OBSERVEDSHARE
	ID_IBSHARE - OBSERVEDSHARE
	ID_ILSHARE - OBSERVEDSHARE
	ID_ICSHARE - OBSERVEDSHARE
	ID_HTSCHTASK - OBSERVEDSCHTASK
	ID_HUSCHTASK - OBSERVEDSCHTASK
	ID_HQSCHTASK - OBSERVEDSCHTASK
	ID_HRSCHTASK - OBSERVEDSCHTASK
	ID_HOSCHTASK - OBSERVEDSCHTASK
	ID_HWSCHTASK - OBSERVEDSCHTASK
	ID_HZSCHTASK - OBSERVEDSCHTASK
	ID_HVSCHTASK - OBSERVEDSCHTASK
	ID_HSSCHTASK - OBSERVEDSCHTASK
	ID_HPSCHTASK - OBSERVEDSCHTASK
	ID_HXSCHTASK - OBSERVEDSCHTASK
	ID_HYSCHTASK - OBSERVEDSCHTASK
	ID_ISFILE - OBSERVEDFILE
	ID_IPFILE - OBSERVEDFILE
	ID_IRFILE - OBSERVEDFILE
	ID_IMFILE - OBSERVEDFILE
	ID_ITFILE - OBSERVEDFILE
	ID_IVFILE - OBSERVEDFILE
	ID_IWFILE - OBSERVEDFILE
	ID_IQFILE - OBSERVEDFILE
	ID_IUFILE - OBSERVEDFILE
	ID_IXFILE - OBSERVEDFILE
	ID_IOFILE - OBSERVEDFILE
	ID_INFILE - OBSERVEDFILE
	NUM__121 - NUM
	NUM__101 - NUM
	NUM__142 - NUM
	NUM__150 - NUM
	NUM__136 - NUM
	NUM__164 - NUM
	NUM__115 - NUM
	NUM__149 - NUM
	NUM__129 - NUM
	NUM__157 - NUM
	NUM__170 - NUM
	NUM__177 - NUM
	NUM__135 - NUM
	NUM__100 - NUM
	NUM__107 - NUM
	NUM__171 - NUM
	NUM__114 - NUM
	NUM__108 - NUM
	NUM__128 - NUM
	NUM__163 - NUM
	NUM__122 - NUM
	NUM__143 - NUM
	NUM__178 - NUM
	NUM__156 - NUM
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_HCRAT - OBSERVEDRAT
	ID_HHRAT - OBSERVEDRAT
	ID_HLRAT - OBSERVEDRAT
	ID_HERAT - OBSERVEDRAT
	ID_HJRAT - OBSERVEDRAT
	ID_HARAT - OBSERVEDRAT
	ID_HKRAT - OBSERVEDRAT
	ID_HFRAT - OBSERVEDRAT
	ID_HIRAT - OBSERVEDRAT
	ID_HGRAT - OBSERVEDRAT
	ID_HDRAT - OBSERVEDRAT
	ID_HNRAT - OBSERVEDRAT
	ID_HMRAT - OBSERVEDRAT
)
(:init
	(KNOWS ID_EWHOST)
	(KNOWS ID_HARAT)
	(KNOWS_PROPERTY ID_EWHOST PFQDN)
	(KNOWS_PROPERTY ID_HARAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_HARAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_TDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_DIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_CODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_CODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_WDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_DUHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EBHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GTHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_DUHOST NO)
	(PROP_DC ID_EBHOST NO)
	(PROP_DC ID_EIHOST NO)
	(PROP_DC ID_EPHOST NO)
	(PROP_DC ID_EWHOST NO)
	(PROP_DC ID_FDHOST NO)
	(PROP_DC ID_FKHOST NO)
	(PROP_DC ID_FRHOST NO)
	(PROP_DC ID_FYHOST NO)
	(PROP_DC ID_GFHOST NO)
	(PROP_DC ID_GMHOST YES)
	(PROP_DC ID_GTHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_DUHOST STR__EA)
	(PROP_DNS_DOMAIN_NAME ID_EBHOST STR__EH)
	(PROP_DNS_DOMAIN_NAME ID_EIHOST STR__EO)
	(PROP_DNS_DOMAIN_NAME ID_EPHOST STR__EV)
	(PROP_DNS_DOMAIN_NAME ID_EWHOST STR__FC)
	(PROP_DNS_DOMAIN_NAME ID_FDHOST STR__FJ)
	(PROP_DNS_DOMAIN_NAME ID_FKHOST STR__FQ)
	(PROP_DNS_DOMAIN_NAME ID_FRHOST STR__FX)
	(PROP_DNS_DOMAIN_NAME ID_FYHOST STR__GE)
	(PROP_DNS_DOMAIN_NAME ID_GFHOST STR__GL)
	(PROP_DNS_DOMAIN_NAME ID_GMHOST STR__GS)
	(PROP_DNS_DOMAIN_NAME ID_GTHOST STR__GZ)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_HARAT YES)
	(PROP_EXECUTABLE ID_HARAT STR__HB)
	(PROP_FQDN ID_DUHOST STR__DY)
	(PROP_FQDN ID_EBHOST STR__EF)
	(PROP_FQDN ID_EIHOST STR__EM)
	(PROP_FQDN ID_EPHOST STR__ET)
	(PROP_FQDN ID_EWHOST STR__FA)
	(PROP_FQDN ID_FDHOST STR__FH)
	(PROP_FQDN ID_FKHOST STR__FO)
	(PROP_FQDN ID_FRHOST STR__FV)
	(PROP_FQDN ID_FYHOST STR__GC)
	(PROP_FQDN ID_GFHOST STR__GJ)
	(PROP_FQDN ID_GMHOST STR__GQ)
	(PROP_FQDN ID_GTHOST STR__GX)
	(PROP_HOST ID_DVTIMEDELTA ID_DUHOST)
	(PROP_HOST ID_ECTIMEDELTA ID_EBHOST)
	(PROP_HOST ID_EJTIMEDELTA ID_EIHOST)
	(PROP_HOST ID_EQTIMEDELTA ID_EPHOST)
	(PROP_HOST ID_EXTIMEDELTA ID_EWHOST)
	(PROP_HOST ID_FETIMEDELTA ID_FDHOST)
	(PROP_HOST ID_FLTIMEDELTA ID_FKHOST)
	(PROP_HOST ID_FSTIMEDELTA ID_FRHOST)
	(PROP_HOST ID_FZTIMEDELTA ID_FYHOST)
	(PROP_HOST ID_GGTIMEDELTA ID_GFHOST)
	(PROP_HOST ID_GNTIMEDELTA ID_GMHOST)
	(PROP_HOST ID_GUTIMEDELTA ID_GTHOST)
	(PROP_HOST ID_HARAT ID_EWHOST)
	(PROP_HOSTNAME ID_DUHOST STR__DZ)
	(PROP_HOSTNAME ID_EBHOST STR__EG)
	(PROP_HOSTNAME ID_EIHOST STR__EN)
	(PROP_HOSTNAME ID_EPHOST STR__EU)
	(PROP_HOSTNAME ID_EWHOST STR__FB)
	(PROP_HOSTNAME ID_FDHOST STR__FI)
	(PROP_HOSTNAME ID_FKHOST STR__FP)
	(PROP_HOSTNAME ID_FRHOST STR__FW)
	(PROP_HOSTNAME ID_FYHOST STR__GD)
	(PROP_HOSTNAME ID_GFHOST STR__GK)
	(PROP_HOSTNAME ID_GMHOST STR__GR)
	(PROP_HOSTNAME ID_GTHOST STR__GY)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_DVTIMEDELTA NUM__101)
	(PROP_MICROSECONDS ID_ECTIMEDELTA NUM__108)
	(PROP_MICROSECONDS ID_EJTIMEDELTA NUM__115)
	(PROP_MICROSECONDS ID_EQTIMEDELTA NUM__122)
	(PROP_MICROSECONDS ID_EXTIMEDELTA NUM__129)
	(PROP_MICROSECONDS ID_FETIMEDELTA NUM__136)
	(PROP_MICROSECONDS ID_FLTIMEDELTA NUM__143)
	(PROP_MICROSECONDS ID_FSTIMEDELTA NUM__150)
	(PROP_MICROSECONDS ID_FZTIMEDELTA NUM__157)
	(PROP_MICROSECONDS ID_GGTIMEDELTA NUM__164)
	(PROP_MICROSECONDS ID_GNTIMEDELTA NUM__171)
	(PROP_MICROSECONDS ID_GUTIMEDELTA NUM__178)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_DVTIMEDELTA NUM__100)
	(PROP_SECONDS ID_ECTIMEDELTA NUM__107)
	(PROP_SECONDS ID_EJTIMEDELTA NUM__114)
	(PROP_SECONDS ID_EQTIMEDELTA NUM__121)
	(PROP_SECONDS ID_EXTIMEDELTA NUM__128)
	(PROP_SECONDS ID_FETIMEDELTA NUM__135)
	(PROP_SECONDS ID_FLTIMEDELTA NUM__142)
	(PROP_SECONDS ID_FSTIMEDELTA NUM__149)
	(PROP_SECONDS ID_FZTIMEDELTA NUM__156)
	(PROP_SECONDS ID_GGTIMEDELTA NUM__163)
	(PROP_SECONDS ID_GNTIMEDELTA NUM__170)
	(PROP_SECONDS ID_GUTIMEDELTA NUM__177)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_DUHOST ID_DVTIMEDELTA)
	(PROP_TIMEDELTA ID_EBHOST ID_ECTIMEDELTA)
	(PROP_TIMEDELTA ID_EIHOST ID_EJTIMEDELTA)
	(PROP_TIMEDELTA ID_EPHOST ID_EQTIMEDELTA)
	(PROP_TIMEDELTA ID_EWHOST ID_EXTIMEDELTA)
	(PROP_TIMEDELTA ID_FDHOST ID_FETIMEDELTA)
	(PROP_TIMEDELTA ID_FKHOST ID_FLTIMEDELTA)
	(PROP_TIMEDELTA ID_FRHOST ID_FSTIMEDELTA)
	(PROP_TIMEDELTA ID_FYHOST ID_FZTIMEDELTA)
	(PROP_TIMEDELTA ID_GFHOST ID_GGTIMEDELTA)
	(PROP_TIMEDELTA ID_GMHOST ID_GNTIMEDELTA)
	(PROP_TIMEDELTA ID_GTHOST ID_GUTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_HDRAT ID_FRHOST)(PROP_HOST ID_HGRAT ID_GTHOST)(PROP_HOST ID_HIRAT ID_FDHOST)(PROP_HOST ID_HFRAT ID_GFHOST)(PROP_HOST ID_HKRAT ID_GMHOST)(PROP_HOST ID_HJRAT ID_DUHOST)(PROP_HOST ID_HERAT ID_EIHOST)(PROP_HOST ID_HLRAT ID_FKHOST)(PROP_HOST ID_HHRAT ID_FYHOST)(PROP_HOST ID_HCRAT ID_EPHOST)(PROP_HOST ID_HMRAT ID_EBHOST)))
(:metric minimize (total-cost))

)
