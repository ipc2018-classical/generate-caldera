(define (problem p14_hosts_trial_11) (:domain CALDERA)
(:objects
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_GVHOST - OBSERVEDHOST
	ID_FFHOST - OBSERVEDHOST
	ID_GOHOST - OBSERVEDHOST
	ID_FTHOST - OBSERVEDHOST
	ID_EYHOST - OBSERVEDHOST
	ID_FMHOST - OBSERVEDHOST
	ID_GAHOST - OBSERVEDHOST
	ID_GHHOST - OBSERVEDHOST
	ID_HJHOST - OBSERVEDHOST
	ID_HXHOST - OBSERVEDHOST
	ID_EKHOST - OBSERVEDHOST
	ID_HCHOST - OBSERVEDHOST
	ID_ERHOST - OBSERVEDHOST
	ID_HQHOST - OBSERVEDHOST
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_IURAT - OBSERVEDRAT
	ID_JGRAT - OBSERVEDRAT
	ID_JCRAT - OBSERVEDRAT
	ID_JFRAT - OBSERVEDRAT
	ID_JBRAT - OBSERVEDRAT
	ID_JARAT - OBSERVEDRAT
	ID_IZRAT - OBSERVEDRAT
	ID_JERAT - OBSERVEDRAT
	ID_JHRAT - OBSERVEDRAT
	ID_JDRAT - OBSERVEDRAT
	ID_IXRAT - OBSERVEDRAT
	ID_IVRAT - OBSERVEDRAT
	ID_IWRAT - OBSERVEDRAT
	ID_IERAT - OBSERVEDRAT
	ID_IYRAT - OBSERVEDRAT
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ECDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_IRSHARE - OBSERVEDSHARE
	ID_IQSHARE - OBSERVEDSHARE
	ID_IPSHARE - OBSERVEDSHARE
	ID_IOSHARE - OBSERVEDSHARE
	ID_IGSHARE - OBSERVEDSHARE
	ID_ISSHARE - OBSERVEDSHARE
	ID_IKSHARE - OBSERVEDSHARE
	ID_IHSHARE - OBSERVEDSHARE
	ID_IMSHARE - OBSERVEDSHARE
	ID_ILSHARE - OBSERVEDSHARE
	ID_IJSHARE - OBSERVEDSHARE
	ID_IISHARE - OBSERVEDSHARE
	ID_ITSHARE - OBSERVEDSHARE
	ID_INSHARE - OBSERVEDSHARE
	ID_HYTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HKTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ESTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GITIMEDELTA - OBSERVEDTIMEDELTA
	ID_GWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ELTIMEDELTA - OBSERVEDTIMEDELTA
	ID_KBSCHTASK - OBSERVEDSCHTASK
	ID_JWSCHTASK - OBSERVEDSCHTASK
	ID_KHSCHTASK - OBSERVEDSCHTASK
	ID_KESCHTASK - OBSERVEDSCHTASK
	ID_JXSCHTASK - OBSERVEDSCHTASK
	ID_KASCHTASK - OBSERVEDSCHTASK
	ID_KFSCHTASK - OBSERVEDSCHTASK
	ID_KGSCHTASK - OBSERVEDSCHTASK
	ID_KISCHTASK - OBSERVEDSCHTASK
	ID_KJSCHTASK - OBSERVEDSCHTASK
	ID_JYSCHTASK - OBSERVEDSCHTASK
	ID_KDSCHTASK - OBSERVEDSCHTASK
	ID_KCSCHTASK - OBSERVEDSCHTASK
	ID_JZSCHTASK - OBSERVEDSCHTASK
	NUM__201 - NUM
	NUM__207 - NUM
	NUM__208 - NUM
	NUM__137 - NUM
	NUM__200 - NUM
	NUM__194 - NUM
	NUM__193 - NUM
	NUM__172 - NUM
	NUM__186 - NUM
	NUM__158 - NUM
	NUM__144 - NUM
	NUM__123 - NUM
	NUM__152 - NUM
	NUM__130 - NUM
	NUM__131 - NUM
	NUM__151 - NUM
	NUM__117 - NUM
	NUM__159 - NUM
	NUM__173 - NUM
	NUM__124 - NUM
	NUM__180 - NUM
	NUM__187 - NUM
	NUM__145 - NUM
	NUM__166 - NUM
	NUM__116 - NUM
	NUM__179 - NUM
	NUM__138 - NUM
	NUM__165 - NUM
	ID_JPFILE - OBSERVEDFILE
	ID_JNFILE - OBSERVEDFILE
	ID_JKFILE - OBSERVEDFILE
	ID_JLFILE - OBSERVEDFILE
	ID_JJFILE - OBSERVEDFILE
	ID_JRFILE - OBSERVEDFILE
	ID_JOFILE - OBSERVEDFILE
	ID_JSFILE - OBSERVEDFILE
	ID_JIFILE - OBSERVEDFILE
	ID_JUFILE - OBSERVEDFILE
	ID_JMFILE - OBSERVEDFILE
	ID_JQFILE - OBSERVEDFILE
	ID_JTFILE - OBSERVEDFILE
	ID_JVFILE - OBSERVEDFILE
	STR__GG - STRING
	STR__NANCY - STRING
	STR__FE - STRING
	STR__Y - STRING
	STR__GS - STRING
	STR__BG - STRING
	STR__DO - STRING
	STR__CR - STRING
	STR__FD - STRING
	STR__J - STRING
	STR__BL - STRING
	STR__GN - STRING
	STR__M - STRING
	STR__HN - STRING
	STR__EW - STRING
	STR__CB - STRING
	STR__BK - STRING
	STR__FZ - STRING
	STR__DX - STRING
	STR__MARIA - STRING
	STR__BC - STRING
	STR__CV - STRING
	STR__BT - STRING
	STR__HH - STRING
	STR__F - STRING
	STR__CF - STRING
	STR__CM - STRING
	STR__DC - STRING
	STR__DL - STRING
	STR__HU - STRING
	STR__HG - STRING
	STR__I - STRING
	STR__EV - STRING
	STR__GT - STRING
	STR__HB - STRING
	STR__ALPHA - STRING
	STR__ROBERT - STRING
	STR__CA - STRING
	STR__CY - STRING
	STR__FQ - STRING
	STR__IB - STRING
	STR__DK - STRING
	STR__EO - STRING
	STR__GF - STRING
	STR__DT - STRING
	STR__FL - STRING
	STR__ID - STRING
	STR__PATRICIA - STRING
	STR__DS - STRING
	STR__JENNIFER - STRING
	STR__ELIZABETH - STRING
	STR__BD - STRING
	STR__U - STRING
	STR__MARK - STRING
	STR__DH - STRING
	STR__BX - STRING
	STR__CZ - STRING
	STR__RICHARD - STRING
	STR__BP - STRING
	STR__GM - STRING
	STR__GZ - STRING
	STR__WILLIAM - STRING
	STR__EJ - STRING
	STR__BH - STRING
	STR__JAMES - STRING
	STR__DG - STRING
	STR__HO - STRING
	STR__FX - STRING
	STR__E - STRING
	STR__FR - STRING
	STR__BO - STRING
	STR__SUSAN - STRING
	STR__GL - STRING
	STR__EP - STRING
	STR__DP - STRING
	STR__BW - STRING
	STR__GU - STRING
	STR__HP - STRING
	STR__CHRISTOPHER - STRING
	STR__EQ - STRING
	STR__FJ - STRING
	STR__HV - STRING
	STR__JOSEPH - STRING
	STR__Z - STRING
	STR__HI - STRING
	STR__GE - STRING
	STR__MARY - STRING
	STR__IC - STRING
	STR__EB - STRING
	STR__DAVID - STRING
	STR__JOHN - STRING
	STR__CE - STRING
	STR__HA - STRING
	STR__EE - STRING
	STR__EA - STRING
	STR__Q - STRING
	STR__FY - STRING
	STR__V - STRING
	STR__CU - STRING
	STR__DOROTHY - STRING
	STR__CI - STRING
	STR__EI - STRING
	STR__IF - STRING
	STR__CN - STRING
	STR__FK - STRING
	STR__PAUL - STRING
	STR__BETTY - STRING
	STR__B - STRING
	STR__CHARLES - STRING
	STR__CQ - STRING
	STR__THOMAS - STRING
	STR__MARGARET - STRING
	STR__EX - STRING
	STR__KAREN - STRING
	STR__FS - STRING
	STR__CJ - STRING
	STR__BARBARA - STRING
	STR__MICHAEL - STRING
	STR__BS - STRING
	STR__N - STRING
	STR__R - STRING
	STR__DD - STRING
	STR__LINDA - STRING
	STR__EF - STRING
	STR__FC - STRING
	STR__HW - STRING
	STR__DW - STRING
	STR__DANIEL - STRING
	STR__LISA - STRING
)
(:init
	(KNOWS ID_HJHOST)
	(KNOWS ID_IERAT)
	(KNOWS_PROPERTY ID_HJHOST PFQDN)
	(KNOWS_PROPERTY ID_IERAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_IERAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_EHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_DJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GVHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GVHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GVHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HCHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HCHOST ID_DNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HCHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HJHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HJHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HJHOST ID_EHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HQHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HQHOST ID_DRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HQHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HXHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HXHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_HXHOST ID_DNDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_CODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_DQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_CCDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_BQDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_CCDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_CKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GVHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GVHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HCHOST ID_DIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HCHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HJHOST ID_DIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HJHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HQHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HQHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HXHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_HXHOST ID_BYDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_EKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ERHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GOHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HCHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HJHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HQHOST)
	(MEM_HOSTS ID_ADOMAIN ID_HXHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_DUDOMAINUSER ID_DVDOMAINCREDENTIAL)
	(PROP_CRED ID_DYDOMAINUSER ID_DZDOMAINCREDENTIAL)
	(PROP_CRED ID_ECDOMAINUSER ID_EDDOMAINCREDENTIAL)
	(PROP_CRED ID_EGDOMAINUSER ID_EHDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_EKHOST NO)
	(PROP_DC ID_ERHOST NO)
	(PROP_DC ID_EYHOST YES)
	(PROP_DC ID_FFHOST NO)
	(PROP_DC ID_FMHOST NO)
	(PROP_DC ID_FTHOST NO)
	(PROP_DC ID_GAHOST NO)
	(PROP_DC ID_GHHOST NO)
	(PROP_DC ID_GOHOST YES)
	(PROP_DC ID_GVHOST NO)
	(PROP_DC ID_HCHOST NO)
	(PROP_DC ID_HJHOST NO)
	(PROP_DC ID_HQHOST NO)
	(PROP_DC ID_HXHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_EKHOST STR__EQ)
	(PROP_DNS_DOMAIN_NAME ID_ERHOST STR__EX)
	(PROP_DNS_DOMAIN_NAME ID_EYHOST STR__FE)
	(PROP_DNS_DOMAIN_NAME ID_FFHOST STR__FL)
	(PROP_DNS_DOMAIN_NAME ID_FMHOST STR__FS)
	(PROP_DNS_DOMAIN_NAME ID_FTHOST STR__FZ)
	(PROP_DNS_DOMAIN_NAME ID_GAHOST STR__GG)
	(PROP_DNS_DOMAIN_NAME ID_GHHOST STR__GN)
	(PROP_DNS_DOMAIN_NAME ID_GOHOST STR__GU)
	(PROP_DNS_DOMAIN_NAME ID_GVHOST STR__HB)
	(PROP_DNS_DOMAIN_NAME ID_HCHOST STR__HI)
	(PROP_DNS_DOMAIN_NAME ID_HJHOST STR__HP)
	(PROP_DNS_DOMAIN_NAME ID_HQHOST STR__HW)
	(PROP_DNS_DOMAIN_NAME ID_HXHOST STR__ID)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ECDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ERHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_HCHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HJHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HXHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_IERAT YES)
	(PROP_EXECUTABLE ID_IERAT STR__IF)
	(PROP_FQDN ID_EKHOST STR__EP)
	(PROP_FQDN ID_ERHOST STR__EW)
	(PROP_FQDN ID_EYHOST STR__FD)
	(PROP_FQDN ID_FFHOST STR__FK)
	(PROP_FQDN ID_FMHOST STR__FR)
	(PROP_FQDN ID_FTHOST STR__FY)
	(PROP_FQDN ID_GAHOST STR__GF)
	(PROP_FQDN ID_GHHOST STR__GM)
	(PROP_FQDN ID_GOHOST STR__GT)
	(PROP_FQDN ID_GVHOST STR__HA)
	(PROP_FQDN ID_HCHOST STR__HH)
	(PROP_FQDN ID_HJHOST STR__HO)
	(PROP_FQDN ID_HQHOST STR__HV)
	(PROP_FQDN ID_HXHOST STR__IC)
	(PROP_HOST ID_ELTIMEDELTA ID_EKHOST)
	(PROP_HOST ID_ESTIMEDELTA ID_ERHOST)
	(PROP_HOST ID_EZTIMEDELTA ID_EYHOST)
	(PROP_HOST ID_FGTIMEDELTA ID_FFHOST)
	(PROP_HOST ID_FNTIMEDELTA ID_FMHOST)
	(PROP_HOST ID_FUTIMEDELTA ID_FTHOST)
	(PROP_HOST ID_GBTIMEDELTA ID_GAHOST)
	(PROP_HOST ID_GITIMEDELTA ID_GHHOST)
	(PROP_HOST ID_GPTIMEDELTA ID_GOHOST)
	(PROP_HOST ID_GWTIMEDELTA ID_GVHOST)
	(PROP_HOST ID_HDTIMEDELTA ID_HCHOST)
	(PROP_HOST ID_HKTIMEDELTA ID_HJHOST)
	(PROP_HOST ID_HRTIMEDELTA ID_HQHOST)
	(PROP_HOST ID_HYTIMEDELTA ID_HXHOST)
	(PROP_HOST ID_IERAT ID_HJHOST)
	(PROP_HOSTNAME ID_EKHOST STR__EO)
	(PROP_HOSTNAME ID_ERHOST STR__EV)
	(PROP_HOSTNAME ID_EYHOST STR__FC)
	(PROP_HOSTNAME ID_FFHOST STR__FJ)
	(PROP_HOSTNAME ID_FMHOST STR__FQ)
	(PROP_HOSTNAME ID_FTHOST STR__FX)
	(PROP_HOSTNAME ID_GAHOST STR__GE)
	(PROP_HOSTNAME ID_GHHOST STR__GL)
	(PROP_HOSTNAME ID_GOHOST STR__GS)
	(PROP_HOSTNAME ID_GVHOST STR__GZ)
	(PROP_HOSTNAME ID_HCHOST STR__HG)
	(PROP_HOSTNAME ID_HJHOST STR__HN)
	(PROP_HOSTNAME ID_HQHOST STR__HU)
	(PROP_HOSTNAME ID_HXHOST STR__IB)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_DUDOMAINUSER NO)
	(PROP_IS_GROUP ID_DYDOMAINUSER NO)
	(PROP_IS_GROUP ID_ECDOMAINUSER NO)
	(PROP_IS_GROUP ID_EGDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_ELTIMEDELTA NUM__116)
	(PROP_MICROSECONDS ID_ESTIMEDELTA NUM__123)
	(PROP_MICROSECONDS ID_EZTIMEDELTA NUM__130)
	(PROP_MICROSECONDS ID_FGTIMEDELTA NUM__137)
	(PROP_MICROSECONDS ID_FNTIMEDELTA NUM__144)
	(PROP_MICROSECONDS ID_FUTIMEDELTA NUM__151)
	(PROP_MICROSECONDS ID_GBTIMEDELTA NUM__158)
	(PROP_MICROSECONDS ID_GITIMEDELTA NUM__165)
	(PROP_MICROSECONDS ID_GPTIMEDELTA NUM__172)
	(PROP_MICROSECONDS ID_GWTIMEDELTA NUM__179)
	(PROP_MICROSECONDS ID_HDTIMEDELTA NUM__186)
	(PROP_MICROSECONDS ID_HKTIMEDELTA NUM__193)
	(PROP_MICROSECONDS ID_HRTIMEDELTA NUM__200)
	(PROP_MICROSECONDS ID_HYTIMEDELTA NUM__207)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_DVDOMAINCREDENTIAL STR__DW)
	(PROP_PASSWORD ID_DZDOMAINCREDENTIAL STR__EA)
	(PROP_PASSWORD ID_EDDOMAINCREDENTIAL STR__EE)
	(PROP_PASSWORD ID_EHDOMAINCREDENTIAL STR__EI)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_ELTIMEDELTA NUM__117)
	(PROP_SECONDS ID_ESTIMEDELTA NUM__124)
	(PROP_SECONDS ID_EZTIMEDELTA NUM__131)
	(PROP_SECONDS ID_FGTIMEDELTA NUM__138)
	(PROP_SECONDS ID_FNTIMEDELTA NUM__145)
	(PROP_SECONDS ID_FUTIMEDELTA NUM__152)
	(PROP_SECONDS ID_GBTIMEDELTA NUM__159)
	(PROP_SECONDS ID_GITIMEDELTA NUM__166)
	(PROP_SECONDS ID_GPTIMEDELTA NUM__173)
	(PROP_SECONDS ID_GWTIMEDELTA NUM__180)
	(PROP_SECONDS ID_HDTIMEDELTA NUM__187)
	(PROP_SECONDS ID_HKTIMEDELTA NUM__194)
	(PROP_SECONDS ID_HRTIMEDELTA NUM__201)
	(PROP_SECONDS ID_HYTIMEDELTA NUM__208)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_DUDOMAINUSER STR__DX)
	(PROP_SID ID_DYDOMAINUSER STR__EB)
	(PROP_SID ID_ECDOMAINUSER STR__EF)
	(PROP_SID ID_EGDOMAINUSER STR__EJ)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_EKHOST ID_ELTIMEDELTA)
	(PROP_TIMEDELTA ID_ERHOST ID_ESTIMEDELTA)
	(PROP_TIMEDELTA ID_EYHOST ID_EZTIMEDELTA)
	(PROP_TIMEDELTA ID_FFHOST ID_FGTIMEDELTA)
	(PROP_TIMEDELTA ID_FMHOST ID_FNTIMEDELTA)
	(PROP_TIMEDELTA ID_FTHOST ID_FUTIMEDELTA)
	(PROP_TIMEDELTA ID_GAHOST ID_GBTIMEDELTA)
	(PROP_TIMEDELTA ID_GHHOST ID_GITIMEDELTA)
	(PROP_TIMEDELTA ID_GOHOST ID_GPTIMEDELTA)
	(PROP_TIMEDELTA ID_GVHOST ID_GWTIMEDELTA)
	(PROP_TIMEDELTA ID_HCHOST ID_HDTIMEDELTA)
	(PROP_TIMEDELTA ID_HJHOST ID_HKTIMEDELTA)
	(PROP_TIMEDELTA ID_HQHOST ID_HRTIMEDELTA)
	(PROP_TIMEDELTA ID_HXHOST ID_HYTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_DVDOMAINCREDENTIAL ID_DUDOMAINUSER)
	(PROP_USER ID_DZDOMAINCREDENTIAL ID_DYDOMAINUSER)
	(PROP_USER ID_EDDOMAINCREDENTIAL ID_ECDOMAINUSER)
	(PROP_USER ID_EHDOMAINCREDENTIAL ID_EGDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_DUDOMAINUSER STR__PAUL)
	(PROP_USERNAME ID_DYDOMAINUSER STR__KAREN)
	(PROP_USERNAME ID_ECDOMAINUSER STR__MARK)
	(PROP_USERNAME ID_EGDOMAINUSER STR__BETTY)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_IVRAT ID_ERHOST)(PROP_HOST ID_IXRAT ID_HCHOST)(PROP_HOST ID_JDRAT ID_EKHOST)(PROP_HOST ID_JHRAT ID_HXHOST)(PROP_HOST ID_JERAT ID_GHHOST)(PROP_HOST ID_IZRAT ID_GAHOST)(PROP_HOST ID_JARAT ID_FMHOST)(PROP_HOST ID_JBRAT ID_EYHOST)(PROP_HOST ID_JFRAT ID_FTHOST)(PROP_HOST ID_JCRAT ID_GOHOST)(PROP_HOST ID_JGRAT ID_FFHOST)(PROP_HOST ID_IURAT ID_GVHOST)(PROP_HOST ID_IYRAT ID_HQHOST)))
(:metric minimize (total-cost))

)
