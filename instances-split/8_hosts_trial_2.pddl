(define (problem p8_hosts_trial_2) (:domain CALDERA)
(:objects
	ID_CBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CITIMEDELTA - OBSERVEDTIMEDELTA
	ID_BGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_STIMEDELTA - OBSERVEDTIMEDELTA
	ID_BUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_LTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CSFILE - OBSERVEDFILE
	ID_CWFILE - OBSERVEDFILE
	ID_CQFILE - OBSERVEDFILE
	ID_CUFILE - OBSERVEDFILE
	ID_CXFILE - OBSERVEDFILE
	ID_CVFILE - OBSERVEDFILE
	ID_CTFILE - OBSERVEDFILE
	ID_CRFILE - OBSERVEDFILE
	ID_ADOMAIN - OBSERVEDDOMAIN
	NUM__40 - NUM
	NUM__48 - NUM
	NUM__62 - NUM
	NUM__55 - NUM
	NUM__26 - NUM
	NUM__54 - NUM
	NUM__41 - NUM
	NUM__47 - NUM
	NUM__61 - NUM
	NUM__12 - NUM
	NUM__13 - NUM
	NUM__20 - NUM
	NUM__27 - NUM
	NUM__34 - NUM
	NUM__19 - NUM
	NUM__33 - NUM
	ID_YHOST - OBSERVEDHOST
	ID_BTHOST - OBSERVEDHOST
	ID_BFHOST - OBSERVEDHOST
	ID_BMHOST - OBSERVEDHOST
	ID_CAHOST - OBSERVEDHOST
	ID_CHHOST - OBSERVEDHOST
	ID_RHOST - OBSERVEDHOST
	ID_KHOST - OBSERVEDHOST
	ID_CORAT - OBSERVEDRAT
	ID_DVRAT - OBSERVEDRAT
	ID_DSRAT - OBSERVEDRAT
	ID_DPRAT - OBSERVEDRAT
	ID_DQRAT - OBSERVEDRAT
	ID_DRRAT - OBSERVEDRAT
	ID_DURAT - OBSERVEDRAT
	ID_DORAT - OBSERVEDRAT
	ID_DTRAT - OBSERVEDRAT
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	STR__CN - STRING
	STR__JAMES - STRING
	STR__F - STRING
	STR__BS - STRING
	STR__ALPHA - STRING
	STR__BY - STRING
	STR__B - STRING
	STR__BL - STRING
	STR__BQ - STRING
	STR__MARY - STRING
	STR__E - STRING
	STR__BD - STRING
	STR__W - STRING
	STR__BJ - STRING
	STR__BK - STRING
	STR__BX - STRING
	STR__CE - STRING
	STR__J - STRING
	STR__I - STRING
	STR__CM - STRING
	STR__CP - STRING
	STR__BR - STRING
	STR__CG - STRING
	STR__CF - STRING
	STR__X - STRING
	STR__BZ - STRING
	STR__O - STRING
	STR__CL - STRING
	STR__P - STRING
	STR__V - STRING
	STR__BC - STRING
	STR__Q - STRING
	STR__BE - STRING
	ID_CZSHARE - OBSERVEDSHARE
	ID_DBSHARE - OBSERVEDSHARE
	ID_DFSHARE - OBSERVEDSHARE
	ID_DDSHARE - OBSERVEDSHARE
	ID_DASHARE - OBSERVEDSHARE
	ID_DESHARE - OBSERVEDSHARE
	ID_DCSHARE - OBSERVEDSHARE
	ID_CYSHARE - OBSERVEDSHARE
	ID_DLSCHTASK - OBSERVEDSCHTASK
	ID_DMSCHTASK - OBSERVEDSCHTASK
	ID_DISCHTASK - OBSERVEDSCHTASK
	ID_DHSCHTASK - OBSERVEDSCHTASK
	ID_DGSCHTASK - OBSERVEDSCHTASK
	ID_DNSCHTASK - OBSERVEDSCHTASK
	ID_DJSCHTASK - OBSERVEDSCHTASK
	ID_DKSCHTASK - OBSERVEDSCHTASK
)
(:init
	(KNOWS ID_BFHOST)
	(KNOWS ID_CORAT)
	(KNOWS_PROPERTY ID_BFHOST PFQDN)
	(KNOWS_PROPERTY ID_CORAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_CORAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BFHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BFHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BFHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BFHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_GDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_KHOST)
	(MEM_HOSTS ID_ADOMAIN ID_RHOST)
	(MEM_HOSTS ID_ADOMAIN ID_YHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_DC ID_BFHOST NO)
	(PROP_DC ID_BMHOST NO)
	(PROP_DC ID_BTHOST YES)
	(PROP_DC ID_CAHOST NO)
	(PROP_DC ID_CHHOST NO)
	(PROP_DC ID_KHOST YES)
	(PROP_DC ID_RHOST NO)
	(PROP_DC ID_YHOST YES)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BFHOST STR__BJ)
	(PROP_DNS_DOMAIN_NAME ID_BMHOST STR__BQ)
	(PROP_DNS_DOMAIN_NAME ID_BTHOST STR__BX)
	(PROP_DNS_DOMAIN_NAME ID_CAHOST STR__CE)
	(PROP_DNS_DOMAIN_NAME ID_CHHOST STR__CL)
	(PROP_DNS_DOMAIN_NAME ID_KHOST STR__O)
	(PROP_DNS_DOMAIN_NAME ID_RHOST STR__V)
	(PROP_DNS_DOMAIN_NAME ID_YHOST STR__BC)
	(PROP_DOMAIN ID_BFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_RHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_YHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_CORAT YES)
	(PROP_EXECUTABLE ID_CORAT STR__CP)
	(PROP_FQDN ID_BFHOST STR__BK)
	(PROP_FQDN ID_BMHOST STR__BR)
	(PROP_FQDN ID_BTHOST STR__BY)
	(PROP_FQDN ID_CAHOST STR__CF)
	(PROP_FQDN ID_CHHOST STR__CM)
	(PROP_FQDN ID_KHOST STR__P)
	(PROP_FQDN ID_RHOST STR__W)
	(PROP_FQDN ID_YHOST STR__BD)
	(PROP_HOST ID_BGTIMEDELTA ID_BFHOST)
	(PROP_HOST ID_BNTIMEDELTA ID_BMHOST)
	(PROP_HOST ID_BUTIMEDELTA ID_BTHOST)
	(PROP_HOST ID_CBTIMEDELTA ID_CAHOST)
	(PROP_HOST ID_CITIMEDELTA ID_CHHOST)
	(PROP_HOST ID_CORAT ID_BFHOST)
	(PROP_HOST ID_LTIMEDELTA ID_KHOST)
	(PROP_HOST ID_STIMEDELTA ID_RHOST)
	(PROP_HOST ID_ZTIMEDELTA ID_YHOST)
	(PROP_HOSTNAME ID_BFHOST STR__BL)
	(PROP_HOSTNAME ID_BMHOST STR__BS)
	(PROP_HOSTNAME ID_BTHOST STR__BZ)
	(PROP_HOSTNAME ID_CAHOST STR__CG)
	(PROP_HOSTNAME ID_CHHOST STR__CN)
	(PROP_HOSTNAME ID_KHOST STR__Q)
	(PROP_HOSTNAME ID_RHOST STR__X)
	(PROP_HOSTNAME ID_YHOST STR__BE)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BGTIMEDELTA NUM__33)
	(PROP_MICROSECONDS ID_BNTIMEDELTA NUM__40)
	(PROP_MICROSECONDS ID_BUTIMEDELTA NUM__47)
	(PROP_MICROSECONDS ID_CBTIMEDELTA NUM__54)
	(PROP_MICROSECONDS ID_CITIMEDELTA NUM__61)
	(PROP_MICROSECONDS ID_LTIMEDELTA NUM__12)
	(PROP_MICROSECONDS ID_STIMEDELTA NUM__19)
	(PROP_MICROSECONDS ID_ZTIMEDELTA NUM__26)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_SECONDS ID_BGTIMEDELTA NUM__34)
	(PROP_SECONDS ID_BNTIMEDELTA NUM__41)
	(PROP_SECONDS ID_BUTIMEDELTA NUM__48)
	(PROP_SECONDS ID_CBTIMEDELTA NUM__55)
	(PROP_SECONDS ID_CITIMEDELTA NUM__62)
	(PROP_SECONDS ID_LTIMEDELTA NUM__13)
	(PROP_SECONDS ID_STIMEDELTA NUM__20)
	(PROP_SECONDS ID_ZTIMEDELTA NUM__27)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_TIMEDELTA ID_BFHOST ID_BGTIMEDELTA)
	(PROP_TIMEDELTA ID_BMHOST ID_BNTIMEDELTA)
	(PROP_TIMEDELTA ID_BTHOST ID_BUTIMEDELTA)
	(PROP_TIMEDELTA ID_CAHOST ID_CBTIMEDELTA)
	(PROP_TIMEDELTA ID_CHHOST ID_CITIMEDELTA)
	(PROP_TIMEDELTA ID_KHOST ID_LTIMEDELTA)
	(PROP_TIMEDELTA ID_RHOST ID_STIMEDELTA)
	(PROP_TIMEDELTA ID_YHOST ID_ZTIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_DURAT ID_RHOST)(PROP_HOST ID_DRRAT ID_CHHOST)(PROP_HOST ID_DQRAT ID_CAHOST)(PROP_HOST ID_DPRAT ID_BMHOST)(PROP_HOST ID_DSRAT ID_BTHOST)(PROP_HOST ID_DVRAT ID_YHOST)(PROP_HOST ID_DTRAT ID_KHOST)))
(:metric minimize (total-cost))

)
