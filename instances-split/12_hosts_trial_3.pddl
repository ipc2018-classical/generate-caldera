(define (problem p12_hosts_trial_3) (:domain CALDERA)
(:objects
	ID_EAFILE - OBSERVEDFILE
	ID_EEFILE - OBSERVEDFILE
	ID_EGFILE - OBSERVEDFILE
	ID_EDFILE - OBSERVEDFILE
	ID_EJFILE - OBSERVEDFILE
	ID_EIFILE - OBSERVEDFILE
	ID_ELFILE - OBSERVEDFILE
	ID_EKFILE - OBSERVEDFILE
	ID_EFFILE - OBSERVEDFILE
	ID_ECFILE - OBSERVEDFILE
	ID_EHFILE - OBSERVEDFILE
	ID_EBFILE - OBSERVEDFILE
	ID_FISCHTASK - OBSERVEDSCHTASK
	ID_FBSCHTASK - OBSERVEDSCHTASK
	ID_FDSCHTASK - OBSERVEDSCHTASK
	ID_EYSCHTASK - OBSERVEDSCHTASK
	ID_FESCHTASK - OBSERVEDSCHTASK
	ID_FASCHTASK - OBSERVEDSCHTASK
	ID_FHSCHTASK - OBSERVEDSCHTASK
	ID_FCSCHTASK - OBSERVEDSCHTASK
	ID_FFSCHTASK - OBSERVEDSCHTASK
	ID_EZSCHTASK - OBSERVEDSCHTASK
	ID_FJSCHTASK - OBSERVEDSCHTASK
	ID_FGSCHTASK - OBSERVEDSCHTASK
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CBHOST - OBSERVEDHOST
	ID_DDHOST - OBSERVEDHOST
	ID_BNHOST - OBSERVEDHOST
	ID_CPHOST - OBSERVEDHOST
	ID_SHOST - OBSERVEDHOST
	ID_BGHOST - OBSERVEDHOST
	ID_CWHOST - OBSERVEDHOST
	ID_BUHOST - OBSERVEDHOST
	ID_ZHOST - OBSERVEDHOST
	ID_DKHOST - OBSERVEDHOST
	ID_DRHOST - OBSERVEDHOST
	ID_CIHOST - OBSERVEDHOST
	ID_ADOMAIN - OBSERVEDDOMAIN
	STR__CV - STRING
	STR__J - STRING
	STR__DC - STRING
	STR__BL - STRING
	STR__BM - STRING
	STR__PATRICIA - STRING
	STR__CG - STRING
	STR__Y - STRING
	STR__CN - STRING
	STR__DO - STRING
	STR__Q - STRING
	STR__CM - STRING
	STR__R - STRING
	STR__DH - STRING
	STR__X - STRING
	STR__DJ - STRING
	STR__DX - STRING
	STR__JAMES - STRING
	STR__ALPHA - STRING
	STR__MARY - STRING
	STR__BK - STRING
	STR__DZ - STRING
	STR__DV - STRING
	STR__BF - STRING
	STR__DI - STRING
	STR__DW - STRING
	STR__CU - STRING
	STR__F - STRING
	STR__CA - STRING
	STR__DP - STRING
	STR__BE - STRING
	STR__E - STRING
	STR__BY - STRING
	STR__CO - STRING
	STR__BT - STRING
	STR__CT - STRING
	STR__BR - STRING
	STR__B - STRING
	STR__BD - STRING
	STR__DQ - STRING
	STR__CF - STRING
	STR__M - STRING
	STR__DA - STRING
	STR__W - STRING
	STR__CH - STRING
	STR__BS - STRING
	STR__BZ - STRING
	STR__JOHN - STRING
	STR__DB - STRING
	STR__I - STRING
	STR__N - STRING
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EORAT - OBSERVEDRAT
	ID_EPRAT - OBSERVEDRAT
	ID_ENRAT - OBSERVEDRAT
	ID_EWRAT - OBSERVEDRAT
	ID_EURAT - OBSERVEDRAT
	ID_EVRAT - OBSERVEDRAT
	ID_EQRAT - OBSERVEDRAT
	ID_EXRAT - OBSERVEDRAT
	ID_DYRAT - OBSERVEDRAT
	ID_EMRAT - OBSERVEDRAT
	ID_ESRAT - OBSERVEDRAT
	ID_ERRAT - OBSERVEDRAT
	ID_ETRAT - OBSERVEDRAT
	ID_CJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DETIMEDELTA - OBSERVEDTIMEDELTA
	ID_TTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BOTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BATIMEDELTA - OBSERVEDTIMEDELTA
	ID_BVTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CCTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BHTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DLTIMEDELTA - OBSERVEDTIMEDELTA
	NUM__76 - NUM
	NUM__90 - NUM
	NUM__97 - NUM
	NUM__56 - NUM
	NUM__41 - NUM
	NUM__70 - NUM
	NUM__21 - NUM
	NUM__91 - NUM
	NUM__69 - NUM
	NUM__27 - NUM
	NUM__84 - NUM
	NUM__35 - NUM
	NUM__77 - NUM
	NUM__49 - NUM
	NUM__55 - NUM
	NUM__34 - NUM
	NUM__98 - NUM
	NUM__83 - NUM
	NUM__42 - NUM
	NUM__20 - NUM
	NUM__62 - NUM
	NUM__28 - NUM
	NUM__63 - NUM
	NUM__48 - NUM
	ID_FSSHARE - OBSERVEDSHARE
	ID_FMSHARE - OBSERVEDSHARE
	ID_FVSHARE - OBSERVEDSHARE
	ID_FKSHARE - OBSERVEDSHARE
	ID_FQSHARE - OBSERVEDSHARE
	ID_FTSHARE - OBSERVEDSHARE
	ID_FRSHARE - OBSERVEDSHARE
	ID_FUSHARE - OBSERVEDSHARE
	ID_FPSHARE - OBSERVEDSHARE
	ID_FLSHARE - OBSERVEDSHARE
	ID_FOSHARE - OBSERVEDSHARE
	ID_FNSHARE - OBSERVEDSHARE
)
(:init
	(KNOWS ID_CBHOST)
	(KNOWS ID_DYRAT)
	(KNOWS_PROPERTY ID_CBHOST PFQDN)
	(KNOWS_PROPERTY ID_DYRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_DYRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BGHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BGHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BNHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BNHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BUHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BUHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CBHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CBHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CIHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CIHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CPHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CPHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CWHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CWHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DDHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DDHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DKHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DKHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DRHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DRHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_SHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_SHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ZHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ZHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BGHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BGHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BNHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BNHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BUHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BUHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CBHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CBHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CIHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CIHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CPHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CPHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CWHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CWHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DDHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DDHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DKHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DKHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DRHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DRHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_SHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_SHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ZHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ZHOST ID_KDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BGHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BNHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BUHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CBHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_SHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ZHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_DC ID_BGHOST NO)
	(PROP_DC ID_BNHOST YES)
	(PROP_DC ID_BUHOST NO)
	(PROP_DC ID_CBHOST NO)
	(PROP_DC ID_CIHOST NO)
	(PROP_DC ID_CPHOST NO)
	(PROP_DC ID_CWHOST NO)
	(PROP_DC ID_DDHOST NO)
	(PROP_DC ID_DKHOST NO)
	(PROP_DC ID_DRHOST NO)
	(PROP_DC ID_SHOST NO)
	(PROP_DC ID_ZHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BGHOST STR__BL)
	(PROP_DNS_DOMAIN_NAME ID_BNHOST STR__BS)
	(PROP_DNS_DOMAIN_NAME ID_BUHOST STR__BZ)
	(PROP_DNS_DOMAIN_NAME ID_CBHOST STR__CG)
	(PROP_DNS_DOMAIN_NAME ID_CIHOST STR__CN)
	(PROP_DNS_DOMAIN_NAME ID_CPHOST STR__CU)
	(PROP_DNS_DOMAIN_NAME ID_CWHOST STR__DB)
	(PROP_DNS_DOMAIN_NAME ID_DDHOST STR__DI)
	(PROP_DNS_DOMAIN_NAME ID_DKHOST STR__DP)
	(PROP_DNS_DOMAIN_NAME ID_DRHOST STR__DW)
	(PROP_DNS_DOMAIN_NAME ID_SHOST STR__X)
	(PROP_DNS_DOMAIN_NAME ID_ZHOST STR__BE)
	(PROP_DOMAIN ID_BGHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BNHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BUHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ZHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_DYRAT YES)
	(PROP_EXECUTABLE ID_DYRAT STR__DZ)
	(PROP_FQDN ID_BGHOST STR__BK)
	(PROP_FQDN ID_BNHOST STR__BR)
	(PROP_FQDN ID_BUHOST STR__BY)
	(PROP_FQDN ID_CBHOST STR__CF)
	(PROP_FQDN ID_CIHOST STR__CM)
	(PROP_FQDN ID_CPHOST STR__CT)
	(PROP_FQDN ID_CWHOST STR__DA)
	(PROP_FQDN ID_DDHOST STR__DH)
	(PROP_FQDN ID_DKHOST STR__DO)
	(PROP_FQDN ID_DRHOST STR__DV)
	(PROP_FQDN ID_SHOST STR__W)
	(PROP_FQDN ID_ZHOST STR__BD)
	(PROP_HOST ID_BATIMEDELTA ID_ZHOST)
	(PROP_HOST ID_BHTIMEDELTA ID_BGHOST)
	(PROP_HOST ID_BOTIMEDELTA ID_BNHOST)
	(PROP_HOST ID_BVTIMEDELTA ID_BUHOST)
	(PROP_HOST ID_CCTIMEDELTA ID_CBHOST)
	(PROP_HOST ID_CJTIMEDELTA ID_CIHOST)
	(PROP_HOST ID_CQTIMEDELTA ID_CPHOST)
	(PROP_HOST ID_CXTIMEDELTA ID_CWHOST)
	(PROP_HOST ID_DETIMEDELTA ID_DDHOST)
	(PROP_HOST ID_DLTIMEDELTA ID_DKHOST)
	(PROP_HOST ID_DSTIMEDELTA ID_DRHOST)
	(PROP_HOST ID_DYRAT ID_CBHOST)
	(PROP_HOST ID_TTIMEDELTA ID_SHOST)
	(PROP_HOSTNAME ID_BGHOST STR__BM)
	(PROP_HOSTNAME ID_BNHOST STR__BT)
	(PROP_HOSTNAME ID_BUHOST STR__CA)
	(PROP_HOSTNAME ID_CBHOST STR__CH)
	(PROP_HOSTNAME ID_CIHOST STR__CO)
	(PROP_HOSTNAME ID_CPHOST STR__CV)
	(PROP_HOSTNAME ID_CWHOST STR__DC)
	(PROP_HOSTNAME ID_DDHOST STR__DJ)
	(PROP_HOSTNAME ID_DKHOST STR__DQ)
	(PROP_HOSTNAME ID_DRHOST STR__DX)
	(PROP_HOSTNAME ID_SHOST STR__Y)
	(PROP_HOSTNAME ID_ZHOST STR__BF)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_MICROSECONDS ID_BATIMEDELTA NUM__28)
	(PROP_MICROSECONDS ID_BHTIMEDELTA NUM__35)
	(PROP_MICROSECONDS ID_BOTIMEDELTA NUM__42)
	(PROP_MICROSECONDS ID_BVTIMEDELTA NUM__49)
	(PROP_MICROSECONDS ID_CCTIMEDELTA NUM__56)
	(PROP_MICROSECONDS ID_CJTIMEDELTA NUM__63)
	(PROP_MICROSECONDS ID_CQTIMEDELTA NUM__70)
	(PROP_MICROSECONDS ID_CXTIMEDELTA NUM__77)
	(PROP_MICROSECONDS ID_DETIMEDELTA NUM__84)
	(PROP_MICROSECONDS ID_DLTIMEDELTA NUM__91)
	(PROP_MICROSECONDS ID_DSTIMEDELTA NUM__98)
	(PROP_MICROSECONDS ID_TTIMEDELTA NUM__21)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_SECONDS ID_BATIMEDELTA NUM__27)
	(PROP_SECONDS ID_BHTIMEDELTA NUM__34)
	(PROP_SECONDS ID_BOTIMEDELTA NUM__41)
	(PROP_SECONDS ID_BVTIMEDELTA NUM__48)
	(PROP_SECONDS ID_CCTIMEDELTA NUM__55)
	(PROP_SECONDS ID_CJTIMEDELTA NUM__62)
	(PROP_SECONDS ID_CQTIMEDELTA NUM__69)
	(PROP_SECONDS ID_CXTIMEDELTA NUM__76)
	(PROP_SECONDS ID_DETIMEDELTA NUM__83)
	(PROP_SECONDS ID_DLTIMEDELTA NUM__90)
	(PROP_SECONDS ID_DSTIMEDELTA NUM__97)
	(PROP_SECONDS ID_TTIMEDELTA NUM__20)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_TIMEDELTA ID_BGHOST ID_BHTIMEDELTA)
	(PROP_TIMEDELTA ID_BNHOST ID_BOTIMEDELTA)
	(PROP_TIMEDELTA ID_BUHOST ID_BVTIMEDELTA)
	(PROP_TIMEDELTA ID_CBHOST ID_CCTIMEDELTA)
	(PROP_TIMEDELTA ID_CIHOST ID_CJTIMEDELTA)
	(PROP_TIMEDELTA ID_CPHOST ID_CQTIMEDELTA)
	(PROP_TIMEDELTA ID_CWHOST ID_CXTIMEDELTA)
	(PROP_TIMEDELTA ID_DDHOST ID_DETIMEDELTA)
	(PROP_TIMEDELTA ID_DKHOST ID_DLTIMEDELTA)
	(PROP_TIMEDELTA ID_DRHOST ID_DSTIMEDELTA)
	(PROP_TIMEDELTA ID_SHOST ID_TTIMEDELTA)
	(PROP_TIMEDELTA ID_ZHOST ID_BATIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_ESRAT ID_DRHOST)(PROP_HOST ID_EMRAT ID_DKHOST)(PROP_HOST ID_EXRAT ID_ZHOST)(PROP_HOST ID_EQRAT ID_BUHOST)(PROP_HOST ID_EVRAT ID_CWHOST)(PROP_HOST ID_EURAT ID_BGHOST)(PROP_HOST ID_EWRAT ID_SHOST)(PROP_HOST ID_ENRAT ID_CPHOST)(PROP_HOST ID_EPRAT ID_BNHOST)(PROP_HOST ID_EORAT ID_DDHOST)(PROP_HOST ID_ETRAT ID_CIHOST)))
(:metric minimize (total-cost))

)
