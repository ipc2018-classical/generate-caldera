(define (problem p12_hosts_trial_2) (:domain CALDERA)
(:objects
	ID_EOSCHTASK - OBSERVEDSCHTASK
	ID_ELSCHTASK - OBSERVEDSCHTASK
	ID_EJSCHTASK - OBSERVEDSCHTASK
	ID_EESCHTASK - OBSERVEDSCHTASK
	ID_EGSCHTASK - OBSERVEDSCHTASK
	ID_EPSCHTASK - OBSERVEDSCHTASK
	ID_EHSCHTASK - OBSERVEDSCHTASK
	ID_ENSCHTASK - OBSERVEDSCHTASK
	ID_EFSCHTASK - OBSERVEDSCHTASK
	ID_EMSCHTASK - OBSERVEDSCHTASK
	ID_EISCHTASK - OBSERVEDSCHTASK
	ID_EKSCHTASK - OBSERVEDSCHTASK
	ID_FNSHARE - OBSERVEDSHARE
	ID_FJSHARE - OBSERVEDSHARE
	ID_FDSHARE - OBSERVEDSHARE
	ID_FMSHARE - OBSERVEDSHARE
	ID_FESHARE - OBSERVEDSHARE
	ID_FKSHARE - OBSERVEDSHARE
	ID_FISHARE - OBSERVEDSHARE
	ID_FFSHARE - OBSERVEDSHARE
	ID_FHSHARE - OBSERVEDSHARE
	ID_FLSHARE - OBSERVEDSHARE
	ID_FGSHARE - OBSERVEDSHARE
	ID_FCSHARE - OBSERVEDSHARE
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	STR__F - STRING
	STR__CU - STRING
	STR__BK - STRING
	STR__CE - STRING
	STR__DO - STRING
	STR__BC - STRING
	STR__I - STRING
	STR__CN - STRING
	STR__CG - STRING
	STR__BL - STRING
	STR__CL - STRING
	STR__CF - STRING
	STR__DH - STRING
	STR__J - STRING
	STR__DP - STRING
	STR__JAMES - STRING
	STR__BY - STRING
	STR__E - STRING
	STR__CM - STRING
	STR__BE - STRING
	STR__DA - STRING
	STR__CS - STRING
	STR__BD - STRING
	STR__ALPHA - STRING
	STR__MARY - STRING
	STR__Q - STRING
	STR__DR - STRING
	STR__DN - STRING
	STR__BQ - STRING
	STR__BR - STRING
	STR__P - STRING
	STR__DG - STRING
	STR__CT - STRING
	STR__CZ - STRING
	STR__O - STRING
	STR__W - STRING
	STR__X - STRING
	STR__DI - STRING
	STR__BS - STRING
	STR__DB - STRING
	STR__BJ - STRING
	STR__B - STRING
	STR__V - STRING
	STR__BX - STRING
	STR__BZ - STRING
	ID_ECFILE - OBSERVEDFILE
	ID_DXFILE - OBSERVEDFILE
	ID_DVFILE - OBSERVEDFILE
	ID_EBFILE - OBSERVEDFILE
	ID_DUFILE - OBSERVEDFILE
	ID_DZFILE - OBSERVEDFILE
	ID_DSFILE - OBSERVEDFILE
	ID_DTFILE - OBSERVEDFILE
	ID_EDFILE - OBSERVEDFILE
	ID_DYFILE - OBSERVEDFILE
	ID_EAFILE - OBSERVEDFILE
	ID_DWFILE - OBSERVEDFILE
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CITIMEDELTA - OBSERVEDTIMEDELTA
	ID_BNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_LTIMEDELTA - OBSERVEDTIMEDELTA
	ID_STIMEDELTA - OBSERVEDTIMEDELTA
	ID_DKTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ERRAT - OBSERVEDRAT
	ID_ETRAT - OBSERVEDRAT
	ID_ESRAT - OBSERVEDRAT
	ID_EVRAT - OBSERVEDRAT
	ID_DQRAT - OBSERVEDRAT
	ID_EZRAT - OBSERVEDRAT
	ID_FARAT - OBSERVEDRAT
	ID_EXRAT - OBSERVEDRAT
	ID_EYRAT - OBSERVEDRAT
	ID_FBRAT - OBSERVEDRAT
	ID_EWRAT - OBSERVEDRAT
	ID_EURAT - OBSERVEDRAT
	ID_EQRAT - OBSERVEDRAT
	ID_CVHOST - OBSERVEDHOST
	ID_KHOST - OBSERVEDHOST
	ID_DCHOST - OBSERVEDHOST
	ID_DJHOST - OBSERVEDHOST
	ID_YHOST - OBSERVEDHOST
	ID_CHHOST - OBSERVEDHOST
	ID_BFHOST - OBSERVEDHOST
	ID_CAHOST - OBSERVEDHOST
	ID_COHOST - OBSERVEDHOST
	ID_RHOST - OBSERVEDHOST
	ID_BTHOST - OBSERVEDHOST
	ID_BMHOST - OBSERVEDHOST
	NUM__40 - NUM
	NUM__48 - NUM
	NUM__69 - NUM
	NUM__41 - NUM
	NUM__20 - NUM
	NUM__33 - NUM
	NUM__62 - NUM
	NUM__26 - NUM
	NUM__54 - NUM
	NUM__76 - NUM
	NUM__75 - NUM
	NUM__89 - NUM
	NUM__13 - NUM
	NUM__27 - NUM
	NUM__68 - NUM
	NUM__61 - NUM
	NUM__34 - NUM
	NUM__12 - NUM
	NUM__19 - NUM
	NUM__83 - NUM
	NUM__90 - NUM
	NUM__47 - NUM
	NUM__55 - NUM
	NUM__82 - NUM
	ID_ADOMAIN - OBSERVEDDOMAIN
)
(:init
	(KNOWS ID_DQRAT)
	(KNOWS ID_RHOST)
	(KNOWS_PROPERTY ID_DQRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_DQRAT PHOST)
	(KNOWS_PROPERTY ID_RHOST PFQDN)
	(MEM_CACHED_DOMAIN_CREDS ID_BFHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BFHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BMHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BTHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CAHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CHHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_COHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CVHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DCHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DJHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_KHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_RHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_YHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BFHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BFHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BMHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BTHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CAHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CHHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_COHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CVHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DCHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DJHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_KHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_RHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_YHOST ID_GDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_COHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DCHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DJHOST)
	(MEM_HOSTS ID_ADOMAIN ID_KHOST)
	(MEM_HOSTS ID_ADOMAIN ID_RHOST)
	(MEM_HOSTS ID_ADOMAIN ID_YHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_DC ID_BFHOST NO)
	(PROP_DC ID_BMHOST NO)
	(PROP_DC ID_BTHOST NO)
	(PROP_DC ID_CAHOST NO)
	(PROP_DC ID_CHHOST NO)
	(PROP_DC ID_COHOST NO)
	(PROP_DC ID_CVHOST NO)
	(PROP_DC ID_DCHOST NO)
	(PROP_DC ID_DJHOST YES)
	(PROP_DC ID_KHOST YES)
	(PROP_DC ID_RHOST NO)
	(PROP_DC ID_YHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BFHOST STR__BL)
	(PROP_DNS_DOMAIN_NAME ID_BMHOST STR__BS)
	(PROP_DNS_DOMAIN_NAME ID_BTHOST STR__BZ)
	(PROP_DNS_DOMAIN_NAME ID_CAHOST STR__CG)
	(PROP_DNS_DOMAIN_NAME ID_CHHOST STR__CN)
	(PROP_DNS_DOMAIN_NAME ID_COHOST STR__CU)
	(PROP_DNS_DOMAIN_NAME ID_CVHOST STR__DB)
	(PROP_DNS_DOMAIN_NAME ID_DCHOST STR__DI)
	(PROP_DNS_DOMAIN_NAME ID_DJHOST STR__DP)
	(PROP_DNS_DOMAIN_NAME ID_KHOST STR__Q)
	(PROP_DNS_DOMAIN_NAME ID_RHOST STR__X)
	(PROP_DNS_DOMAIN_NAME ID_YHOST STR__BE)
	(PROP_DOMAIN ID_BFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_COHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DCHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DJHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_RHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_YHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_DQRAT YES)
	(PROP_EXECUTABLE ID_DQRAT STR__DR)
	(PROP_FQDN ID_BFHOST STR__BJ)
	(PROP_FQDN ID_BMHOST STR__BQ)
	(PROP_FQDN ID_BTHOST STR__BX)
	(PROP_FQDN ID_CAHOST STR__CE)
	(PROP_FQDN ID_CHHOST STR__CL)
	(PROP_FQDN ID_COHOST STR__CS)
	(PROP_FQDN ID_CVHOST STR__CZ)
	(PROP_FQDN ID_DCHOST STR__DG)
	(PROP_FQDN ID_DJHOST STR__DN)
	(PROP_FQDN ID_KHOST STR__O)
	(PROP_FQDN ID_RHOST STR__V)
	(PROP_FQDN ID_YHOST STR__BC)
	(PROP_HOST ID_BGTIMEDELTA ID_BFHOST)
	(PROP_HOST ID_BNTIMEDELTA ID_BMHOST)
	(PROP_HOST ID_BUTIMEDELTA ID_BTHOST)
	(PROP_HOST ID_CBTIMEDELTA ID_CAHOST)
	(PROP_HOST ID_CITIMEDELTA ID_CHHOST)
	(PROP_HOST ID_CPTIMEDELTA ID_COHOST)
	(PROP_HOST ID_CWTIMEDELTA ID_CVHOST)
	(PROP_HOST ID_DDTIMEDELTA ID_DCHOST)
	(PROP_HOST ID_DKTIMEDELTA ID_DJHOST)
	(PROP_HOST ID_DQRAT ID_RHOST)
	(PROP_HOST ID_LTIMEDELTA ID_KHOST)
	(PROP_HOST ID_STIMEDELTA ID_RHOST)
	(PROP_HOST ID_ZTIMEDELTA ID_YHOST)
	(PROP_HOSTNAME ID_BFHOST STR__BK)
	(PROP_HOSTNAME ID_BMHOST STR__BR)
	(PROP_HOSTNAME ID_BTHOST STR__BY)
	(PROP_HOSTNAME ID_CAHOST STR__CF)
	(PROP_HOSTNAME ID_CHHOST STR__CM)
	(PROP_HOSTNAME ID_COHOST STR__CT)
	(PROP_HOSTNAME ID_CVHOST STR__DA)
	(PROP_HOSTNAME ID_DCHOST STR__DH)
	(PROP_HOSTNAME ID_DJHOST STR__DO)
	(PROP_HOSTNAME ID_KHOST STR__P)
	(PROP_HOSTNAME ID_RHOST STR__W)
	(PROP_HOSTNAME ID_YHOST STR__BD)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BGTIMEDELTA NUM__33)
	(PROP_MICROSECONDS ID_BNTIMEDELTA NUM__40)
	(PROP_MICROSECONDS ID_BUTIMEDELTA NUM__47)
	(PROP_MICROSECONDS ID_CBTIMEDELTA NUM__54)
	(PROP_MICROSECONDS ID_CITIMEDELTA NUM__61)
	(PROP_MICROSECONDS ID_CPTIMEDELTA NUM__68)
	(PROP_MICROSECONDS ID_CWTIMEDELTA NUM__75)
	(PROP_MICROSECONDS ID_DDTIMEDELTA NUM__82)
	(PROP_MICROSECONDS ID_DKTIMEDELTA NUM__89)
	(PROP_MICROSECONDS ID_LTIMEDELTA NUM__12)
	(PROP_MICROSECONDS ID_STIMEDELTA NUM__19)
	(PROP_MICROSECONDS ID_ZTIMEDELTA NUM__26)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_SECONDS ID_BGTIMEDELTA NUM__34)
	(PROP_SECONDS ID_BNTIMEDELTA NUM__41)
	(PROP_SECONDS ID_BUTIMEDELTA NUM__48)
	(PROP_SECONDS ID_CBTIMEDELTA NUM__55)
	(PROP_SECONDS ID_CITIMEDELTA NUM__62)
	(PROP_SECONDS ID_CPTIMEDELTA NUM__69)
	(PROP_SECONDS ID_CWTIMEDELTA NUM__76)
	(PROP_SECONDS ID_DDTIMEDELTA NUM__83)
	(PROP_SECONDS ID_DKTIMEDELTA NUM__90)
	(PROP_SECONDS ID_LTIMEDELTA NUM__13)
	(PROP_SECONDS ID_STIMEDELTA NUM__20)
	(PROP_SECONDS ID_ZTIMEDELTA NUM__27)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_TIMEDELTA ID_BFHOST ID_BGTIMEDELTA)
	(PROP_TIMEDELTA ID_BMHOST ID_BNTIMEDELTA)
	(PROP_TIMEDELTA ID_BTHOST ID_BUTIMEDELTA)
	(PROP_TIMEDELTA ID_CAHOST ID_CBTIMEDELTA)
	(PROP_TIMEDELTA ID_CHHOST ID_CITIMEDELTA)
	(PROP_TIMEDELTA ID_COHOST ID_CPTIMEDELTA)
	(PROP_TIMEDELTA ID_CVHOST ID_CWTIMEDELTA)
	(PROP_TIMEDELTA ID_DCHOST ID_DDTIMEDELTA)
	(PROP_TIMEDELTA ID_DJHOST ID_DKTIMEDELTA)
	(PROP_TIMEDELTA ID_KHOST ID_LTIMEDELTA)
	(PROP_TIMEDELTA ID_RHOST ID_STIMEDELTA)
	(PROP_TIMEDELTA ID_YHOST ID_ZTIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_EWRAT ID_BTHOST)(PROP_HOST ID_FBRAT ID_COHOST)(PROP_HOST ID_EYRAT ID_CAHOST)(PROP_HOST ID_EXRAT ID_BFHOST)(PROP_HOST ID_FARAT ID_CHHOST)(PROP_HOST ID_EZRAT ID_YHOST)(PROP_HOST ID_EVRAT ID_DJHOST)(PROP_HOST ID_ESRAT ID_DCHOST)(PROP_HOST ID_ETRAT ID_KHOST)(PROP_HOST ID_ERRAT ID_CVHOST)(PROP_HOST ID_EQRAT ID_BMHOST)))
(:metric minimize (total-cost))

)
