(define (problem p5_hosts_trial_8) (:domain CALDERA)
(:objects
	ID_HARAT - OBSERVEDRAT
	ID_GZRAT - OBSERVEDRAT
	ID_GRRAT - OBSERVEDRAT
	ID_GYRAT - OBSERVEDRAT
	ID_HCRAT - OBSERVEDRAT
	ID_HBRAT - OBSERVEDRAT
	ID_ADOMAIN - OBSERVEDDOMAIN
	STR__CE - STRING
	STR__BS - STRING
	STR__MARY - STRING
	STR__CU - STRING
	STR__HELEN - STRING
	STR__GJ - STRING
	STR__GO - STRING
	STR__CY - STRING
	STR__THOMAS - STRING
	STR__M - STRING
	STR__B - STRING
	STR__BK - STRING
	STR__J - STRING
	STR__BX - STRING
	STR__KENNETH - STRING
	STR__V - STRING
	STR__CM - STRING
	STR__Z - STRING
	STR__JENNIFER - STRING
	STR__EF - STRING
	STR__FD - STRING
	STR__FG - STRING
	STR__BARBARA - STRING
	STR__CN - STRING
	STR__GA - STRING
	STR__CR - STRING
	STR__Y - STRING
	STR__DANIEL - STRING
	STR__GEORGE - STRING
	STR__FT - STRING
	STR__FN - STRING
	STR__ELIZABETH - STRING
	STR__GH - STRING
	STR__DP - STRING
	STR__JAMES - STRING
	STR__BETTY - STRING
	STR__CHRISTOPHER - STRING
	STR__NANCY - STRING
	STR__EQ - STRING
	STR__CZ - STRING
	STR__FO - STRING
	STR__JOHN - STRING
	STR__EB - STRING
	STR__BW - STRING
	STR__DX - STRING
	STR__EM - STRING
	STR__FC - STRING
	STR__BL - STRING
	STR__DONNA - STRING
	STR__Q - STRING
	STR__ROBERT - STRING
	STR__DH - STRING
	STR__SANDRA - STRING
	STR__EA - STRING
	STR__BO - STRING
	STR__EV - STRING
	STR__I - STRING
	STR__LISA - STRING
	STR__CI - STRING
	STR__BP - STRING
	STR__CJ - STRING
	STR__DT - STRING
	STR__ER - STRING
	STR__LINDA - STRING
	STR__EI - STRING
	STR__JOSEPH - STRING
	STR__CA - STRING
	STR__KAREN - STRING
	STR__ALPHA - STRING
	STR__FU - STRING
	STR__BG - STRING
	STR__BH - STRING
	STR__FH - STRING
	STR__GI - STRING
	STR__EN - STRING
	STR__CHARLES - STRING
	STR__DK - STRING
	STR__EU - STRING
	STR__DD - STRING
	STR__R - STRING
	STR__FM - STRING
	STR__EZ - STRING
	STR__DO - STRING
	STR__RICHARD - STRING
	STR__PAUL - STRING
	STR__EY - STRING
	STR__MARIA - STRING
	STR__CF - STRING
	STR__DAVID - STRING
	STR__WILLIAM - STRING
	STR__F - STRING
	STR__MARK - STRING
	STR__DS - STRING
	STR__DL - STRING
	STR__E - STRING
	STR__CV - STRING
	STR__GP - STRING
	STR__EJ - STRING
	STR__DC - STRING
	STR__MARGARET - STRING
	STR__BT - STRING
	STR__MICHAEL - STRING
	STR__FV - STRING
	STR__U - STRING
	STR__N - STRING
	STR__BC - STRING
	STR__DOROTHY - STRING
	STR__GC - STRING
	STR__GQ - STRING
	STR__CQ - STRING
	STR__DG - STRING
	STR__DONALD - STRING
	STR__SUSAN - STRING
	STR__DW - STRING
	STR__PATRICIA - STRING
	STR__GB - STRING
	STR__EE - STRING
	STR__GS - STRING
	STR__BD - STRING
	STR__CB - STRING
	ID_HGSCHTASK - OBSERVEDSCHTASK
	ID_HFSCHTASK - OBSERVEDSCHTASK
	ID_HHSCHTASK - OBSERVEDSCHTASK
	ID_HDSCHTASK - OBSERVEDSCHTASK
	ID_HESCHTASK - OBSERVEDSCHTASK
	ID_FIHOST - OBSERVEDHOST
	ID_GDHOST - OBSERVEDHOST
	ID_GKHOST - OBSERVEDHOST
	ID_FWHOST - OBSERVEDHOST
	ID_FPHOST - OBSERVEDHOST
	ID_HKSHARE - OBSERVEDSHARE
	ID_HJSHARE - OBSERVEDSHARE
	ID_HLSHARE - OBSERVEDSHARE
	ID_HISHARE - OBSERVEDSHARE
	ID_HMSHARE - OBSERVEDSHARE
	ID_GXFILE - OBSERVEDFILE
	ID_GWFILE - OBSERVEDFILE
	ID_GUFILE - OBSERVEDFILE
	ID_GTFILE - OBSERVEDFILE
	ID_GVFILE - OBSERVEDFILE
	ID_FXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GETIMEDELTA - OBSERVEDTIMEDELTA
	ID_GLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FJTIMEDELTA - OBSERVEDTIMEDELTA
	NUM__155 - NUM
	NUM__161 - NUM
	NUM__147 - NUM
	NUM__148 - NUM
	NUM__169 - NUM
	NUM__154 - NUM
	NUM__141 - NUM
	NUM__140 - NUM
	NUM__168 - NUM
	NUM__162 - NUM
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_EGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ECDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FADOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_FEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ESDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_EXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ETDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_FFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ELDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_FBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
)
(:init
	(KNOWS ID_FIHOST)
	(KNOWS ID_GRRAT)
	(KNOWS_PROPERTY ID_FIHOST PFQDN)
	(KNOWS_PROPERTY ID_GRRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_GRRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_BZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_DZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FIHOST ID_ETDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_FBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FPHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_FBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FWHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_EPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GDHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_ELDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GKHOST ID_FFDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FIHOST ID_EWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_DYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FPHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_CKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_ECDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_EGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FWHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_EKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GDHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_DMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_FEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GKHOST ID_KDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_FIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GKHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_DUDOMAINUSER ID_DVDOMAINCREDENTIAL)
	(PROP_CRED ID_DYDOMAINUSER ID_DZDOMAINCREDENTIAL)
	(PROP_CRED ID_ECDOMAINUSER ID_EDDOMAINCREDENTIAL)
	(PROP_CRED ID_EGDOMAINUSER ID_EHDOMAINCREDENTIAL)
	(PROP_CRED ID_EKDOMAINUSER ID_ELDOMAINCREDENTIAL)
	(PROP_CRED ID_EODOMAINUSER ID_EPDOMAINCREDENTIAL)
	(PROP_CRED ID_ESDOMAINUSER ID_ETDOMAINCREDENTIAL)
	(PROP_CRED ID_EWDOMAINUSER ID_EXDOMAINCREDENTIAL)
	(PROP_CRED ID_FADOMAINUSER ID_FBDOMAINCREDENTIAL)
	(PROP_CRED ID_FEDOMAINUSER ID_FFDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_FIHOST NO)
	(PROP_DC ID_FPHOST NO)
	(PROP_DC ID_FWHOST NO)
	(PROP_DC ID_GDHOST NO)
	(PROP_DC ID_GKHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_FIHOST STR__FN)
	(PROP_DNS_DOMAIN_NAME ID_FPHOST STR__FU)
	(PROP_DNS_DOMAIN_NAME ID_FWHOST STR__GB)
	(PROP_DNS_DOMAIN_NAME ID_GDHOST STR__GI)
	(PROP_DNS_DOMAIN_NAME ID_GKHOST STR__GP)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ECDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ELDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ESDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ETDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_FBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_FFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_FIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_GRRAT YES)
	(PROP_EXECUTABLE ID_GRRAT STR__GS)
	(PROP_FQDN ID_FIHOST STR__FO)
	(PROP_FQDN ID_FPHOST STR__FV)
	(PROP_FQDN ID_FWHOST STR__GC)
	(PROP_FQDN ID_GDHOST STR__GJ)
	(PROP_FQDN ID_GKHOST STR__GQ)
	(PROP_HOST ID_FJTIMEDELTA ID_FIHOST)
	(PROP_HOST ID_FQTIMEDELTA ID_FPHOST)
	(PROP_HOST ID_FXTIMEDELTA ID_FWHOST)
	(PROP_HOST ID_GETIMEDELTA ID_GDHOST)
	(PROP_HOST ID_GLTIMEDELTA ID_GKHOST)
	(PROP_HOST ID_GRRAT ID_FIHOST)
	(PROP_HOSTNAME ID_FIHOST STR__FM)
	(PROP_HOSTNAME ID_FPHOST STR__FT)
	(PROP_HOSTNAME ID_FWHOST STR__GA)
	(PROP_HOSTNAME ID_GDHOST STR__GH)
	(PROP_HOSTNAME ID_GKHOST STR__GO)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_DUDOMAINUSER NO)
	(PROP_IS_GROUP ID_DYDOMAINUSER NO)
	(PROP_IS_GROUP ID_ECDOMAINUSER NO)
	(PROP_IS_GROUP ID_EGDOMAINUSER NO)
	(PROP_IS_GROUP ID_EKDOMAINUSER NO)
	(PROP_IS_GROUP ID_EODOMAINUSER NO)
	(PROP_IS_GROUP ID_ESDOMAINUSER NO)
	(PROP_IS_GROUP ID_EWDOMAINUSER NO)
	(PROP_IS_GROUP ID_FADOMAINUSER NO)
	(PROP_IS_GROUP ID_FEDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_FJTIMEDELTA NUM__140)
	(PROP_MICROSECONDS ID_FQTIMEDELTA NUM__147)
	(PROP_MICROSECONDS ID_FXTIMEDELTA NUM__154)
	(PROP_MICROSECONDS ID_GETIMEDELTA NUM__161)
	(PROP_MICROSECONDS ID_GLTIMEDELTA NUM__168)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_DVDOMAINCREDENTIAL STR__DW)
	(PROP_PASSWORD ID_DZDOMAINCREDENTIAL STR__EA)
	(PROP_PASSWORD ID_EDDOMAINCREDENTIAL STR__EE)
	(PROP_PASSWORD ID_EHDOMAINCREDENTIAL STR__EI)
	(PROP_PASSWORD ID_ELDOMAINCREDENTIAL STR__EM)
	(PROP_PASSWORD ID_EPDOMAINCREDENTIAL STR__EQ)
	(PROP_PASSWORD ID_ETDOMAINCREDENTIAL STR__EU)
	(PROP_PASSWORD ID_EXDOMAINCREDENTIAL STR__EY)
	(PROP_PASSWORD ID_FBDOMAINCREDENTIAL STR__FC)
	(PROP_PASSWORD ID_FFDOMAINCREDENTIAL STR__FG)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_FJTIMEDELTA NUM__141)
	(PROP_SECONDS ID_FQTIMEDELTA NUM__148)
	(PROP_SECONDS ID_FXTIMEDELTA NUM__155)
	(PROP_SECONDS ID_GETIMEDELTA NUM__162)
	(PROP_SECONDS ID_GLTIMEDELTA NUM__169)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_DUDOMAINUSER STR__DX)
	(PROP_SID ID_DYDOMAINUSER STR__EB)
	(PROP_SID ID_ECDOMAINUSER STR__EF)
	(PROP_SID ID_EGDOMAINUSER STR__EJ)
	(PROP_SID ID_EKDOMAINUSER STR__EN)
	(PROP_SID ID_EODOMAINUSER STR__ER)
	(PROP_SID ID_ESDOMAINUSER STR__EV)
	(PROP_SID ID_EWDOMAINUSER STR__EZ)
	(PROP_SID ID_FADOMAINUSER STR__FD)
	(PROP_SID ID_FEDOMAINUSER STR__FH)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_FIHOST ID_FJTIMEDELTA)
	(PROP_TIMEDELTA ID_FPHOST ID_FQTIMEDELTA)
	(PROP_TIMEDELTA ID_FWHOST ID_FXTIMEDELTA)
	(PROP_TIMEDELTA ID_GDHOST ID_GETIMEDELTA)
	(PROP_TIMEDELTA ID_GKHOST ID_GLTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_DVDOMAINCREDENTIAL ID_DUDOMAINUSER)
	(PROP_USER ID_DZDOMAINCREDENTIAL ID_DYDOMAINUSER)
	(PROP_USER ID_EDDOMAINCREDENTIAL ID_ECDOMAINUSER)
	(PROP_USER ID_EHDOMAINCREDENTIAL ID_EGDOMAINUSER)
	(PROP_USER ID_ELDOMAINCREDENTIAL ID_EKDOMAINUSER)
	(PROP_USER ID_EPDOMAINCREDENTIAL ID_EODOMAINUSER)
	(PROP_USER ID_ETDOMAINCREDENTIAL ID_ESDOMAINUSER)
	(PROP_USER ID_EXDOMAINCREDENTIAL ID_EWDOMAINUSER)
	(PROP_USER ID_FBDOMAINCREDENTIAL ID_FADOMAINUSER)
	(PROP_USER ID_FFDOMAINCREDENTIAL ID_FEDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_DUDOMAINUSER STR__PAUL)
	(PROP_USERNAME ID_DYDOMAINUSER STR__KAREN)
	(PROP_USERNAME ID_ECDOMAINUSER STR__MARK)
	(PROP_USERNAME ID_EGDOMAINUSER STR__BETTY)
	(PROP_USERNAME ID_EKDOMAINUSER STR__DONALD)
	(PROP_USERNAME ID_EODOMAINUSER STR__HELEN)
	(PROP_USERNAME ID_ESDOMAINUSER STR__GEORGE)
	(PROP_USERNAME ID_EWDOMAINUSER STR__SANDRA)
	(PROP_USERNAME ID_FADOMAINUSER STR__KENNETH)
	(PROP_USERNAME ID_FEDOMAINUSER STR__DONNA)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_HCRAT ID_FPHOST)(PROP_HOST ID_GZRAT ID_GKHOST)(PROP_HOST ID_HARAT ID_GDHOST)(PROP_HOST ID_GYRAT ID_FWHOST)))
(:metric minimize (total-cost))

)
