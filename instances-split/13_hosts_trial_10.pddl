(define (problem p13_hosts_trial_10) (:domain CALDERA)
(:objects
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HGRAT - OBSERVEDRAT
	ID_HFRAT - OBSERVEDRAT
	ID_HBRAT - OBSERVEDRAT
	ID_HIRAT - OBSERVEDRAT
	ID_HJRAT - OBSERVEDRAT
	ID_GXRAT - OBSERVEDRAT
	ID_GZRAT - OBSERVEDRAT
	ID_GVRAT - OBSERVEDRAT
	ID_HERAT - OBSERVEDRAT
	ID_HCRAT - OBSERVEDRAT
	ID_HDRAT - OBSERVEDRAT
	ID_HHRAT - OBSERVEDRAT
	ID_HARAT - OBSERVEDRAT
	ID_GYRAT - OBSERVEDRAT
	STR__CQ - STRING
	STR__ALPHA - STRING
	STR__DU - STRING
	STR__CM - STRING
	STR__CI - STRING
	STR__U - STRING
	STR__DD - STRING
	STR__FK - STRING
	STR__DOROTHY - STRING
	STR__CY - STRING
	STR__Q - STRING
	STR__EJ - STRING
	STR__CA - STRING
	STR__BD - STRING
	STR__MARGARET - STRING
	STR__CHRISTOPHER - STRING
	STR__BK - STRING
	STR__DH - STRING
	STR__GG - STRING
	STR__EO - STRING
	STR__BO - STRING
	STR__E - STRING
	STR__MARIA - STRING
	STR__JOSEPH - STRING
	STR__GU - STRING
	STR__N - STRING
	STR__BW - STRING
	STR__BX - STRING
	STR__DT - STRING
	STR__DAVID - STRING
	STR__DC - STRING
	STR__THOMAS - STRING
	STR__EV - STRING
	STR__BARBARA - STRING
	STR__MICHAEL - STRING
	STR__FZ - STRING
	STR__GF - STRING
	STR__CF - STRING
	STR__CU - STRING
	STR__BH - STRING
	STR__CV - STRING
	STR__CN - STRING
	STR__FL - STRING
	STR__EX - STRING
	STR__EH - STRING
	STR__BL - STRING
	STR__GT - STRING
	STR__JAMES - STRING
	STR__Z - STRING
	STR__DM - STRING
	STR__FY - STRING
	STR__GL - STRING
	STR__EB - STRING
	STR__CB - STRING
	STR__FJ - STRING
	STR__CR - STRING
	STR__MARY - STRING
	STR__M - STRING
	STR__EW - STRING
	STR__GM - STRING
	STR__R - STRING
	STR__Y - STRING
	STR__BS - STRING
	STR__CZ - STRING
	STR__GE - STRING
	STR__FQ - STRING
	STR__SUSAN - STRING
	STR__EP - STRING
	STR__FC - STRING
	STR__DO - STRING
	STR__FR - STRING
	STR__FX - STRING
	STR__DG - STRING
	STR__FS - STRING
	STR__EI - STRING
	STR__V - STRING
	STR__DV - STRING
	STR__PATRICIA - STRING
	STR__B - STRING
	STR__EQ - STRING
	STR__I - STRING
	STR__FD - STRING
	STR__CE - STRING
	STR__GS - STRING
	STR__EC - STRING
	STR__ROBERT - STRING
	STR__JOHN - STRING
	STR__BG - STRING
	STR__ELIZABETH - STRING
	STR__EA - STRING
	STR__DN - STRING
	STR__J - STRING
	STR__CJ - STRING
	STR__BC - STRING
	STR__LINDA - STRING
	STR__CHARLES - STRING
	STR__RICHARD - STRING
	STR__WILLIAM - STRING
	STR__JENNIFER - STRING
	STR__GW - STRING
	STR__F - STRING
	STR__BT - STRING
	STR__FE - STRING
	STR__GN - STRING
	STR__BP - STRING
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_IMSHARE - OBSERVEDSHARE
	ID_IPSHARE - OBSERVEDSHARE
	ID_INSHARE - OBSERVEDSHARE
	ID_ISSHARE - OBSERVEDSHARE
	ID_IRSHARE - OBSERVEDSHARE
	ID_ITSHARE - OBSERVEDSHARE
	ID_IWSHARE - OBSERVEDSHARE
	ID_IKSHARE - OBSERVEDSHARE
	ID_IUSHARE - OBSERVEDSHARE
	ID_IVSHARE - OBSERVEDSHARE
	ID_IOSHARE - OBSERVEDSHARE
	ID_ILSHARE - OBSERVEDSHARE
	ID_IQSHARE - OBSERVEDSHARE
	NUM__131 - NUM
	NUM__96 - NUM
	NUM__103 - NUM
	NUM__95 - NUM
	NUM__145 - NUM
	NUM__102 - NUM
	NUM__116 - NUM
	NUM__138 - NUM
	NUM__166 - NUM
	NUM__152 - NUM
	NUM__158 - NUM
	NUM__144 - NUM
	NUM__110 - NUM
	NUM__109 - NUM
	NUM__172 - NUM
	NUM__151 - NUM
	NUM__123 - NUM
	NUM__137 - NUM
	NUM__124 - NUM
	NUM__165 - NUM
	NUM__130 - NUM
	NUM__159 - NUM
	NUM__173 - NUM
	NUM__89 - NUM
	NUM__117 - NUM
	NUM__88 - NUM
	ID_GAHOST - OBSERVEDHOST
	ID_DPHOST - OBSERVEDHOST
	ID_DIHOST - OBSERVEDHOST
	ID_DWHOST - OBSERVEDHOST
	ID_EYHOST - OBSERVEDHOST
	ID_GOHOST - OBSERVEDHOST
	ID_EDHOST - OBSERVEDHOST
	ID_FFHOST - OBSERVEDHOST
	ID_FMHOST - OBSERVEDHOST
	ID_FTHOST - OBSERVEDHOST
	ID_GHHOST - OBSERVEDHOST
	ID_EKHOST - OBSERVEDHOST
	ID_ERHOST - OBSERVEDHOST
	ID_FNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ESTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ELTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EETIMEDELTA - OBSERVEDTIMEDELTA
	ID_GITIMEDELTA - OBSERVEDTIMEDELTA
	ID_GPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ICFILE - OBSERVEDFILE
	ID_IFFILE - OBSERVEDFILE
	ID_IBFILE - OBSERVEDFILE
	ID_HYFILE - OBSERVEDFILE
	ID_HZFILE - OBSERVEDFILE
	ID_IIFILE - OBSERVEDFILE
	ID_IDFILE - OBSERVEDFILE
	ID_IGFILE - OBSERVEDFILE
	ID_IJFILE - OBSERVEDFILE
	ID_HXFILE - OBSERVEDFILE
	ID_IEFILE - OBSERVEDFILE
	ID_IHFILE - OBSERVEDFILE
	ID_IAFILE - OBSERVEDFILE
	ID_HPSCHTASK - OBSERVEDSCHTASK
	ID_HWSCHTASK - OBSERVEDSCHTASK
	ID_HKSCHTASK - OBSERVEDSCHTASK
	ID_HMSCHTASK - OBSERVEDSCHTASK
	ID_HSSCHTASK - OBSERVEDSCHTASK
	ID_HTSCHTASK - OBSERVEDSCHTASK
	ID_HQSCHTASK - OBSERVEDSCHTASK
	ID_HVSCHTASK - OBSERVEDSCHTASK
	ID_HNSCHTASK - OBSERVEDSCHTASK
	ID_HUSCHTASK - OBSERVEDSCHTASK
	ID_HRSCHTASK - OBSERVEDSCHTASK
	ID_HLSCHTASK - OBSERVEDSCHTASK
	ID_HOSCHTASK - OBSERVEDSCHTASK
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
)
(:init
	(KNOWS ID_FTHOST)
	(KNOWS ID_GVRAT)
	(KNOWS_PROPERTY ID_FTHOST PFQDN)
	(KNOWS_PROPERTY ID_GVRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_GVRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_DIHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DIHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DIHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DPHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DPHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DPHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DWHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DWHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DWHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EDHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EDHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EDHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EKHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_ERHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EYHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_CHDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FFHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FMHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FTHOST ID_CPDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GAHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_CXDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GOHOST ID_XDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_DIHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DIHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DIHOST ID_CCDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DIHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DPHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DPHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DPHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DPHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DWHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DWHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DWHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DWHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EDHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EDHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EDHOST ID_CODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EDHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EKHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_ERHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_CWDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EYHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_DEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FFHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FMHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_CKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_DADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FTHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_BYDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GAHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_BUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_CKDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHHOST ID_CSDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_CGDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GOHOST ID_WDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_DIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_ERHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GOHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_DIHOST NO)
	(PROP_DC ID_DPHOST NO)
	(PROP_DC ID_DWHOST NO)
	(PROP_DC ID_EDHOST NO)
	(PROP_DC ID_EKHOST NO)
	(PROP_DC ID_ERHOST NO)
	(PROP_DC ID_EYHOST NO)
	(PROP_DC ID_FFHOST NO)
	(PROP_DC ID_FMHOST NO)
	(PROP_DC ID_FTHOST YES)
	(PROP_DC ID_GAHOST YES)
	(PROP_DC ID_GHHOST NO)
	(PROP_DC ID_GOHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_DIHOST STR__DN)
	(PROP_DNS_DOMAIN_NAME ID_DPHOST STR__DU)
	(PROP_DNS_DOMAIN_NAME ID_DWHOST STR__EB)
	(PROP_DNS_DOMAIN_NAME ID_EDHOST STR__EI)
	(PROP_DNS_DOMAIN_NAME ID_EKHOST STR__EP)
	(PROP_DNS_DOMAIN_NAME ID_ERHOST STR__EW)
	(PROP_DNS_DOMAIN_NAME ID_EYHOST STR__FD)
	(PROP_DNS_DOMAIN_NAME ID_FFHOST STR__FK)
	(PROP_DNS_DOMAIN_NAME ID_FMHOST STR__FR)
	(PROP_DNS_DOMAIN_NAME ID_FTHOST STR__FY)
	(PROP_DNS_DOMAIN_NAME ID_GAHOST STR__GF)
	(PROP_DNS_DOMAIN_NAME ID_GHHOST STR__GM)
	(PROP_DNS_DOMAIN_NAME ID_GOHOST STR__GT)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_ERHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_GVRAT YES)
	(PROP_EXECUTABLE ID_GVRAT STR__GW)
	(PROP_FQDN ID_DIHOST STR__DO)
	(PROP_FQDN ID_DPHOST STR__DV)
	(PROP_FQDN ID_DWHOST STR__EC)
	(PROP_FQDN ID_EDHOST STR__EJ)
	(PROP_FQDN ID_EKHOST STR__EQ)
	(PROP_FQDN ID_ERHOST STR__EX)
	(PROP_FQDN ID_EYHOST STR__FE)
	(PROP_FQDN ID_FFHOST STR__FL)
	(PROP_FQDN ID_FMHOST STR__FS)
	(PROP_FQDN ID_FTHOST STR__FZ)
	(PROP_FQDN ID_GAHOST STR__GG)
	(PROP_FQDN ID_GHHOST STR__GN)
	(PROP_FQDN ID_GOHOST STR__GU)
	(PROP_HOST ID_DJTIMEDELTA ID_DIHOST)
	(PROP_HOST ID_DQTIMEDELTA ID_DPHOST)
	(PROP_HOST ID_DXTIMEDELTA ID_DWHOST)
	(PROP_HOST ID_EETIMEDELTA ID_EDHOST)
	(PROP_HOST ID_ELTIMEDELTA ID_EKHOST)
	(PROP_HOST ID_ESTIMEDELTA ID_ERHOST)
	(PROP_HOST ID_EZTIMEDELTA ID_EYHOST)
	(PROP_HOST ID_FGTIMEDELTA ID_FFHOST)
	(PROP_HOST ID_FNTIMEDELTA ID_FMHOST)
	(PROP_HOST ID_FUTIMEDELTA ID_FTHOST)
	(PROP_HOST ID_GBTIMEDELTA ID_GAHOST)
	(PROP_HOST ID_GITIMEDELTA ID_GHHOST)
	(PROP_HOST ID_GPTIMEDELTA ID_GOHOST)
	(PROP_HOST ID_GVRAT ID_FTHOST)
	(PROP_HOSTNAME ID_DIHOST STR__DM)
	(PROP_HOSTNAME ID_DPHOST STR__DT)
	(PROP_HOSTNAME ID_DWHOST STR__EA)
	(PROP_HOSTNAME ID_EDHOST STR__EH)
	(PROP_HOSTNAME ID_EKHOST STR__EO)
	(PROP_HOSTNAME ID_ERHOST STR__EV)
	(PROP_HOSTNAME ID_EYHOST STR__FC)
	(PROP_HOSTNAME ID_FFHOST STR__FJ)
	(PROP_HOSTNAME ID_FMHOST STR__FQ)
	(PROP_HOSTNAME ID_FTHOST STR__FX)
	(PROP_HOSTNAME ID_GAHOST STR__GE)
	(PROP_HOSTNAME ID_GHHOST STR__GL)
	(PROP_HOSTNAME ID_GOHOST STR__GS)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_DJTIMEDELTA NUM__89)
	(PROP_MICROSECONDS ID_DQTIMEDELTA NUM__96)
	(PROP_MICROSECONDS ID_DXTIMEDELTA NUM__103)
	(PROP_MICROSECONDS ID_EETIMEDELTA NUM__110)
	(PROP_MICROSECONDS ID_ELTIMEDELTA NUM__117)
	(PROP_MICROSECONDS ID_ESTIMEDELTA NUM__124)
	(PROP_MICROSECONDS ID_EZTIMEDELTA NUM__131)
	(PROP_MICROSECONDS ID_FGTIMEDELTA NUM__138)
	(PROP_MICROSECONDS ID_FNTIMEDELTA NUM__145)
	(PROP_MICROSECONDS ID_FUTIMEDELTA NUM__152)
	(PROP_MICROSECONDS ID_GBTIMEDELTA NUM__159)
	(PROP_MICROSECONDS ID_GITIMEDELTA NUM__166)
	(PROP_MICROSECONDS ID_GPTIMEDELTA NUM__173)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_DJTIMEDELTA NUM__88)
	(PROP_SECONDS ID_DQTIMEDELTA NUM__95)
	(PROP_SECONDS ID_DXTIMEDELTA NUM__102)
	(PROP_SECONDS ID_EETIMEDELTA NUM__109)
	(PROP_SECONDS ID_ELTIMEDELTA NUM__116)
	(PROP_SECONDS ID_ESTIMEDELTA NUM__123)
	(PROP_SECONDS ID_EZTIMEDELTA NUM__130)
	(PROP_SECONDS ID_FGTIMEDELTA NUM__137)
	(PROP_SECONDS ID_FNTIMEDELTA NUM__144)
	(PROP_SECONDS ID_FUTIMEDELTA NUM__151)
	(PROP_SECONDS ID_GBTIMEDELTA NUM__158)
	(PROP_SECONDS ID_GITIMEDELTA NUM__165)
	(PROP_SECONDS ID_GPTIMEDELTA NUM__172)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_DIHOST ID_DJTIMEDELTA)
	(PROP_TIMEDELTA ID_DPHOST ID_DQTIMEDELTA)
	(PROP_TIMEDELTA ID_DWHOST ID_DXTIMEDELTA)
	(PROP_TIMEDELTA ID_EDHOST ID_EETIMEDELTA)
	(PROP_TIMEDELTA ID_EKHOST ID_ELTIMEDELTA)
	(PROP_TIMEDELTA ID_ERHOST ID_ESTIMEDELTA)
	(PROP_TIMEDELTA ID_EYHOST ID_EZTIMEDELTA)
	(PROP_TIMEDELTA ID_FFHOST ID_FGTIMEDELTA)
	(PROP_TIMEDELTA ID_FMHOST ID_FNTIMEDELTA)
	(PROP_TIMEDELTA ID_FTHOST ID_FUTIMEDELTA)
	(PROP_TIMEDELTA ID_GAHOST ID_GBTIMEDELTA)
	(PROP_TIMEDELTA ID_GHHOST ID_GITIMEDELTA)
	(PROP_TIMEDELTA ID_GOHOST ID_GPTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_HHRAT ID_EKHOST)(PROP_HOST ID_HDRAT ID_GHHOST)(PROP_HOST ID_HCRAT ID_FMHOST)(PROP_HOST ID_HERAT ID_FFHOST)(PROP_HOST ID_GZRAT ID_EDHOST)(PROP_HOST ID_GXRAT ID_GOHOST)(PROP_HOST ID_HJRAT ID_EYHOST)(PROP_HOST ID_HIRAT ID_DWHOST)(PROP_HOST ID_HBRAT ID_DIHOST)(PROP_HOST ID_HFRAT ID_DPHOST)(PROP_HOST ID_HGRAT ID_GAHOST)(PROP_HOST ID_GYRAT ID_ERHOST)))
(:metric minimize (total-cost))

)
