(define (problem p18_hosts_trial_1) (:domain CALDERA)
(:objects
	ID_HKRAT - OBSERVEDRAT
	ID_HWRAT - OBSERVEDRAT
	ID_HRRAT - OBSERVEDRAT
	ID_HURAT - OBSERVEDRAT
	ID_HSRAT - OBSERVEDRAT
	ID_HVRAT - OBSERVEDRAT
	ID_FCRAT - OBSERVEDRAT
	ID_HPRAT - OBSERVEDRAT
	ID_HTRAT - OBSERVEDRAT
	ID_HMRAT - OBSERVEDRAT
	ID_HJRAT - OBSERVEDRAT
	ID_HNRAT - OBSERVEDRAT
	ID_HQRAT - OBSERVEDRAT
	ID_HHRAT - OBSERVEDRAT
	ID_HGRAT - OBSERVEDRAT
	ID_HLRAT - OBSERVEDRAT
	ID_HORAT - OBSERVEDRAT
	ID_HXRAT - OBSERVEDRAT
	ID_HIRAT - OBSERVEDRAT
	ID_GWSHARE - OBSERVEDSHARE
	ID_GRSHARE - OBSERVEDSHARE
	ID_GXSHARE - OBSERVEDSHARE
	ID_HDSHARE - OBSERVEDSHARE
	ID_HASHARE - OBSERVEDSHARE
	ID_GVSHARE - OBSERVEDSHARE
	ID_HFSHARE - OBSERVEDSHARE
	ID_GTSHARE - OBSERVEDSHARE
	ID_GPSHARE - OBSERVEDSHARE
	ID_GOSHARE - OBSERVEDSHARE
	ID_HESHARE - OBSERVEDSHARE
	ID_GUSHARE - OBSERVEDSHARE
	ID_GYSHARE - OBSERVEDSHARE
	ID_HCSHARE - OBSERVEDSHARE
	ID_GQSHARE - OBSERVEDSHARE
	ID_GSSHARE - OBSERVEDSHARE
	ID_GZSHARE - OBSERVEDSHARE
	ID_HBSHARE - OBSERVEDSHARE
	ID_EHHOST - OBSERVEDHOST
	ID_EAHOST - OBSERVEDHOST
	ID_EVHOST - OBSERVEDHOST
	ID_CKHOST - OBSERVEDHOST
	ID_GHOST - OBSERVEDHOST
	ID_BBHOST - OBSERVEDHOST
	ID_CDHOST - OBSERVEDHOST
	ID_CRHOST - OBSERVEDHOST
	ID_DMHOST - OBSERVEDHOST
	ID_CYHOST - OBSERVEDHOST
	ID_EOHOST - OBSERVEDHOST
	ID_BPHOST - OBSERVEDHOST
	ID_DTHOST - OBSERVEDHOST
	ID_DFHOST - OBSERVEDHOST
	ID_NHOST - OBSERVEDHOST
	ID_UHOST - OBSERVEDHOST
	ID_BWHOST - OBSERVEDHOST
	ID_BIHOST - OBSERVEDHOST
	ID_CZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EBTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BCTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_HTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_OTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EITIMEDELTA - OBSERVEDTIMEDELTA
	ID_DNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_VTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CETIMEDELTA - OBSERVEDTIMEDELTA
	ID_ADOMAIN - OBSERVEDDOMAIN
	STR__EU - STRING
	STR__BF - STRING
	STR__CI - STRING
	STR__EF - STRING
	STR__BT - STRING
	STR__DZ - STRING
	STR__CV - STRING
	STR__CX - STRING
	STR__CW - STRING
	STR__DS - STRING
	STR__EN - STRING
	STR__ES - STRING
	STR__FA - STRING
	STR__CB - STRING
	STR__BN - STRING
	STR__DJ - STRING
	STR__DK - STRING
	STR__Y - STRING
	STR__BV - STRING
	STR__B - STRING
	STR__ET - STRING
	STR__CQ - STRING
	STR__M - STRING
	STR__CH - STRING
	STR__DD - STRING
	STR__DQ - STRING
	STR__FB - STRING
	STR__JAMES - STRING
	STR__DE - STRING
	STR__DC - STRING
	STR__EL - STRING
	STR__CJ - STRING
	STR__Z - STRING
	STR__BU - STRING
	STR__CA - STRING
	STR__ALPHA - STRING
	STR__DY - STRING
	STR__FD - STRING
	STR__CO - STRING
	STR__DX - STRING
	STR__E - STRING
	STR__BG - STRING
	STR__CC - STRING
	STR__EM - STRING
	STR__F - STRING
	STR__EE - STRING
	STR__K - STRING
	STR__DR - STRING
	STR__EZ - STRING
	STR__T - STRING
	STR__BM - STRING
	STR__BA - STRING
	STR__S - STRING
	STR__DL - STRING
	STR__BO - STRING
	STR__EG - STRING
	STR__L - STRING
	STR__CP - STRING
	STR__R - STRING
	STR__BH - STRING
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GHFILE - OBSERVEDFILE
	ID_GLFILE - OBSERVEDFILE
	ID_GKFILE - OBSERVEDFILE
	ID_GGFILE - OBSERVEDFILE
	ID_GDFILE - OBSERVEDFILE
	ID_FYFILE - OBSERVEDFILE
	ID_GJFILE - OBSERVEDFILE
	ID_GAFILE - OBSERVEDFILE
	ID_GNFILE - OBSERVEDFILE
	ID_GFFILE - OBSERVEDFILE
	ID_GMFILE - OBSERVEDFILE
	ID_GIFILE - OBSERVEDFILE
	ID_FZFILE - OBSERVEDFILE
	ID_FWFILE - OBSERVEDFILE
	ID_FXFILE - OBSERVEDFILE
	ID_GCFILE - OBSERVEDFILE
	ID_GEFILE - OBSERVEDFILE
	ID_GBFILE - OBSERVEDFILE
	ID_FTSCHTASK - OBSERVEDSCHTASK
	ID_FFSCHTASK - OBSERVEDSCHTASK
	ID_FKSCHTASK - OBSERVEDSCHTASK
	ID_FLSCHTASK - OBSERVEDSCHTASK
	ID_FPSCHTASK - OBSERVEDSCHTASK
	ID_FMSCHTASK - OBSERVEDSCHTASK
	ID_FQSCHTASK - OBSERVEDSCHTASK
	ID_FGSCHTASK - OBSERVEDSCHTASK
	ID_FISCHTASK - OBSERVEDSCHTASK
	ID_FHSCHTASK - OBSERVEDSCHTASK
	ID_FRSCHTASK - OBSERVEDSCHTASK
	ID_FOSCHTASK - OBSERVEDSCHTASK
	ID_FSSCHTASK - OBSERVEDSCHTASK
	ID_FNSCHTASK - OBSERVEDSCHTASK
	ID_FJSCHTASK - OBSERVEDSCHTASK
	ID_FUSCHTASK - OBSERVEDSCHTASK
	ID_FVSCHTASK - OBSERVEDSCHTASK
	ID_FESCHTASK - OBSERVEDSCHTASK
	NUM__37 - NUM
	NUM__58 - NUM
	NUM__64 - NUM
	NUM__113 - NUM
	NUM__9 - NUM
	NUM__16 - NUM
	NUM__100 - NUM
	NUM__114 - NUM
	NUM__44 - NUM
	NUM__57 - NUM
	NUM__43 - NUM
	NUM__128 - NUM
	NUM__71 - NUM
	NUM__72 - NUM
	NUM__99 - NUM
	NUM__30 - NUM
	NUM__36 - NUM
	NUM__86 - NUM
	NUM__29 - NUM
	NUM__15 - NUM
	NUM__92 - NUM
	NUM__79 - NUM
	NUM__8 - NUM
	NUM__23 - NUM
	NUM__93 - NUM
	NUM__121 - NUM
	NUM__127 - NUM
	NUM__107 - NUM
	NUM__120 - NUM
	NUM__50 - NUM
	NUM__106 - NUM
	NUM__22 - NUM
	NUM__85 - NUM
	NUM__78 - NUM
	NUM__65 - NUM
	NUM__51 - NUM
)
(:init
	(KNOWS ID_CYHOST)
	(KNOWS ID_FCRAT)
	(KNOWS_PROPERTY ID_CYHOST PFQDN)
	(KNOWS_PROPERTY ID_FCRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_FCRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BBHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BIHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BPHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BWHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CDHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CKHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CRHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CYHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DFHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DMHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DTHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EAHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EHHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_NHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_UHOST ID_DDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BBHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BIHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BPHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BWHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CDHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CKHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CRHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CYHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DFHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DMHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DTHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EAHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EHHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EOHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EVHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_NHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_UHOST ID_CDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BBHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DTHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EAHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EHHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EOHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GHOST)
	(MEM_HOSTS ID_ADOMAIN ID_NHOST)
	(MEM_HOSTS ID_ADOMAIN ID_UHOST)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_DC ID_BBHOST NO)
	(PROP_DC ID_BIHOST NO)
	(PROP_DC ID_BPHOST NO)
	(PROP_DC ID_BWHOST NO)
	(PROP_DC ID_CDHOST YES)
	(PROP_DC ID_CKHOST NO)
	(PROP_DC ID_CRHOST YES)
	(PROP_DC ID_CYHOST NO)
	(PROP_DC ID_DFHOST NO)
	(PROP_DC ID_DMHOST NO)
	(PROP_DC ID_DTHOST NO)
	(PROP_DC ID_EAHOST NO)
	(PROP_DC ID_EHHOST NO)
	(PROP_DC ID_EOHOST NO)
	(PROP_DC ID_EVHOST NO)
	(PROP_DC ID_GHOST YES)
	(PROP_DC ID_NHOST YES)
	(PROP_DC ID_UHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BBHOST STR__BF)
	(PROP_DNS_DOMAIN_NAME ID_BIHOST STR__BM)
	(PROP_DNS_DOMAIN_NAME ID_BPHOST STR__BT)
	(PROP_DNS_DOMAIN_NAME ID_BWHOST STR__CA)
	(PROP_DNS_DOMAIN_NAME ID_CDHOST STR__CH)
	(PROP_DNS_DOMAIN_NAME ID_CKHOST STR__CO)
	(PROP_DNS_DOMAIN_NAME ID_CRHOST STR__CV)
	(PROP_DNS_DOMAIN_NAME ID_CYHOST STR__DC)
	(PROP_DNS_DOMAIN_NAME ID_DFHOST STR__DJ)
	(PROP_DNS_DOMAIN_NAME ID_DMHOST STR__DQ)
	(PROP_DNS_DOMAIN_NAME ID_DTHOST STR__DX)
	(PROP_DNS_DOMAIN_NAME ID_EAHOST STR__EE)
	(PROP_DNS_DOMAIN_NAME ID_EHHOST STR__EL)
	(PROP_DNS_DOMAIN_NAME ID_EOHOST STR__ES)
	(PROP_DNS_DOMAIN_NAME ID_EVHOST STR__EZ)
	(PROP_DNS_DOMAIN_NAME ID_GHOST STR__K)
	(PROP_DNS_DOMAIN_NAME ID_NHOST STR__R)
	(PROP_DNS_DOMAIN_NAME ID_UHOST STR__Y)
	(PROP_DOMAIN ID_BBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EAHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EHHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_NHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_UHOST ID_ADOMAIN)
	(PROP_ELEVATED ID_FCRAT YES)
	(PROP_EXECUTABLE ID_FCRAT STR__FD)
	(PROP_FQDN ID_BBHOST STR__BH)
	(PROP_FQDN ID_BIHOST STR__BO)
	(PROP_FQDN ID_BPHOST STR__BV)
	(PROP_FQDN ID_BWHOST STR__CC)
	(PROP_FQDN ID_CDHOST STR__CJ)
	(PROP_FQDN ID_CKHOST STR__CQ)
	(PROP_FQDN ID_CRHOST STR__CX)
	(PROP_FQDN ID_CYHOST STR__DE)
	(PROP_FQDN ID_DFHOST STR__DL)
	(PROP_FQDN ID_DMHOST STR__DS)
	(PROP_FQDN ID_DTHOST STR__DZ)
	(PROP_FQDN ID_EAHOST STR__EG)
	(PROP_FQDN ID_EHHOST STR__EN)
	(PROP_FQDN ID_EOHOST STR__EU)
	(PROP_FQDN ID_EVHOST STR__FB)
	(PROP_FQDN ID_GHOST STR__M)
	(PROP_FQDN ID_NHOST STR__T)
	(PROP_FQDN ID_UHOST STR__BA)
	(PROP_HOST ID_BCTIMEDELTA ID_BBHOST)
	(PROP_HOST ID_BJTIMEDELTA ID_BIHOST)
	(PROP_HOST ID_BQTIMEDELTA ID_BPHOST)
	(PROP_HOST ID_BXTIMEDELTA ID_BWHOST)
	(PROP_HOST ID_CETIMEDELTA ID_CDHOST)
	(PROP_HOST ID_CLTIMEDELTA ID_CKHOST)
	(PROP_HOST ID_CSTIMEDELTA ID_CRHOST)
	(PROP_HOST ID_CZTIMEDELTA ID_CYHOST)
	(PROP_HOST ID_DGTIMEDELTA ID_DFHOST)
	(PROP_HOST ID_DNTIMEDELTA ID_DMHOST)
	(PROP_HOST ID_DUTIMEDELTA ID_DTHOST)
	(PROP_HOST ID_EBTIMEDELTA ID_EAHOST)
	(PROP_HOST ID_EITIMEDELTA ID_EHHOST)
	(PROP_HOST ID_EPTIMEDELTA ID_EOHOST)
	(PROP_HOST ID_EWTIMEDELTA ID_EVHOST)
	(PROP_HOST ID_FCRAT ID_CYHOST)
	(PROP_HOST ID_HTIMEDELTA ID_GHOST)
	(PROP_HOST ID_OTIMEDELTA ID_NHOST)
	(PROP_HOST ID_VTIMEDELTA ID_UHOST)
	(PROP_HOSTNAME ID_BBHOST STR__BG)
	(PROP_HOSTNAME ID_BIHOST STR__BN)
	(PROP_HOSTNAME ID_BPHOST STR__BU)
	(PROP_HOSTNAME ID_BWHOST STR__CB)
	(PROP_HOSTNAME ID_CDHOST STR__CI)
	(PROP_HOSTNAME ID_CKHOST STR__CP)
	(PROP_HOSTNAME ID_CRHOST STR__CW)
	(PROP_HOSTNAME ID_CYHOST STR__DD)
	(PROP_HOSTNAME ID_DFHOST STR__DK)
	(PROP_HOSTNAME ID_DMHOST STR__DR)
	(PROP_HOSTNAME ID_DTHOST STR__DY)
	(PROP_HOSTNAME ID_EAHOST STR__EF)
	(PROP_HOSTNAME ID_EHHOST STR__EM)
	(PROP_HOSTNAME ID_EOHOST STR__ET)
	(PROP_HOSTNAME ID_EVHOST STR__FA)
	(PROP_HOSTNAME ID_GHOST STR__L)
	(PROP_HOSTNAME ID_NHOST STR__S)
	(PROP_HOSTNAME ID_UHOST STR__Z)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BCTIMEDELTA NUM__30)
	(PROP_MICROSECONDS ID_BJTIMEDELTA NUM__37)
	(PROP_MICROSECONDS ID_BQTIMEDELTA NUM__44)
	(PROP_MICROSECONDS ID_BXTIMEDELTA NUM__51)
	(PROP_MICROSECONDS ID_CETIMEDELTA NUM__58)
	(PROP_MICROSECONDS ID_CLTIMEDELTA NUM__65)
	(PROP_MICROSECONDS ID_CSTIMEDELTA NUM__72)
	(PROP_MICROSECONDS ID_CZTIMEDELTA NUM__79)
	(PROP_MICROSECONDS ID_DGTIMEDELTA NUM__86)
	(PROP_MICROSECONDS ID_DNTIMEDELTA NUM__93)
	(PROP_MICROSECONDS ID_DUTIMEDELTA NUM__100)
	(PROP_MICROSECONDS ID_EBTIMEDELTA NUM__107)
	(PROP_MICROSECONDS ID_EITIMEDELTA NUM__114)
	(PROP_MICROSECONDS ID_EPTIMEDELTA NUM__121)
	(PROP_MICROSECONDS ID_EWTIMEDELTA NUM__128)
	(PROP_MICROSECONDS ID_HTIMEDELTA NUM__9)
	(PROP_MICROSECONDS ID_OTIMEDELTA NUM__16)
	(PROP_MICROSECONDS ID_VTIMEDELTA NUM__23)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_SECONDS ID_BCTIMEDELTA NUM__29)
	(PROP_SECONDS ID_BJTIMEDELTA NUM__36)
	(PROP_SECONDS ID_BQTIMEDELTA NUM__43)
	(PROP_SECONDS ID_BXTIMEDELTA NUM__50)
	(PROP_SECONDS ID_CETIMEDELTA NUM__57)
	(PROP_SECONDS ID_CLTIMEDELTA NUM__64)
	(PROP_SECONDS ID_CSTIMEDELTA NUM__71)
	(PROP_SECONDS ID_CZTIMEDELTA NUM__78)
	(PROP_SECONDS ID_DGTIMEDELTA NUM__85)
	(PROP_SECONDS ID_DNTIMEDELTA NUM__92)
	(PROP_SECONDS ID_DUTIMEDELTA NUM__99)
	(PROP_SECONDS ID_EBTIMEDELTA NUM__106)
	(PROP_SECONDS ID_EITIMEDELTA NUM__113)
	(PROP_SECONDS ID_EPTIMEDELTA NUM__120)
	(PROP_SECONDS ID_EWTIMEDELTA NUM__127)
	(PROP_SECONDS ID_HTIMEDELTA NUM__8)
	(PROP_SECONDS ID_OTIMEDELTA NUM__15)
	(PROP_SECONDS ID_VTIMEDELTA NUM__22)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_TIMEDELTA ID_BBHOST ID_BCTIMEDELTA)
	(PROP_TIMEDELTA ID_BIHOST ID_BJTIMEDELTA)
	(PROP_TIMEDELTA ID_BPHOST ID_BQTIMEDELTA)
	(PROP_TIMEDELTA ID_BWHOST ID_BXTIMEDELTA)
	(PROP_TIMEDELTA ID_CDHOST ID_CETIMEDELTA)
	(PROP_TIMEDELTA ID_CKHOST ID_CLTIMEDELTA)
	(PROP_TIMEDELTA ID_CRHOST ID_CSTIMEDELTA)
	(PROP_TIMEDELTA ID_CYHOST ID_CZTIMEDELTA)
	(PROP_TIMEDELTA ID_DFHOST ID_DGTIMEDELTA)
	(PROP_TIMEDELTA ID_DMHOST ID_DNTIMEDELTA)
	(PROP_TIMEDELTA ID_DTHOST ID_DUTIMEDELTA)
	(PROP_TIMEDELTA ID_EAHOST ID_EBTIMEDELTA)
	(PROP_TIMEDELTA ID_EHHOST ID_EITIMEDELTA)
	(PROP_TIMEDELTA ID_EOHOST ID_EPTIMEDELTA)
	(PROP_TIMEDELTA ID_EVHOST ID_EWTIMEDELTA)
	(PROP_TIMEDELTA ID_GHOST ID_HTIMEDELTA)
	(PROP_TIMEDELTA ID_NHOST ID_OTIMEDELTA)
	(PROP_TIMEDELTA ID_UHOST ID_VTIMEDELTA)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_HLRAT ID_DFHOST)(PROP_HOST ID_HGRAT ID_DTHOST)(PROP_HOST ID_HHRAT ID_DMHOST)(PROP_HOST ID_HJRAT ID_CRHOST)(PROP_HOST ID_HMRAT ID_BBHOST)(PROP_HOST ID_HPRAT ID_GHOST)(PROP_HOST ID_HURAT ID_EVHOST)(PROP_HOST ID_HWRAT ID_BIHOST)(PROP_HOST ID_HKRAT ID_BWHOST)(PROP_HOST ID_HIRAT ID_UHOST)(PROP_HOST ID_HSRAT ID_BPHOST)(PROP_HOST ID_HQRAT ID_EOHOST)(PROP_HOST ID_HNRAT ID_CDHOST)(PROP_HOST ID_HTRAT ID_CKHOST)(PROP_HOST ID_HVRAT ID_EAHOST)(PROP_HOST ID_HRRAT ID_EHHOST)(PROP_HOST ID_HXRAT ID_NHOST)))
(:metric minimize (total-cost))

)
