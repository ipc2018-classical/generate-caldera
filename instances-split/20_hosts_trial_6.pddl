(define (problem p20_hosts_trial_6) (:domain CALDERA)
(:objects
	NUM__87 - NUM
	NUM__114 - NUM
	NUM__73 - NUM
	NUM__177 - NUM
	NUM__58 - NUM
	NUM__94 - NUM
	NUM__101 - NUM
	NUM__107 - NUM
	NUM__80 - NUM
	NUM__44 - NUM
	NUM__142 - NUM
	NUM__129 - NUM
	NUM__93 - NUM
	NUM__156 - NUM
	NUM__108 - NUM
	NUM__143 - NUM
	NUM__128 - NUM
	NUM__178 - NUM
	NUM__72 - NUM
	NUM__164 - NUM
	NUM__65 - NUM
	NUM__170 - NUM
	NUM__86 - NUM
	NUM__121 - NUM
	NUM__150 - NUM
	NUM__149 - NUM
	NUM__163 - NUM
	NUM__122 - NUM
	NUM__136 - NUM
	NUM__79 - NUM
	NUM__157 - NUM
	NUM__51 - NUM
	NUM__171 - NUM
	NUM__115 - NUM
	NUM__45 - NUM
	NUM__59 - NUM
	NUM__66 - NUM
	NUM__135 - NUM
	NUM__100 - NUM
	NUM__52 - NUM
	STR__CI - STRING
	STR__GS - STRING
	STR__FQ - STRING
	STR__EN - STRING
	STR__DS - STRING
	STR__FI - STRING
	STR__EU - STRING
	STR__BV - STRING
	STR__CR - STRING
	STR__I - STRING
	STR__CX - STRING
	STR__BARBARA - STRING
	STR__JAMES - STRING
	STR__DZ - STRING
	STR__Y - STRING
	STR__BW - STRING
	STR__CC - STRING
	STR__CB - STRING
	STR__Q - STRING
	STR__CJ - STRING
	STR__CQ - STRING
	STR__B - STRING
	STR__MICHAEL - STRING
	STR__BO - STRING
	STR__FV - STRING
	STR__DM - STRING
	STR__JOHN - STRING
	STR__F - STRING
	STR__FW - STRING
	STR__DY - STRING
	STR__BC - STRING
	STR__GX - STRING
	STR__ELIZABETH - STRING
	STR__GC - STRING
	STR__DR - STRING
	STR__GQ - STRING
	STR__GK - STRING
	STR__GY - STRING
	STR__EM - STRING
	STR__GR - STRING
	STR__ALPHA - STRING
	STR__GL - STRING
	STR__ET - STRING
	STR__U - STRING
	STR__LINDA - STRING
	STR__HB - STRING
	STR__WILLIAM - STRING
	STR__DD - STRING
	STR__N - STRING
	STR__FJ - STRING
	STR__Z - STRING
	STR__E - STRING
	STR__FP - STRING
	STR__BH - STRING
	STR__FX - STRING
	STR__FH - STRING
	STR__BL - STRING
	STR__CK - STRING
	STR__V - STRING
	STR__BG - STRING
	STR__CW - STRING
	STR__EG - STRING
	STR__CY - STRING
	STR__DE - STRING
	STR__GD - STRING
	STR__EV - STRING
	STR__FO - STRING
	STR__DL - STRING
	STR__FB - STRING
	STR__CD - STRING
	STR__BU - STRING
	STR__BP - STRING
	STR__MARY - STRING
	STR__ROBERT - STRING
	STR__GE - STRING
	STR__M - STRING
	STR__DK - STRING
	STR__EF - STRING
	STR__GZ - STRING
	STR__DF - STRING
	STR__EO - STRING
	STR__FC - STRING
	STR__DT - STRING
	STR__GJ - STRING
	STR__PATRICIA - STRING
	STR__CP - STRING
	STR__EH - STRING
	STR__BD - STRING
	STR__FA - STRING
	STR__R - STRING
	STR__EA - STRING
	STR__J - STRING
	STR__BK - STRING
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DGHOST - OBSERVEDHOST
	ID_CZHOST - OBSERVEDHOST
	ID_DNHOST - OBSERVEDHOST
	ID_DUHOST - OBSERVEDHOST
	ID_EBHOST - OBSERVEDHOST
	ID_EWHOST - OBSERVEDHOST
	ID_FRHOST - OBSERVEDHOST
	ID_FYHOST - OBSERVEDHOST
	ID_EPHOST - OBSERVEDHOST
	ID_GMHOST - OBSERVEDHOST
	ID_BQHOST - OBSERVEDHOST
	ID_BXHOST - OBSERVEDHOST
	ID_FDHOST - OBSERVEDHOST
	ID_CEHOST - OBSERVEDHOST
	ID_EIHOST - OBSERVEDHOST
	ID_GTHOST - OBSERVEDHOST
	ID_GFHOST - OBSERVEDHOST
	ID_FKHOST - OBSERVEDHOST
	ID_CSHOST - OBSERVEDHOST
	ID_CLHOST - OBSERVEDHOST
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_HUSHARE - OBSERVEDSHARE
	ID_HSSHARE - OBSERVEDSHARE
	ID_HISHARE - OBSERVEDSHARE
	ID_HRSHARE - OBSERVEDSHARE
	ID_HLSHARE - OBSERVEDSHARE
	ID_HOSHARE - OBSERVEDSHARE
	ID_HTSHARE - OBSERVEDSHARE
	ID_HKSHARE - OBSERVEDSHARE
	ID_HQSHARE - OBSERVEDSHARE
	ID_HPSHARE - OBSERVEDSHARE
	ID_HCSHARE - OBSERVEDSHARE
	ID_HGSHARE - OBSERVEDSHARE
	ID_HMSHARE - OBSERVEDSHARE
	ID_HESHARE - OBSERVEDSHARE
	ID_HVSHARE - OBSERVEDSHARE
	ID_HNSHARE - OBSERVEDSHARE
	ID_HJSHARE - OBSERVEDSHARE
	ID_HHSHARE - OBSERVEDSHARE
	ID_HFSHARE - OBSERVEDSHARE
	ID_HDSHARE - OBSERVEDSHARE
	ID_ISRAT - OBSERVEDRAT
	ID_ITRAT - OBSERVEDRAT
	ID_IWRAT - OBSERVEDRAT
	ID_IQRAT - OBSERVEDRAT
	ID_IYRAT - OBSERVEDRAT
	ID_JJRAT - OBSERVEDRAT
	ID_IURAT - OBSERVEDRAT
	ID_JBRAT - OBSERVEDRAT
	ID_JFRAT - OBSERVEDRAT
	ID_JDRAT - OBSERVEDRAT
	ID_HARAT - OBSERVEDRAT
	ID_JHRAT - OBSERVEDRAT
	ID_IVRAT - OBSERVEDRAT
	ID_IXRAT - OBSERVEDRAT
	ID_JGRAT - OBSERVEDRAT
	ID_JCRAT - OBSERVEDRAT
	ID_JIRAT - OBSERVEDRAT
	ID_JERAT - OBSERVEDRAT
	ID_IZRAT - OBSERVEDRAT
	ID_IRRAT - OBSERVEDRAT
	ID_JARAT - OBSERVEDRAT
	ID_CTTIMEDELTA - OBSERVEDTIMEDELTA
	ID_BYTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EJTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EQTIMEDELTA - OBSERVEDTIMEDELTA
	ID_ECTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DHTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FLTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FETIMEDELTA - OBSERVEDTIMEDELTA
	ID_GGTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EXTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FSTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CFTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GNTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DATIMEDELTA - OBSERVEDTIMEDELTA
	ID_BRTIMEDELTA - OBSERVEDTIMEDELTA
	ID_CMTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DVTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FZTIMEDELTA - OBSERVEDTIMEDELTA
	ID_GUTIMEDELTA - OBSERVEDTIMEDELTA
	ID_DOTIMEDELTA - OBSERVEDTIMEDELTA
	ID_IASCHTASK - OBSERVEDSCHTASK
	ID_HZSCHTASK - OBSERVEDSCHTASK
	ID_INSCHTASK - OBSERVEDSCHTASK
	ID_IPSCHTASK - OBSERVEDSCHTASK
	ID_ILSCHTASK - OBSERVEDSCHTASK
	ID_IGSCHTASK - OBSERVEDSCHTASK
	ID_HXSCHTASK - OBSERVEDSCHTASK
	ID_IOSCHTASK - OBSERVEDSCHTASK
	ID_HYSCHTASK - OBSERVEDSCHTASK
	ID_IBSCHTASK - OBSERVEDSCHTASK
	ID_ICSCHTASK - OBSERVEDSCHTASK
	ID_IESCHTASK - OBSERVEDSCHTASK
	ID_IHSCHTASK - OBSERVEDSCHTASK
	ID_HWSCHTASK - OBSERVEDSCHTASK
	ID_IMSCHTASK - OBSERVEDSCHTASK
	ID_IFSCHTASK - OBSERVEDSCHTASK
	ID_IJSCHTASK - OBSERVEDSCHTASK
	ID_IDSCHTASK - OBSERVEDSCHTASK
	ID_IISCHTASK - OBSERVEDSCHTASK
	ID_IKSCHTASK - OBSERVEDSCHTASK
	ID_KBFILE - OBSERVEDFILE
	ID_JWFILE - OBSERVEDFILE
	ID_JVFILE - OBSERVEDFILE
	ID_KCFILE - OBSERVEDFILE
	ID_JKFILE - OBSERVEDFILE
	ID_JOFILE - OBSERVEDFILE
	ID_JLFILE - OBSERVEDFILE
	ID_JUFILE - OBSERVEDFILE
	ID_JYFILE - OBSERVEDFILE
	ID_JSFILE - OBSERVEDFILE
	ID_JQFILE - OBSERVEDFILE
	ID_JZFILE - OBSERVEDFILE
	ID_JNFILE - OBSERVEDFILE
	ID_JPFILE - OBSERVEDFILE
	ID_KDFILE - OBSERVEDFILE
	ID_JMFILE - OBSERVEDFILE
	ID_JRFILE - OBSERVEDFILE
	ID_JTFILE - OBSERVEDFILE
	ID_JXFILE - OBSERVEDFILE
	ID_KAFILE - OBSERVEDFILE
	ID_ADOMAIN - OBSERVEDDOMAIN
)
(:init
	(KNOWS ID_EBHOST)
	(KNOWS ID_HARAT)
	(KNOWS_PROPERTY ID_EBHOST PFQDN)
	(KNOWS_PROPERTY ID_HARAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_HARAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BQHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_BXHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CEHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CLHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CSHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_CZHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DGHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DNHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_DUHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EBHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EIHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EPHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EWHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FDHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FKHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_BJDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FRHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FYHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_DDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_HDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GFHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_BNDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_PDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GMHOST ID_XDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_BBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_BFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_TDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_GTHOST ID_XDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BQHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_BXHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CEHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CLHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CSHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_CZHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DGHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DNHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_DUHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EBHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EIHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EPHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EWHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FDHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FKHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_CDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FRHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FYHOST ID_WDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_BIDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GFHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_BMDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_ODOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GMHOST ID_SDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_BADOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_BEDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_GDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_KDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_GTHOST ID_ODOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_BQHOST)
	(MEM_HOSTS ID_ADOMAIN ID_BXHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CEHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CLHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CSHOST)
	(MEM_HOSTS ID_ADOMAIN ID_CZHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DGHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DNHOST)
	(MEM_HOSTS ID_ADOMAIN ID_DUHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EBHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EIHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EPHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EWHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FDHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FKHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FRHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FYHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GFHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GMHOST)
	(MEM_HOSTS ID_ADOMAIN ID_GTHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_BQHOST YES)
	(PROP_DC ID_BXHOST NO)
	(PROP_DC ID_CEHOST NO)
	(PROP_DC ID_CLHOST NO)
	(PROP_DC ID_CSHOST NO)
	(PROP_DC ID_CZHOST NO)
	(PROP_DC ID_DGHOST NO)
	(PROP_DC ID_DNHOST NO)
	(PROP_DC ID_DUHOST YES)
	(PROP_DC ID_EBHOST NO)
	(PROP_DC ID_EIHOST NO)
	(PROP_DC ID_EPHOST NO)
	(PROP_DC ID_EWHOST NO)
	(PROP_DC ID_FDHOST NO)
	(PROP_DC ID_FKHOST NO)
	(PROP_DC ID_FRHOST NO)
	(PROP_DC ID_FYHOST NO)
	(PROP_DC ID_GFHOST NO)
	(PROP_DC ID_GMHOST NO)
	(PROP_DC ID_GTHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_BQHOST STR__BW)
	(PROP_DNS_DOMAIN_NAME ID_BXHOST STR__CD)
	(PROP_DNS_DOMAIN_NAME ID_CEHOST STR__CK)
	(PROP_DNS_DOMAIN_NAME ID_CLHOST STR__CR)
	(PROP_DNS_DOMAIN_NAME ID_CSHOST STR__CY)
	(PROP_DNS_DOMAIN_NAME ID_CZHOST STR__DF)
	(PROP_DNS_DOMAIN_NAME ID_DGHOST STR__DM)
	(PROP_DNS_DOMAIN_NAME ID_DNHOST STR__DT)
	(PROP_DNS_DOMAIN_NAME ID_DUHOST STR__EA)
	(PROP_DNS_DOMAIN_NAME ID_EBHOST STR__EH)
	(PROP_DNS_DOMAIN_NAME ID_EIHOST STR__EO)
	(PROP_DNS_DOMAIN_NAME ID_EPHOST STR__EV)
	(PROP_DNS_DOMAIN_NAME ID_EWHOST STR__FC)
	(PROP_DNS_DOMAIN_NAME ID_FDHOST STR__FJ)
	(PROP_DNS_DOMAIN_NAME ID_FKHOST STR__FQ)
	(PROP_DNS_DOMAIN_NAME ID_FRHOST STR__FX)
	(PROP_DNS_DOMAIN_NAME ID_FYHOST STR__GE)
	(PROP_DNS_DOMAIN_NAME ID_GFHOST STR__GL)
	(PROP_DNS_DOMAIN_NAME ID_GMHOST STR__GS)
	(PROP_DNS_DOMAIN_NAME ID_GTHOST STR__GZ)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_BXHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CEHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CLHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CSHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_CZHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DGHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DNHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_DUHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EBHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EIHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EPHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EWHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FDHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FKHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FRHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FYHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_GFHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GMHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GTHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_HARAT YES)
	(PROP_EXECUTABLE ID_HARAT STR__HB)
	(PROP_FQDN ID_BQHOST STR__BU)
	(PROP_FQDN ID_BXHOST STR__CB)
	(PROP_FQDN ID_CEHOST STR__CI)
	(PROP_FQDN ID_CLHOST STR__CP)
	(PROP_FQDN ID_CSHOST STR__CW)
	(PROP_FQDN ID_CZHOST STR__DD)
	(PROP_FQDN ID_DGHOST STR__DK)
	(PROP_FQDN ID_DNHOST STR__DR)
	(PROP_FQDN ID_DUHOST STR__DY)
	(PROP_FQDN ID_EBHOST STR__EF)
	(PROP_FQDN ID_EIHOST STR__EM)
	(PROP_FQDN ID_EPHOST STR__ET)
	(PROP_FQDN ID_EWHOST STR__FA)
	(PROP_FQDN ID_FDHOST STR__FH)
	(PROP_FQDN ID_FKHOST STR__FO)
	(PROP_FQDN ID_FRHOST STR__FV)
	(PROP_FQDN ID_FYHOST STR__GC)
	(PROP_FQDN ID_GFHOST STR__GJ)
	(PROP_FQDN ID_GMHOST STR__GQ)
	(PROP_FQDN ID_GTHOST STR__GX)
	(PROP_HOST ID_BRTIMEDELTA ID_BQHOST)
	(PROP_HOST ID_BYTIMEDELTA ID_BXHOST)
	(PROP_HOST ID_CFTIMEDELTA ID_CEHOST)
	(PROP_HOST ID_CMTIMEDELTA ID_CLHOST)
	(PROP_HOST ID_CTTIMEDELTA ID_CSHOST)
	(PROP_HOST ID_DATIMEDELTA ID_CZHOST)
	(PROP_HOST ID_DHTIMEDELTA ID_DGHOST)
	(PROP_HOST ID_DOTIMEDELTA ID_DNHOST)
	(PROP_HOST ID_DVTIMEDELTA ID_DUHOST)
	(PROP_HOST ID_ECTIMEDELTA ID_EBHOST)
	(PROP_HOST ID_EJTIMEDELTA ID_EIHOST)
	(PROP_HOST ID_EQTIMEDELTA ID_EPHOST)
	(PROP_HOST ID_EXTIMEDELTA ID_EWHOST)
	(PROP_HOST ID_FETIMEDELTA ID_FDHOST)
	(PROP_HOST ID_FLTIMEDELTA ID_FKHOST)
	(PROP_HOST ID_FSTIMEDELTA ID_FRHOST)
	(PROP_HOST ID_FZTIMEDELTA ID_FYHOST)
	(PROP_HOST ID_GGTIMEDELTA ID_GFHOST)
	(PROP_HOST ID_GNTIMEDELTA ID_GMHOST)
	(PROP_HOST ID_GUTIMEDELTA ID_GTHOST)
	(PROP_HOST ID_HARAT ID_EBHOST)
	(PROP_HOSTNAME ID_BQHOST STR__BV)
	(PROP_HOSTNAME ID_BXHOST STR__CC)
	(PROP_HOSTNAME ID_CEHOST STR__CJ)
	(PROP_HOSTNAME ID_CLHOST STR__CQ)
	(PROP_HOSTNAME ID_CSHOST STR__CX)
	(PROP_HOSTNAME ID_CZHOST STR__DE)
	(PROP_HOSTNAME ID_DGHOST STR__DL)
	(PROP_HOSTNAME ID_DNHOST STR__DS)
	(PROP_HOSTNAME ID_DUHOST STR__DZ)
	(PROP_HOSTNAME ID_EBHOST STR__EG)
	(PROP_HOSTNAME ID_EIHOST STR__EN)
	(PROP_HOSTNAME ID_EPHOST STR__EU)
	(PROP_HOSTNAME ID_EWHOST STR__FB)
	(PROP_HOSTNAME ID_FDHOST STR__FI)
	(PROP_HOSTNAME ID_FKHOST STR__FP)
	(PROP_HOSTNAME ID_FRHOST STR__FW)
	(PROP_HOSTNAME ID_FYHOST STR__GD)
	(PROP_HOSTNAME ID_GFHOST STR__GK)
	(PROP_HOSTNAME ID_GMHOST STR__GR)
	(PROP_HOSTNAME ID_GTHOST STR__GY)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_BRTIMEDELTA NUM__44)
	(PROP_MICROSECONDS ID_BYTIMEDELTA NUM__51)
	(PROP_MICROSECONDS ID_CFTIMEDELTA NUM__58)
	(PROP_MICROSECONDS ID_CMTIMEDELTA NUM__65)
	(PROP_MICROSECONDS ID_CTTIMEDELTA NUM__72)
	(PROP_MICROSECONDS ID_DATIMEDELTA NUM__79)
	(PROP_MICROSECONDS ID_DHTIMEDELTA NUM__86)
	(PROP_MICROSECONDS ID_DOTIMEDELTA NUM__93)
	(PROP_MICROSECONDS ID_DVTIMEDELTA NUM__100)
	(PROP_MICROSECONDS ID_ECTIMEDELTA NUM__107)
	(PROP_MICROSECONDS ID_EJTIMEDELTA NUM__114)
	(PROP_MICROSECONDS ID_EQTIMEDELTA NUM__121)
	(PROP_MICROSECONDS ID_EXTIMEDELTA NUM__128)
	(PROP_MICROSECONDS ID_FETIMEDELTA NUM__135)
	(PROP_MICROSECONDS ID_FLTIMEDELTA NUM__142)
	(PROP_MICROSECONDS ID_FSTIMEDELTA NUM__149)
	(PROP_MICROSECONDS ID_FZTIMEDELTA NUM__156)
	(PROP_MICROSECONDS ID_GGTIMEDELTA NUM__163)
	(PROP_MICROSECONDS ID_GNTIMEDELTA NUM__170)
	(PROP_MICROSECONDS ID_GUTIMEDELTA NUM__177)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_BRTIMEDELTA NUM__45)
	(PROP_SECONDS ID_BYTIMEDELTA NUM__52)
	(PROP_SECONDS ID_CFTIMEDELTA NUM__59)
	(PROP_SECONDS ID_CMTIMEDELTA NUM__66)
	(PROP_SECONDS ID_CTTIMEDELTA NUM__73)
	(PROP_SECONDS ID_DATIMEDELTA NUM__80)
	(PROP_SECONDS ID_DHTIMEDELTA NUM__87)
	(PROP_SECONDS ID_DOTIMEDELTA NUM__94)
	(PROP_SECONDS ID_DVTIMEDELTA NUM__101)
	(PROP_SECONDS ID_ECTIMEDELTA NUM__108)
	(PROP_SECONDS ID_EJTIMEDELTA NUM__115)
	(PROP_SECONDS ID_EQTIMEDELTA NUM__122)
	(PROP_SECONDS ID_EXTIMEDELTA NUM__129)
	(PROP_SECONDS ID_FETIMEDELTA NUM__136)
	(PROP_SECONDS ID_FLTIMEDELTA NUM__143)
	(PROP_SECONDS ID_FSTIMEDELTA NUM__150)
	(PROP_SECONDS ID_FZTIMEDELTA NUM__157)
	(PROP_SECONDS ID_GGTIMEDELTA NUM__164)
	(PROP_SECONDS ID_GNTIMEDELTA NUM__171)
	(PROP_SECONDS ID_GUTIMEDELTA NUM__178)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_BQHOST ID_BRTIMEDELTA)
	(PROP_TIMEDELTA ID_BXHOST ID_BYTIMEDELTA)
	(PROP_TIMEDELTA ID_CEHOST ID_CFTIMEDELTA)
	(PROP_TIMEDELTA ID_CLHOST ID_CMTIMEDELTA)
	(PROP_TIMEDELTA ID_CSHOST ID_CTTIMEDELTA)
	(PROP_TIMEDELTA ID_CZHOST ID_DATIMEDELTA)
	(PROP_TIMEDELTA ID_DGHOST ID_DHTIMEDELTA)
	(PROP_TIMEDELTA ID_DNHOST ID_DOTIMEDELTA)
	(PROP_TIMEDELTA ID_DUHOST ID_DVTIMEDELTA)
	(PROP_TIMEDELTA ID_EBHOST ID_ECTIMEDELTA)
	(PROP_TIMEDELTA ID_EIHOST ID_EJTIMEDELTA)
	(PROP_TIMEDELTA ID_EPHOST ID_EQTIMEDELTA)
	(PROP_TIMEDELTA ID_EWHOST ID_EXTIMEDELTA)
	(PROP_TIMEDELTA ID_FDHOST ID_FETIMEDELTA)
	(PROP_TIMEDELTA ID_FKHOST ID_FLTIMEDELTA)
	(PROP_TIMEDELTA ID_FRHOST ID_FSTIMEDELTA)
	(PROP_TIMEDELTA ID_FYHOST ID_FZTIMEDELTA)
	(PROP_TIMEDELTA ID_GFHOST ID_GGTIMEDELTA)
	(PROP_TIMEDELTA ID_GMHOST ID_GNTIMEDELTA)
	(PROP_TIMEDELTA ID_GTHOST ID_GUTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_JFRAT ID_FKHOST)(PROP_HOST ID_JBRAT ID_FDHOST)(PROP_HOST ID_IURAT ID_EWHOST)(PROP_HOST ID_JJRAT ID_DUHOST)(PROP_HOST ID_IQRAT ID_DNHOST)(PROP_HOST ID_IWRAT ID_FRHOST)(PROP_HOST ID_JARAT ID_DGHOST)(PROP_HOST ID_IRRAT ID_CLHOST)(PROP_HOST ID_JERAT ID_GFHOST)(PROP_HOST ID_JHRAT ID_GTHOST)(PROP_HOST ID_IZRAT ID_EIHOST)(PROP_HOST ID_JIRAT ID_CEHOST)(PROP_HOST ID_IXRAT ID_BXHOST)(PROP_HOST ID_IVRAT ID_BQHOST)(PROP_HOST ID_JDRAT ID_EPHOST)(PROP_HOST ID_IYRAT ID_GMHOST)(PROP_HOST ID_ITRAT ID_FYHOST)(PROP_HOST ID_ISRAT ID_CZHOST)(PROP_HOST ID_JCRAT ID_CSHOST)))
(:metric minimize (total-cost))

)
