(define (problem p3_hosts_trial_7) (:domain CALDERA)
(:objects
	ID_FORAT - OBSERVEDRAT
	ID_FQRAT - OBSERVEDRAT
	ID_FJRAT - OBSERVEDRAT
	ID_FPRAT - OBSERVEDRAT
	ID_EGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CODOMAINUSER - OBSERVEDDOMAINUSER
	ID_WDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DADOMAINUSER - OBSERVEDDOMAINUSER
	ID_KDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DIDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BADOMAINUSER - OBSERVEDDOMAINUSER
	ID_CWDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CGDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CSDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BQDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ODOMAINUSER - OBSERVEDDOMAINUSER
	ID_DMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_GDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BMDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_ECDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_EKDOMAINUSER - OBSERVEDDOMAINUSER
	ID_DYDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BEDOMAINUSER - OBSERVEDDOMAINUSER
	ID_BUDOMAINUSER - OBSERVEDDOMAINUSER
	ID_SDOMAINUSER - OBSERVEDDOMAINUSER
	ID_CCDOMAINUSER - OBSERVEDDOMAINUSER
	NUM__120 - NUM
	NUM__127 - NUM
	NUM__135 - NUM
	NUM__128 - NUM
	NUM__134 - NUM
	NUM__121 - NUM
	ID_ADOMAIN - OBSERVEDDOMAIN
	ID_EVHOST - OBSERVEDHOST
	ID_FCHOST - OBSERVEDHOST
	ID_EOHOST - OBSERVEDHOST
	ID_EWTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FDTIMEDELTA - OBSERVEDTIMEDELTA
	ID_EPTIMEDELTA - OBSERVEDTIMEDELTA
	ID_FLSHARE - OBSERVEDSHARE
	ID_FNSHARE - OBSERVEDSHARE
	ID_FMSHARE - OBSERVEDSHARE
	ID_FVSCHTASK - OBSERVEDSCHTASK
	ID_FWSCHTASK - OBSERVEDSCHTASK
	ID_FUSCHTASK - OBSERVEDSCHTASK
	ID_BNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_XDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_TDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DNDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_LDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BFDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CPDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_PDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CTDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_ELDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DBDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CLDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CXDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DRDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BZDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_HDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_CHDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_BVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DJDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_EDDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	ID_DVDOMAINCREDENTIAL - OBSERVEDDOMAINCREDENTIAL
	STR__F - STRING
	STR__CA - STRING
	STR__CY - STRING
	STR__Y - STRING
	STR__KAREN - STRING
	STR__CE - STRING
	STR__MARY - STRING
	STR__E - STRING
	STR__MARK - STRING
	STR__DO - STRING
	STR__DW - STRING
	STR__Z - STRING
	STR__BX - STRING
	STR__THOMAS - STRING
	STR__EE - STRING
	STR__CZ - STRING
	STR__BETTY - STRING
	STR__CV - STRING
	STR__JENNIFER - STRING
	STR__BK - STRING
	STR__EF - STRING
	STR__DP - STRING
	STR__MARIA - STRING
	STR__CQ - STRING
	STR__DK - STRING
	STR__EB - STRING
	STR__V - STRING
	STR__EA - STRING
	STR__PAUL - STRING
	STR__DONALD - STRING
	STR__FK - STRING
	STR__FB - STRING
	STR__EU - STRING
	STR__ES - STRING
	STR__SUSAN - STRING
	STR__FG - STRING
	STR__CR - STRING
	STR__J - STRING
	STR__BARBARA - STRING
	STR__FH - STRING
	STR__JAMES - STRING
	STR__PATRICIA - STRING
	STR__CM - STRING
	STR__BG - STRING
	STR__CI - STRING
	STR__DANIEL - STRING
	STR__CJ - STRING
	STR__CF - STRING
	STR__B - STRING
	STR__MICHAEL - STRING
	STR__M - STRING
	STR__NANCY - STRING
	STR__ET - STRING
	STR__ROBERT - STRING
	STR__DAVID - STRING
	STR__Q - STRING
	STR__BO - STRING
	STR__ELIZABETH - STRING
	STR__EJ - STRING
	STR__EN - STRING
	STR__BS - STRING
	STR__EZ - STRING
	STR__RICHARD - STRING
	STR__FI - STRING
	STR__DX - STRING
	STR__DG - STRING
	STR__LISA - STRING
	STR__CN - STRING
	STR__ALPHA - STRING
	STR__DC - STRING
	STR__DD - STRING
	STR__U - STRING
	STR__DL - STRING
	STR__BH - STRING
	STR__FA - STRING
	STR__JOSEPH - STRING
	STR__MARGARET - STRING
	STR__CHARLES - STRING
	STR__CU - STRING
	STR__DH - STRING
	STR__EM - STRING
	STR__R - STRING
	STR__CHRISTOPHER - STRING
	STR__JOHN - STRING
	STR__N - STRING
	STR__DS - STRING
	STR__BT - STRING
	STR__BD - STRING
	STR__BP - STRING
	STR__I - STRING
	STR__EI - STRING
	STR__LINDA - STRING
	STR__WILLIAM - STRING
	STR__DT - STRING
	STR__CB - STRING
	STR__BW - STRING
	STR__DOROTHY - STRING
	STR__BC - STRING
	STR__BL - STRING
	ID_FSFILE - OBSERVEDFILE
	ID_FTFILE - OBSERVEDFILE
	ID_FRFILE - OBSERVEDFILE
)
(:init
	(KNOWS ID_EOHOST)
	(KNOWS ID_FJRAT)
	(KNOWS_PROPERTY ID_EOHOST PFQDN)
	(KNOWS_PROPERTY ID_FJRAT PEXECUTABLE)
	(KNOWS_PROPERTY ID_FJRAT PHOST)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_DBDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_DVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_DZDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EOHOST ID_LDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_BRDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_CTDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_EDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_EVHOST ID_ELDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FCHOST ID_BVDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FCHOST ID_CDDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FCHOST ID_CLDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FCHOST ID_DFDOMAINCREDENTIAL)
	(MEM_CACHED_DOMAIN_CREDS ID_FCHOST ID_HDOMAINCREDENTIAL)
	(MEM_DOMAIN_USER_ADMINS ID_EOHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_EVHOST ID_DUDOMAINUSER)
	(MEM_DOMAIN_USER_ADMINS ID_FCHOST ID_CKDOMAINUSER)
	(MEM_HOSTS ID_ADOMAIN ID_EOHOST)
	(MEM_HOSTS ID_ADOMAIN ID_EVHOST)
	(MEM_HOSTS ID_ADOMAIN ID_FCHOST)
	(PROP_CRED ID_BADOMAINUSER ID_BBDOMAINCREDENTIAL)
	(PROP_CRED ID_BEDOMAINUSER ID_BFDOMAINCREDENTIAL)
	(PROP_CRED ID_BIDOMAINUSER ID_BJDOMAINCREDENTIAL)
	(PROP_CRED ID_BMDOMAINUSER ID_BNDOMAINCREDENTIAL)
	(PROP_CRED ID_BQDOMAINUSER ID_BRDOMAINCREDENTIAL)
	(PROP_CRED ID_BUDOMAINUSER ID_BVDOMAINCREDENTIAL)
	(PROP_CRED ID_BYDOMAINUSER ID_BZDOMAINCREDENTIAL)
	(PROP_CRED ID_CDOMAINUSER ID_DDOMAINCREDENTIAL)
	(PROP_CRED ID_CCDOMAINUSER ID_CDDOMAINCREDENTIAL)
	(PROP_CRED ID_CGDOMAINUSER ID_CHDOMAINCREDENTIAL)
	(PROP_CRED ID_CKDOMAINUSER ID_CLDOMAINCREDENTIAL)
	(PROP_CRED ID_CODOMAINUSER ID_CPDOMAINCREDENTIAL)
	(PROP_CRED ID_CSDOMAINUSER ID_CTDOMAINCREDENTIAL)
	(PROP_CRED ID_CWDOMAINUSER ID_CXDOMAINCREDENTIAL)
	(PROP_CRED ID_DADOMAINUSER ID_DBDOMAINCREDENTIAL)
	(PROP_CRED ID_DEDOMAINUSER ID_DFDOMAINCREDENTIAL)
	(PROP_CRED ID_DIDOMAINUSER ID_DJDOMAINCREDENTIAL)
	(PROP_CRED ID_DMDOMAINUSER ID_DNDOMAINCREDENTIAL)
	(PROP_CRED ID_DQDOMAINUSER ID_DRDOMAINCREDENTIAL)
	(PROP_CRED ID_DUDOMAINUSER ID_DVDOMAINCREDENTIAL)
	(PROP_CRED ID_DYDOMAINUSER ID_DZDOMAINCREDENTIAL)
	(PROP_CRED ID_ECDOMAINUSER ID_EDDOMAINCREDENTIAL)
	(PROP_CRED ID_EGDOMAINUSER ID_EHDOMAINCREDENTIAL)
	(PROP_CRED ID_EKDOMAINUSER ID_ELDOMAINCREDENTIAL)
	(PROP_CRED ID_GDOMAINUSER ID_HDOMAINCREDENTIAL)
	(PROP_CRED ID_KDOMAINUSER ID_LDOMAINCREDENTIAL)
	(PROP_CRED ID_ODOMAINUSER ID_PDOMAINCREDENTIAL)
	(PROP_CRED ID_SDOMAINUSER ID_TDOMAINCREDENTIAL)
	(PROP_CRED ID_WDOMAINUSER ID_XDOMAINCREDENTIAL)
	(PROP_DC ID_EOHOST NO)
	(PROP_DC ID_EVHOST YES)
	(PROP_DC ID_FCHOST NO)
	(PROP_DNS_DOMAIN ID_ADOMAIN STR__B)
	(PROP_DNS_DOMAIN_NAME ID_EOHOST STR__EU)
	(PROP_DNS_DOMAIN_NAME ID_EVHOST STR__FB)
	(PROP_DNS_DOMAIN_NAME ID_FCHOST STR__FI)
	(PROP_DOMAIN ID_BADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_BYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_BZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CCDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CLDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CPDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CSDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CTDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_CWDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_CXDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DADOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DBDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DEDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DFDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DIDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DJDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DMDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DNDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DQDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DRDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DUDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DVDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_DYDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_DZDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ECDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EDDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EGDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_EHDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EKDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_ELDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_EOHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_EVHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_FCHOST ID_ADOMAIN)
	(PROP_DOMAIN ID_GDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_HDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_KDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_LDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_ODOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_PDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_SDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_TDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_DOMAIN ID_WDOMAINUSER ID_ADOMAIN)
	(PROP_DOMAIN ID_XDOMAINCREDENTIAL ID_ADOMAIN)
	(PROP_ELEVATED ID_FJRAT YES)
	(PROP_EXECUTABLE ID_FJRAT STR__FK)
	(PROP_FQDN ID_EOHOST STR__ES)
	(PROP_FQDN ID_EVHOST STR__EZ)
	(PROP_FQDN ID_FCHOST STR__FG)
	(PROP_HOST ID_EPTIMEDELTA ID_EOHOST)
	(PROP_HOST ID_EWTIMEDELTA ID_EVHOST)
	(PROP_HOST ID_FDTIMEDELTA ID_FCHOST)
	(PROP_HOST ID_FJRAT ID_EOHOST)
	(PROP_HOSTNAME ID_EOHOST STR__ET)
	(PROP_HOSTNAME ID_EVHOST STR__FA)
	(PROP_HOSTNAME ID_FCHOST STR__FH)
	(PROP_IS_GROUP ID_BADOMAINUSER NO)
	(PROP_IS_GROUP ID_BEDOMAINUSER NO)
	(PROP_IS_GROUP ID_BIDOMAINUSER NO)
	(PROP_IS_GROUP ID_BMDOMAINUSER NO)
	(PROP_IS_GROUP ID_BQDOMAINUSER NO)
	(PROP_IS_GROUP ID_BUDOMAINUSER NO)
	(PROP_IS_GROUP ID_BYDOMAINUSER NO)
	(PROP_IS_GROUP ID_CDOMAINUSER NO)
	(PROP_IS_GROUP ID_CCDOMAINUSER NO)
	(PROP_IS_GROUP ID_CGDOMAINUSER NO)
	(PROP_IS_GROUP ID_CKDOMAINUSER NO)
	(PROP_IS_GROUP ID_CODOMAINUSER NO)
	(PROP_IS_GROUP ID_CSDOMAINUSER NO)
	(PROP_IS_GROUP ID_CWDOMAINUSER NO)
	(PROP_IS_GROUP ID_DADOMAINUSER NO)
	(PROP_IS_GROUP ID_DEDOMAINUSER NO)
	(PROP_IS_GROUP ID_DIDOMAINUSER NO)
	(PROP_IS_GROUP ID_DMDOMAINUSER NO)
	(PROP_IS_GROUP ID_DQDOMAINUSER NO)
	(PROP_IS_GROUP ID_DUDOMAINUSER NO)
	(PROP_IS_GROUP ID_DYDOMAINUSER NO)
	(PROP_IS_GROUP ID_ECDOMAINUSER NO)
	(PROP_IS_GROUP ID_EGDOMAINUSER NO)
	(PROP_IS_GROUP ID_EKDOMAINUSER NO)
	(PROP_IS_GROUP ID_GDOMAINUSER NO)
	(PROP_IS_GROUP ID_KDOMAINUSER NO)
	(PROP_IS_GROUP ID_ODOMAINUSER NO)
	(PROP_IS_GROUP ID_SDOMAINUSER NO)
	(PROP_IS_GROUP ID_WDOMAINUSER NO)
	(PROP_MICROSECONDS ID_EPTIMEDELTA NUM__121)
	(PROP_MICROSECONDS ID_EWTIMEDELTA NUM__128)
	(PROP_MICROSECONDS ID_FDTIMEDELTA NUM__135)
	(PROP_PASSWORD ID_BBDOMAINCREDENTIAL STR__BC)
	(PROP_PASSWORD ID_BFDOMAINCREDENTIAL STR__BG)
	(PROP_PASSWORD ID_BJDOMAINCREDENTIAL STR__BK)
	(PROP_PASSWORD ID_BNDOMAINCREDENTIAL STR__BO)
	(PROP_PASSWORD ID_BRDOMAINCREDENTIAL STR__BS)
	(PROP_PASSWORD ID_BVDOMAINCREDENTIAL STR__BW)
	(PROP_PASSWORD ID_BZDOMAINCREDENTIAL STR__CA)
	(PROP_PASSWORD ID_CDDOMAINCREDENTIAL STR__CE)
	(PROP_PASSWORD ID_CHDOMAINCREDENTIAL STR__CI)
	(PROP_PASSWORD ID_CLDOMAINCREDENTIAL STR__CM)
	(PROP_PASSWORD ID_CPDOMAINCREDENTIAL STR__CQ)
	(PROP_PASSWORD ID_CTDOMAINCREDENTIAL STR__CU)
	(PROP_PASSWORD ID_CXDOMAINCREDENTIAL STR__CY)
	(PROP_PASSWORD ID_DDOMAINCREDENTIAL STR__E)
	(PROP_PASSWORD ID_DBDOMAINCREDENTIAL STR__DC)
	(PROP_PASSWORD ID_DFDOMAINCREDENTIAL STR__DG)
	(PROP_PASSWORD ID_DJDOMAINCREDENTIAL STR__DK)
	(PROP_PASSWORD ID_DNDOMAINCREDENTIAL STR__DO)
	(PROP_PASSWORD ID_DRDOMAINCREDENTIAL STR__DS)
	(PROP_PASSWORD ID_DVDOMAINCREDENTIAL STR__DW)
	(PROP_PASSWORD ID_DZDOMAINCREDENTIAL STR__EA)
	(PROP_PASSWORD ID_EDDOMAINCREDENTIAL STR__EE)
	(PROP_PASSWORD ID_EHDOMAINCREDENTIAL STR__EI)
	(PROP_PASSWORD ID_ELDOMAINCREDENTIAL STR__EM)
	(PROP_PASSWORD ID_HDOMAINCREDENTIAL STR__I)
	(PROP_PASSWORD ID_LDOMAINCREDENTIAL STR__M)
	(PROP_PASSWORD ID_PDOMAINCREDENTIAL STR__Q)
	(PROP_PASSWORD ID_TDOMAINCREDENTIAL STR__U)
	(PROP_PASSWORD ID_XDOMAINCREDENTIAL STR__Y)
	(PROP_SECONDS ID_EPTIMEDELTA NUM__120)
	(PROP_SECONDS ID_EWTIMEDELTA NUM__127)
	(PROP_SECONDS ID_FDTIMEDELTA NUM__134)
	(PROP_SID ID_BADOMAINUSER STR__BD)
	(PROP_SID ID_BEDOMAINUSER STR__BH)
	(PROP_SID ID_BIDOMAINUSER STR__BL)
	(PROP_SID ID_BMDOMAINUSER STR__BP)
	(PROP_SID ID_BQDOMAINUSER STR__BT)
	(PROP_SID ID_BUDOMAINUSER STR__BX)
	(PROP_SID ID_BYDOMAINUSER STR__CB)
	(PROP_SID ID_CDOMAINUSER STR__F)
	(PROP_SID ID_CCDOMAINUSER STR__CF)
	(PROP_SID ID_CGDOMAINUSER STR__CJ)
	(PROP_SID ID_CKDOMAINUSER STR__CN)
	(PROP_SID ID_CODOMAINUSER STR__CR)
	(PROP_SID ID_CSDOMAINUSER STR__CV)
	(PROP_SID ID_CWDOMAINUSER STR__CZ)
	(PROP_SID ID_DADOMAINUSER STR__DD)
	(PROP_SID ID_DEDOMAINUSER STR__DH)
	(PROP_SID ID_DIDOMAINUSER STR__DL)
	(PROP_SID ID_DMDOMAINUSER STR__DP)
	(PROP_SID ID_DQDOMAINUSER STR__DT)
	(PROP_SID ID_DUDOMAINUSER STR__DX)
	(PROP_SID ID_DYDOMAINUSER STR__EB)
	(PROP_SID ID_ECDOMAINUSER STR__EF)
	(PROP_SID ID_EGDOMAINUSER STR__EJ)
	(PROP_SID ID_EKDOMAINUSER STR__EN)
	(PROP_SID ID_GDOMAINUSER STR__J)
	(PROP_SID ID_KDOMAINUSER STR__N)
	(PROP_SID ID_ODOMAINUSER STR__R)
	(PROP_SID ID_SDOMAINUSER STR__V)
	(PROP_SID ID_WDOMAINUSER STR__Z)
	(PROP_TIMEDELTA ID_EOHOST ID_EPTIMEDELTA)
	(PROP_TIMEDELTA ID_EVHOST ID_EWTIMEDELTA)
	(PROP_TIMEDELTA ID_FCHOST ID_FDTIMEDELTA)
	(PROP_USER ID_BBDOMAINCREDENTIAL ID_BADOMAINUSER)
	(PROP_USER ID_BFDOMAINCREDENTIAL ID_BEDOMAINUSER)
	(PROP_USER ID_BJDOMAINCREDENTIAL ID_BIDOMAINUSER)
	(PROP_USER ID_BNDOMAINCREDENTIAL ID_BMDOMAINUSER)
	(PROP_USER ID_BRDOMAINCREDENTIAL ID_BQDOMAINUSER)
	(PROP_USER ID_BVDOMAINCREDENTIAL ID_BUDOMAINUSER)
	(PROP_USER ID_BZDOMAINCREDENTIAL ID_BYDOMAINUSER)
	(PROP_USER ID_CDDOMAINCREDENTIAL ID_CCDOMAINUSER)
	(PROP_USER ID_CHDOMAINCREDENTIAL ID_CGDOMAINUSER)
	(PROP_USER ID_CLDOMAINCREDENTIAL ID_CKDOMAINUSER)
	(PROP_USER ID_CPDOMAINCREDENTIAL ID_CODOMAINUSER)
	(PROP_USER ID_CTDOMAINCREDENTIAL ID_CSDOMAINUSER)
	(PROP_USER ID_CXDOMAINCREDENTIAL ID_CWDOMAINUSER)
	(PROP_USER ID_DDOMAINCREDENTIAL ID_CDOMAINUSER)
	(PROP_USER ID_DBDOMAINCREDENTIAL ID_DADOMAINUSER)
	(PROP_USER ID_DFDOMAINCREDENTIAL ID_DEDOMAINUSER)
	(PROP_USER ID_DJDOMAINCREDENTIAL ID_DIDOMAINUSER)
	(PROP_USER ID_DNDOMAINCREDENTIAL ID_DMDOMAINUSER)
	(PROP_USER ID_DRDOMAINCREDENTIAL ID_DQDOMAINUSER)
	(PROP_USER ID_DVDOMAINCREDENTIAL ID_DUDOMAINUSER)
	(PROP_USER ID_DZDOMAINCREDENTIAL ID_DYDOMAINUSER)
	(PROP_USER ID_EDDOMAINCREDENTIAL ID_ECDOMAINUSER)
	(PROP_USER ID_EHDOMAINCREDENTIAL ID_EGDOMAINUSER)
	(PROP_USER ID_ELDOMAINCREDENTIAL ID_EKDOMAINUSER)
	(PROP_USER ID_HDOMAINCREDENTIAL ID_GDOMAINUSER)
	(PROP_USER ID_LDOMAINCREDENTIAL ID_KDOMAINUSER)
	(PROP_USER ID_PDOMAINCREDENTIAL ID_ODOMAINUSER)
	(PROP_USER ID_TDOMAINCREDENTIAL ID_SDOMAINUSER)
	(PROP_USER ID_XDOMAINCREDENTIAL ID_WDOMAINUSER)
	(PROP_USERNAME ID_BADOMAINUSER STR__MICHAEL)
	(PROP_USERNAME ID_BEDOMAINUSER STR__BARBARA)
	(PROP_USERNAME ID_BIDOMAINUSER STR__WILLIAM)
	(PROP_USERNAME ID_BMDOMAINUSER STR__ELIZABETH)
	(PROP_USERNAME ID_BQDOMAINUSER STR__DAVID)
	(PROP_USERNAME ID_BUDOMAINUSER STR__JENNIFER)
	(PROP_USERNAME ID_BYDOMAINUSER STR__RICHARD)
	(PROP_USERNAME ID_CDOMAINUSER STR__JAMES)
	(PROP_USERNAME ID_CCDOMAINUSER STR__MARIA)
	(PROP_USERNAME ID_CGDOMAINUSER STR__CHARLES)
	(PROP_USERNAME ID_CKDOMAINUSER STR__SUSAN)
	(PROP_USERNAME ID_CODOMAINUSER STR__JOSEPH)
	(PROP_USERNAME ID_CSDOMAINUSER STR__MARGARET)
	(PROP_USERNAME ID_CWDOMAINUSER STR__THOMAS)
	(PROP_USERNAME ID_DADOMAINUSER STR__DOROTHY)
	(PROP_USERNAME ID_DEDOMAINUSER STR__CHRISTOPHER)
	(PROP_USERNAME ID_DIDOMAINUSER STR__LISA)
	(PROP_USERNAME ID_DMDOMAINUSER STR__DANIEL)
	(PROP_USERNAME ID_DQDOMAINUSER STR__NANCY)
	(PROP_USERNAME ID_DUDOMAINUSER STR__PAUL)
	(PROP_USERNAME ID_DYDOMAINUSER STR__KAREN)
	(PROP_USERNAME ID_ECDOMAINUSER STR__MARK)
	(PROP_USERNAME ID_EGDOMAINUSER STR__BETTY)
	(PROP_USERNAME ID_EKDOMAINUSER STR__DONALD)
	(PROP_USERNAME ID_GDOMAINUSER STR__MARY)
	(PROP_USERNAME ID_KDOMAINUSER STR__JOHN)
	(PROP_USERNAME ID_ODOMAINUSER STR__PATRICIA)
	(PROP_USERNAME ID_SDOMAINUSER STR__ROBERT)
	(PROP_USERNAME ID_WDOMAINUSER STR__LINDA)
	(PROP_WINDOWS_DOMAIN ID_ADOMAIN STR__ALPHA)
	(procnone)
(= (total-cost) 0)
)
(:goal (and (procnone) (PROP_HOST ID_FORAT ID_EVHOST)(PROP_HOST ID_FPRAT ID_FCHOST)))
(:metric minimize (total-cost))

)
