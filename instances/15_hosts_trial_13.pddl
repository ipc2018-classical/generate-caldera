;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 15 hosts, 30 user, 2 admin per host, 3 active account per host
(define (problem p15_hosts_trial_13)
(:domain caldera)
(:objects id_dyDomainUser id_egDomainUser id_kDomainUser id_buDomainUser id_ecDomainUser id_eoDomainUser id_bqDomainUser id_gDomainUser id_wDomainUser id_dmDomainUser id_csDomainUser id_sDomainUser id_duDomainUser id_daDomainUser id_cgDomainUser id_byDomainUser id_baDomainUser id_oDomainUser id_beDomainUser id_diDomainUser id_coDomainUser id_ekDomainUser id_cDomainUser id_bmDomainUser id_dqDomainUser id_biDomainUser id_ccDomainUser id_deDomainUser id_cwDomainUser id_ckDomainUser - ObservedDomainUser 
 id_aDomain - ObservedDomain 
 id_hlTimeDelta id_hsTimeDelta id_fhTimeDelta id_gcTimeDelta id_gqTimeDelta id_hzTimeDelta id_fvTimeDelta id_foTimeDelta id_igTimeDelta id_etTimeDelta id_gxTimeDelta id_inTimeDelta id_heTimeDelta id_gjTimeDelta id_faTimeDelta - ObservedTimeDelta 
 id_kbShare id_kdShare id_khShare id_kaShare id_kkShare id_kfShare id_jzShare id_kmShare id_kcShare id_kiShare id_klShare id_kgShare id_knShare id_keShare id_kjShare - ObservedShare 
 id_izFile id_jiFile id_jhFile id_jgFile id_jbFile id_ixFile id_jaFile id_jeFile id_iwFile id_jcFile id_jfFile id_jjFile id_iyFile id_ivFile id_jdFile - ObservedFile 
 id_clDomainCredential id_dzDomainCredential id_dDomainCredential id_elDomainCredential id_bvDomainCredential id_dvDomainCredential id_cdDomainCredential id_bbDomainCredential id_dfDomainCredential id_edDomainCredential id_lDomainCredential id_bnDomainCredential id_cpDomainCredential id_djDomainCredential id_hDomainCredential id_ehDomainCredential id_dnDomainCredential id_cxDomainCredential id_pDomainCredential id_epDomainCredential id_dbDomainCredential id_bjDomainCredential id_ctDomainCredential id_bzDomainCredential id_drDomainCredential id_chDomainCredential id_bfDomainCredential id_xDomainCredential id_brDomainCredential id_tDomainCredential - ObservedDomainCredential 
 id_kqSchtask id_ktSchtask id_kzSchtask id_kuSchtask id_kxSchtask id_ksSchtask id_koSchtask id_kvSchtask id_kpSchtask id_kwSchtask id_lbSchtask id_lcSchtask id_kySchtask id_laSchtask id_krSchtask - ObservedSchtask 
 id_fnHost id_gbHost id_hkHost id_fuHost id_gpHost id_esHost id_fgHost id_hyHost id_gwHost id_hdHost id_imHost id_giHost id_ezHost id_hrHost id_ifHost - ObservedHost 
 num__138 num__125 num__201 num__145 num__131 num__173 num__209 num__132 num__166 num__160 num__174 num__216 num__139 num__167 num__152 num__153 num__188 num__222 num__146 num__180 num__194 num__159 num__181 num__208 num__215 num__195 num__124 num__187 num__202 num__223 - num 
 id_jlRat id_jqRat id_jrRat id_jyRat id_itRat id_joRat id_jmRat id_jpRat id_jsRat id_jtRat id_juRat id_jwRat id_jvRat id_jkRat id_jxRat id_jnRat - ObservedRat 
 str__bo str__christopher str__j str__ho str__ds str__hv str__em str__ca str__fe str__r str__ex str__donald str__gh str__mary str__fl str__gm str__cu str__fs str__linda str__john str__dd str__e str__v str__dt str__dg str__betty str__eb str__cy str__thomas str__elizabeth str__cj str__gn str__iq str__dk str__jennifer str__paul str__cq str__er str__ft str__q str__dl str__id str__gf str__ee str__fr str__bc str__bh str__ff str__hq str__bg str__margaret str__mark str__richard str__james str__f str__ic str__gv str__hj str__bw str__ea str__cv str__ce str__alpha str__cz str__cr str__ga str__ir str__ef str__ej str__bx str__bk str__ik str__iu str__david str__hh str__dc str__ij str__gu str__helen str__bs str__is str__ha str__hx str__il str__daniel str__lisa str__dw str__joseph str__cb str__y str__ey str__eq str__gt str__fz str__gg str__nancy str__hc str__cf str__fm str__bd str__bt str__en str__n str__dorothy str__bl str__fd str__ew str__michael str__robert str__b str__maria str__susan str__hi str__bp str__cm str__barbara str__hp str__william str__dx str__cn str__m str__u str__ie str__ci str__i str__do str__fk str__karen str__go str__z str__dh str__fy str__hw str__ei str__patricia str__charles str__hb str__dp - string 
)
(:init     (knows id_fuHost)
     (knows id_itRat)
     (knows_property id_fuHost pfqdn)
     (knows_property id_itRat pexecutable)
     (knows_property id_itRat phost)
     (mem_cached_domain_creds id_esHost id_cpDomainCredential)
     (mem_cached_domain_creds id_esHost id_djDomainCredential)
     (mem_cached_domain_creds id_esHost id_xDomainCredential)
     (mem_cached_domain_creds id_ezHost id_bfDomainCredential)
     (mem_cached_domain_creds id_ezHost id_dDomainCredential)
     (mem_cached_domain_creds id_ezHost id_ehDomainCredential)
     (mem_cached_domain_creds id_fgHost id_bbDomainCredential)
     (mem_cached_domain_creds id_fgHost id_brDomainCredential)
     (mem_cached_domain_creds id_fgHost id_lDomainCredential)
     (mem_cached_domain_creds id_fnHost id_bfDomainCredential)
     (mem_cached_domain_creds id_fnHost id_clDomainCredential)
     (mem_cached_domain_creds id_fnHost id_djDomainCredential)
     (mem_cached_domain_creds id_fuHost id_dfDomainCredential)
     (mem_cached_domain_creds id_fuHost id_drDomainCredential)
     (mem_cached_domain_creds id_fuHost id_hDomainCredential)
     (mem_cached_domain_creds id_gbHost id_bjDomainCredential)
     (mem_cached_domain_creds id_gbHost id_elDomainCredential)
     (mem_cached_domain_creds id_gbHost id_hDomainCredential)
     (mem_cached_domain_creds id_giHost id_edDomainCredential)
     (mem_cached_domain_creds id_giHost id_elDomainCredential)
     (mem_cached_domain_creds id_giHost id_xDomainCredential)
     (mem_cached_domain_creds id_gpHost id_dbDomainCredential)
     (mem_cached_domain_creds id_gpHost id_djDomainCredential)
     (mem_cached_domain_creds id_gpHost id_drDomainCredential)
     (mem_cached_domain_creds id_gwHost id_bvDomainCredential)
     (mem_cached_domain_creds id_gwHost id_dDomainCredential)
     (mem_cached_domain_creds id_gwHost id_dvDomainCredential)
     (mem_cached_domain_creds id_hdHost id_bnDomainCredential)
     (mem_cached_domain_creds id_hdHost id_brDomainCredential)
     (mem_cached_domain_creds id_hdHost id_pDomainCredential)
     (mem_cached_domain_creds id_hkHost id_bfDomainCredential)
     (mem_cached_domain_creds id_hkHost id_bzDomainCredential)
     (mem_cached_domain_creds id_hkHost id_tDomainCredential)
     (mem_cached_domain_creds id_hrHost id_bvDomainCredential)
     (mem_cached_domain_creds id_hrHost id_cpDomainCredential)
     (mem_cached_domain_creds id_hrHost id_tDomainCredential)
     (mem_cached_domain_creds id_hyHost id_cdDomainCredential)
     (mem_cached_domain_creds id_hyHost id_dzDomainCredential)
     (mem_cached_domain_creds id_hyHost id_ehDomainCredential)
     (mem_cached_domain_creds id_ifHost id_bfDomainCredential)
     (mem_cached_domain_creds id_ifHost id_edDomainCredential)
     (mem_cached_domain_creds id_ifHost id_lDomainCredential)
     (mem_cached_domain_creds id_imHost id_dvDomainCredential)
     (mem_cached_domain_creds id_imHost id_ehDomainCredential)
     (mem_cached_domain_creds id_imHost id_pDomainCredential)
     (mem_domain_user_admins id_esHost id_cwDomainUser)
     (mem_domain_user_admins id_esHost id_gDomainUser)
     (mem_domain_user_admins id_ezHost id_biDomainUser)
     (mem_domain_user_admins id_ezHost id_dmDomainUser)
     (mem_domain_user_admins id_fgHost id_dqDomainUser)
     (mem_domain_user_admins id_fgHost id_kDomainUser)
     (mem_domain_user_admins id_fnHost id_biDomainUser)
     (mem_domain_user_admins id_fnHost id_cDomainUser)
     (mem_domain_user_admins id_fuHost id_ccDomainUser)
     (mem_domain_user_admins id_fuHost id_csDomainUser)
     (mem_domain_user_admins id_gbHost id_daDomainUser)
     (mem_domain_user_admins id_gbHost id_gDomainUser)
     (mem_domain_user_admins id_giHost id_deDomainUser)
     (mem_domain_user_admins id_giHost id_eoDomainUser)
     (mem_domain_user_admins id_gpHost id_bqDomainUser)
     (mem_domain_user_admins id_gpHost id_ecDomainUser)
     (mem_domain_user_admins id_gwHost id_cDomainUser)
     (mem_domain_user_admins id_gwHost id_duDomainUser)
     (mem_domain_user_admins id_hdHost id_biDomainUser)
     (mem_domain_user_admins id_hdHost id_byDomainUser)
     (mem_domain_user_admins id_hkHost id_cDomainUser)
     (mem_domain_user_admins id_hkHost id_coDomainUser)
     (mem_domain_user_admins id_hrHost id_duDomainUser)
     (mem_domain_user_admins id_hrHost id_eoDomainUser)
     (mem_domain_user_admins id_hyHost id_cwDomainUser)
     (mem_domain_user_admins id_hyHost id_daDomainUser)
     (mem_domain_user_admins id_ifHost id_cgDomainUser)
     (mem_domain_user_admins id_ifHost id_ckDomainUser)
     (mem_domain_user_admins id_imHost id_dyDomainUser)
     (mem_domain_user_admins id_imHost id_eoDomainUser)
     (mem_hosts id_aDomain id_esHost)
     (mem_hosts id_aDomain id_ezHost)
     (mem_hosts id_aDomain id_fgHost)
     (mem_hosts id_aDomain id_fnHost)
     (mem_hosts id_aDomain id_fuHost)
     (mem_hosts id_aDomain id_gbHost)
     (mem_hosts id_aDomain id_giHost)
     (mem_hosts id_aDomain id_gpHost)
     (mem_hosts id_aDomain id_gwHost)
     (mem_hosts id_aDomain id_hdHost)
     (mem_hosts id_aDomain id_hkHost)
     (mem_hosts id_aDomain id_hrHost)
     (mem_hosts id_aDomain id_hyHost)
     (mem_hosts id_aDomain id_ifHost)
     (mem_hosts id_aDomain id_imHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_byDomainUser id_bzDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_ccDomainUser id_cdDomainCredential)
     (prop_cred id_cgDomainUser id_chDomainCredential)
     (prop_cred id_ckDomainUser id_clDomainCredential)
     (prop_cred id_coDomainUser id_cpDomainCredential)
     (prop_cred id_csDomainUser id_ctDomainCredential)
     (prop_cred id_cwDomainUser id_cxDomainCredential)
     (prop_cred id_daDomainUser id_dbDomainCredential)
     (prop_cred id_deDomainUser id_dfDomainCredential)
     (prop_cred id_diDomainUser id_djDomainCredential)
     (prop_cred id_dmDomainUser id_dnDomainCredential)
     (prop_cred id_dqDomainUser id_drDomainCredential)
     (prop_cred id_duDomainUser id_dvDomainCredential)
     (prop_cred id_dyDomainUser id_dzDomainCredential)
     (prop_cred id_ecDomainUser id_edDomainCredential)
     (prop_cred id_egDomainUser id_ehDomainCredential)
     (prop_cred id_ekDomainUser id_elDomainCredential)
     (prop_cred id_eoDomainUser id_epDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_esHost yes)
     (prop_dc id_ezHost yes)
     (prop_dc id_fgHost no)
     (prop_dc id_fnHost no)
     (prop_dc id_fuHost no)
     (prop_dc id_gbHost no)
     (prop_dc id_giHost no)
     (prop_dc id_gpHost yes)
     (prop_dc id_gwHost no)
     (prop_dc id_hdHost no)
     (prop_dc id_hkHost yes)
     (prop_dc id_hrHost no)
     (prop_dc id_hyHost no)
     (prop_dc id_ifHost no)
     (prop_dc id_imHost no)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_esHost str__ey)
     (prop_dns_domain_name id_ezHost str__ff)
     (prop_dns_domain_name id_fgHost str__fm)
     (prop_dns_domain_name id_fnHost str__ft)
     (prop_dns_domain_name id_fuHost str__ga)
     (prop_dns_domain_name id_gbHost str__gh)
     (prop_dns_domain_name id_giHost str__go)
     (prop_dns_domain_name id_gpHost str__gv)
     (prop_dns_domain_name id_gwHost str__hc)
     (prop_dns_domain_name id_hdHost str__hj)
     (prop_dns_domain_name id_hkHost str__hq)
     (prop_dns_domain_name id_hrHost str__hx)
     (prop_dns_domain_name id_hyHost str__ie)
     (prop_dns_domain_name id_ifHost str__il)
     (prop_dns_domain_name id_imHost str__is)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byDomainUser id_aDomain)
     (prop_domain id_bzDomainCredential id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_ccDomainUser id_aDomain)
     (prop_domain id_cdDomainCredential id_aDomain)
     (prop_domain id_cgDomainUser id_aDomain)
     (prop_domain id_chDomainCredential id_aDomain)
     (prop_domain id_ckDomainUser id_aDomain)
     (prop_domain id_clDomainCredential id_aDomain)
     (prop_domain id_coDomainUser id_aDomain)
     (prop_domain id_cpDomainCredential id_aDomain)
     (prop_domain id_csDomainUser id_aDomain)
     (prop_domain id_ctDomainCredential id_aDomain)
     (prop_domain id_cwDomainUser id_aDomain)
     (prop_domain id_cxDomainCredential id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_daDomainUser id_aDomain)
     (prop_domain id_dbDomainCredential id_aDomain)
     (prop_domain id_deDomainUser id_aDomain)
     (prop_domain id_dfDomainCredential id_aDomain)
     (prop_domain id_diDomainUser id_aDomain)
     (prop_domain id_djDomainCredential id_aDomain)
     (prop_domain id_dmDomainUser id_aDomain)
     (prop_domain id_dnDomainCredential id_aDomain)
     (prop_domain id_dqDomainUser id_aDomain)
     (prop_domain id_drDomainCredential id_aDomain)
     (prop_domain id_duDomainUser id_aDomain)
     (prop_domain id_dvDomainCredential id_aDomain)
     (prop_domain id_dyDomainUser id_aDomain)
     (prop_domain id_dzDomainCredential id_aDomain)
     (prop_domain id_ecDomainUser id_aDomain)
     (prop_domain id_edDomainCredential id_aDomain)
     (prop_domain id_egDomainUser id_aDomain)
     (prop_domain id_ehDomainCredential id_aDomain)
     (prop_domain id_ekDomainUser id_aDomain)
     (prop_domain id_elDomainCredential id_aDomain)
     (prop_domain id_eoDomainUser id_aDomain)
     (prop_domain id_epDomainCredential id_aDomain)
     (prop_domain id_esHost id_aDomain)
     (prop_domain id_ezHost id_aDomain)
     (prop_domain id_fgHost id_aDomain)
     (prop_domain id_fnHost id_aDomain)
     (prop_domain id_fuHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_gbHost id_aDomain)
     (prop_domain id_giHost id_aDomain)
     (prop_domain id_gpHost id_aDomain)
     (prop_domain id_gwHost id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_hdHost id_aDomain)
     (prop_domain id_hkHost id_aDomain)
     (prop_domain id_hrHost id_aDomain)
     (prop_domain id_hyHost id_aDomain)
     (prop_domain id_ifHost id_aDomain)
     (prop_domain id_imHost id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_itRat yes)
     (prop_executable id_itRat str__iu)
     (prop_fqdn id_esHost str__ex)
     (prop_fqdn id_ezHost str__fe)
     (prop_fqdn id_fgHost str__fl)
     (prop_fqdn id_fnHost str__fs)
     (prop_fqdn id_fuHost str__fz)
     (prop_fqdn id_gbHost str__gg)
     (prop_fqdn id_giHost str__gn)
     (prop_fqdn id_gpHost str__gu)
     (prop_fqdn id_gwHost str__hb)
     (prop_fqdn id_hdHost str__hi)
     (prop_fqdn id_hkHost str__hp)
     (prop_fqdn id_hrHost str__hw)
     (prop_fqdn id_hyHost str__id)
     (prop_fqdn id_ifHost str__ik)
     (prop_fqdn id_imHost str__ir)
     (prop_host id_etTimeDelta id_esHost)
     (prop_host id_faTimeDelta id_ezHost)
     (prop_host id_fhTimeDelta id_fgHost)
     (prop_host id_foTimeDelta id_fnHost)
     (prop_host id_fvTimeDelta id_fuHost)
     (prop_host id_gcTimeDelta id_gbHost)
     (prop_host id_gjTimeDelta id_giHost)
     (prop_host id_gqTimeDelta id_gpHost)
     (prop_host id_gxTimeDelta id_gwHost)
     (prop_host id_heTimeDelta id_hdHost)
     (prop_host id_hlTimeDelta id_hkHost)
     (prop_host id_hsTimeDelta id_hrHost)
     (prop_host id_hzTimeDelta id_hyHost)
     (prop_host id_igTimeDelta id_ifHost)
     (prop_host id_inTimeDelta id_imHost)
     (prop_host id_itRat id_fuHost)
     (prop_hostname id_esHost str__ew)
     (prop_hostname id_ezHost str__fd)
     (prop_hostname id_fgHost str__fk)
     (prop_hostname id_fnHost str__fr)
     (prop_hostname id_fuHost str__fy)
     (prop_hostname id_gbHost str__gf)
     (prop_hostname id_giHost str__gm)
     (prop_hostname id_gpHost str__gt)
     (prop_hostname id_gwHost str__ha)
     (prop_hostname id_hdHost str__hh)
     (prop_hostname id_hkHost str__ho)
     (prop_hostname id_hrHost str__hv)
     (prop_hostname id_hyHost str__ic)
     (prop_hostname id_ifHost str__ij)
     (prop_hostname id_imHost str__iq)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_byDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_ccDomainUser no)
     (prop_is_group id_cgDomainUser no)
     (prop_is_group id_ckDomainUser no)
     (prop_is_group id_coDomainUser no)
     (prop_is_group id_csDomainUser no)
     (prop_is_group id_cwDomainUser no)
     (prop_is_group id_daDomainUser no)
     (prop_is_group id_deDomainUser no)
     (prop_is_group id_diDomainUser no)
     (prop_is_group id_dmDomainUser no)
     (prop_is_group id_dqDomainUser no)
     (prop_is_group id_duDomainUser no)
     (prop_is_group id_dyDomainUser no)
     (prop_is_group id_ecDomainUser no)
     (prop_is_group id_egDomainUser no)
     (prop_is_group id_ekDomainUser no)
     (prop_is_group id_eoDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_etTimeDelta num__124)
     (prop_microseconds id_faTimeDelta num__131)
     (prop_microseconds id_fhTimeDelta num__138)
     (prop_microseconds id_foTimeDelta num__145)
     (prop_microseconds id_fvTimeDelta num__152)
     (prop_microseconds id_gcTimeDelta num__159)
     (prop_microseconds id_gjTimeDelta num__166)
     (prop_microseconds id_gqTimeDelta num__173)
     (prop_microseconds id_gxTimeDelta num__180)
     (prop_microseconds id_heTimeDelta num__187)
     (prop_microseconds id_hlTimeDelta num__194)
     (prop_microseconds id_hsTimeDelta num__201)
     (prop_microseconds id_hzTimeDelta num__208)
     (prop_microseconds id_igTimeDelta num__215)
     (prop_microseconds id_inTimeDelta num__222)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_bzDomainCredential str__ca)
     (prop_password id_cdDomainCredential str__ce)
     (prop_password id_chDomainCredential str__ci)
     (prop_password id_clDomainCredential str__cm)
     (prop_password id_cpDomainCredential str__cq)
     (prop_password id_ctDomainCredential str__cu)
     (prop_password id_cxDomainCredential str__cy)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_dbDomainCredential str__dc)
     (prop_password id_dfDomainCredential str__dg)
     (prop_password id_djDomainCredential str__dk)
     (prop_password id_dnDomainCredential str__do)
     (prop_password id_drDomainCredential str__ds)
     (prop_password id_dvDomainCredential str__dw)
     (prop_password id_dzDomainCredential str__ea)
     (prop_password id_edDomainCredential str__ee)
     (prop_password id_ehDomainCredential str__ei)
     (prop_password id_elDomainCredential str__em)
     (prop_password id_epDomainCredential str__eq)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_etTimeDelta num__125)
     (prop_seconds id_faTimeDelta num__132)
     (prop_seconds id_fhTimeDelta num__139)
     (prop_seconds id_foTimeDelta num__146)
     (prop_seconds id_fvTimeDelta num__153)
     (prop_seconds id_gcTimeDelta num__160)
     (prop_seconds id_gjTimeDelta num__167)
     (prop_seconds id_gqTimeDelta num__174)
     (prop_seconds id_gxTimeDelta num__181)
     (prop_seconds id_heTimeDelta num__188)
     (prop_seconds id_hlTimeDelta num__195)
     (prop_seconds id_hsTimeDelta num__202)
     (prop_seconds id_hzTimeDelta num__209)
     (prop_seconds id_igTimeDelta num__216)
     (prop_seconds id_inTimeDelta num__223)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_byDomainUser str__cb)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_ccDomainUser str__cf)
     (prop_sid id_cgDomainUser str__cj)
     (prop_sid id_ckDomainUser str__cn)
     (prop_sid id_coDomainUser str__cr)
     (prop_sid id_csDomainUser str__cv)
     (prop_sid id_cwDomainUser str__cz)
     (prop_sid id_daDomainUser str__dd)
     (prop_sid id_deDomainUser str__dh)
     (prop_sid id_diDomainUser str__dl)
     (prop_sid id_dmDomainUser str__dp)
     (prop_sid id_dqDomainUser str__dt)
     (prop_sid id_duDomainUser str__dx)
     (prop_sid id_dyDomainUser str__eb)
     (prop_sid id_ecDomainUser str__ef)
     (prop_sid id_egDomainUser str__ej)
     (prop_sid id_ekDomainUser str__en)
     (prop_sid id_eoDomainUser str__er)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_esHost id_etTimeDelta)
     (prop_timedelta id_ezHost id_faTimeDelta)
     (prop_timedelta id_fgHost id_fhTimeDelta)
     (prop_timedelta id_fnHost id_foTimeDelta)
     (prop_timedelta id_fuHost id_fvTimeDelta)
     (prop_timedelta id_gbHost id_gcTimeDelta)
     (prop_timedelta id_giHost id_gjTimeDelta)
     (prop_timedelta id_gpHost id_gqTimeDelta)
     (prop_timedelta id_gwHost id_gxTimeDelta)
     (prop_timedelta id_hdHost id_heTimeDelta)
     (prop_timedelta id_hkHost id_hlTimeDelta)
     (prop_timedelta id_hrHost id_hsTimeDelta)
     (prop_timedelta id_hyHost id_hzTimeDelta)
     (prop_timedelta id_ifHost id_igTimeDelta)
     (prop_timedelta id_imHost id_inTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_bzDomainCredential id_byDomainUser)
     (prop_user id_cdDomainCredential id_ccDomainUser)
     (prop_user id_chDomainCredential id_cgDomainUser)
     (prop_user id_clDomainCredential id_ckDomainUser)
     (prop_user id_cpDomainCredential id_coDomainUser)
     (prop_user id_ctDomainCredential id_csDomainUser)
     (prop_user id_cxDomainCredential id_cwDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_dbDomainCredential id_daDomainUser)
     (prop_user id_dfDomainCredential id_deDomainUser)
     (prop_user id_djDomainCredential id_diDomainUser)
     (prop_user id_dnDomainCredential id_dmDomainUser)
     (prop_user id_drDomainCredential id_dqDomainUser)
     (prop_user id_dvDomainCredential id_duDomainUser)
     (prop_user id_dzDomainCredential id_dyDomainUser)
     (prop_user id_edDomainCredential id_ecDomainUser)
     (prop_user id_ehDomainCredential id_egDomainUser)
     (prop_user id_elDomainCredential id_ekDomainUser)
     (prop_user id_epDomainCredential id_eoDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_byDomainUser str__richard)
     (prop_username id_cDomainUser str__james)
     (prop_username id_ccDomainUser str__maria)
     (prop_username id_cgDomainUser str__charles)
     (prop_username id_ckDomainUser str__susan)
     (prop_username id_coDomainUser str__joseph)
     (prop_username id_csDomainUser str__margaret)
     (prop_username id_cwDomainUser str__thomas)
     (prop_username id_daDomainUser str__dorothy)
     (prop_username id_deDomainUser str__christopher)
     (prop_username id_diDomainUser str__lisa)
     (prop_username id_dmDomainUser str__daniel)
     (prop_username id_dqDomainUser str__nancy)
     (prop_username id_duDomainUser str__paul)
     (prop_username id_dyDomainUser str__karen)
     (prop_username id_ecDomainUser str__mark)
     (prop_username id_egDomainUser str__betty)
     (prop_username id_ekDomainUser str__donald)
     (prop_username id_eoDomainUser str__helen)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_jvRat id_ifHost)
     (prop_host id_jqRat id_fnHost)
     (prop_host id_juRat id_gbHost)
     (prop_host id_jpRat id_hkHost)
     (prop_host id_jkRat id_gpHost)
     (prop_host id_jxRat id_esHost)
     (prop_host id_jnRat id_fgHost)
     (prop_host id_jlRat id_hyHost)
     (prop_host id_jrRat id_gwHost)
     (prop_host id_jyRat id_hdHost)
     (prop_host id_joRat id_imHost)
     (prop_host id_jmRat id_giHost)
     (prop_host id_jsRat id_ezHost)
     (prop_host id_jtRat id_hrHost)
)
)
)