;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 12 hosts, 12 user, 1 admin per host, 3 active account per host
(define (problem p12_hosts_trial_9)
(:domain caldera)
(:objects id_bjDomainCredential id_brDomainCredential id_lDomainCredential id_bbDomainCredential id_xDomainCredential id_pDomainCredential id_bfDomainCredential id_tDomainCredential id_bvDomainCredential id_hDomainCredential id_dDomainCredential id_bnDomainCredential - ObservedDomainCredential 
 id_fhSchtask id_fkSchtask id_frSchtask id_fpSchtask id_fqSchtask id_flSchtask id_fmSchtask id_foSchtask id_fjSchtask id_fgSchtask id_fiSchtask id_fnSchtask - ObservedSchtask 
 id_cnTimeDelta id_dpTimeDelta id_cuTimeDelta id_dwTimeDelta id_cgTimeDelta id_ekTimeDelta id_erTimeDelta id_bzTimeDelta id_dbTimeDelta id_edTimeDelta id_diTimeDelta id_eyTimeDelta - ObservedTimeDelta 
 id_fwRat id_fuRat id_ftRat id_feRat id_fzRat id_gcRat id_gdRat id_gbRat id_fsRat id_fvRat id_fyRat id_gaRat id_fxRat - ObservedRat 
 str__dl str__dt str__en str__ff str__r str__eh str__eu str__james str__patricia str__y str__bo str__f str__n str__bs str__bd str__cs str__cx str__df str__dm str__dz str__william str__elizabeth str__j str__david str__m str__jennifer str__cl str__ck str__cz str__ei str__fc str__b str__cc str__du str__ce str__dn str__ep str__ds str__barbara str__bt str__i str__bl str__ea str__fd str__v str__cj str__dg str__e str__bh str__ev str__mary str__bx str__michael str__cq str__bg str__eo str__u str__bw str__fb str__cy str__de str__eg str__alpha str__q str__linda str__bk str__robert str__z str__bp str__cr str__eb str__cd str__ew str__john str__bc - string 
 id_geFile id_ghFile id_gmFile id_gpFile id_gfFile id_gnFile id_giFile id_goFile id_gkFile id_gjFile id_ggFile id_glFile - ObservedFile 
 id_gDomainUser id_bmDomainUser id_kDomainUser id_wDomainUser id_beDomainUser id_sDomainUser id_baDomainUser id_biDomainUser id_cDomainUser id_bqDomainUser id_buDomainUser id_oDomainUser - ObservedDomainUser 
 num__130 num__102 num__60 num__87 num__53 num__94 num__74 num__80 num__123 num__122 num__95 num__66 num__108 num__67 num__81 num__59 num__52 num__88 num__73 num__101 num__115 num__109 num__116 num__129 - num 
 id_aDomain - ObservedDomain 
 id_dvHost id_exHost id_daHost id_cmHost id_cfHost id_byHost id_ecHost id_ctHost id_eqHost id_dhHost id_doHost id_ejHost - ObservedHost 
 id_gwShare id_gtShare id_haShare id_gsShare id_grShare id_gvShare id_gzShare id_gqShare id_gxShare id_hbShare id_guShare id_gyShare - ObservedShare 
)
(:init     (knows id_ctHost)
     (knows id_feRat)
     (knows_property id_ctHost pfqdn)
     (knows_property id_feRat pexecutable)
     (knows_property id_feRat phost)
     (mem_cached_domain_creds id_byHost id_bfDomainCredential)
     (mem_cached_domain_creds id_byHost id_hDomainCredential)
     (mem_cached_domain_creds id_byHost id_xDomainCredential)
     (mem_cached_domain_creds id_cfHost id_bfDomainCredential)
     (mem_cached_domain_creds id_cfHost id_bjDomainCredential)
     (mem_cached_domain_creds id_cfHost id_brDomainCredential)
     (mem_cached_domain_creds id_cmHost id_bvDomainCredential)
     (mem_cached_domain_creds id_cmHost id_dDomainCredential)
     (mem_cached_domain_creds id_cmHost id_lDomainCredential)
     (mem_cached_domain_creds id_ctHost id_brDomainCredential)
     (mem_cached_domain_creds id_ctHost id_bvDomainCredential)
     (mem_cached_domain_creds id_ctHost id_lDomainCredential)
     (mem_cached_domain_creds id_daHost id_bbDomainCredential)
     (mem_cached_domain_creds id_daHost id_brDomainCredential)
     (mem_cached_domain_creds id_daHost id_lDomainCredential)
     (mem_cached_domain_creds id_dhHost id_brDomainCredential)
     (mem_cached_domain_creds id_dhHost id_tDomainCredential)
     (mem_cached_domain_creds id_dhHost id_xDomainCredential)
     (mem_cached_domain_creds id_doHost id_brDomainCredential)
     (mem_cached_domain_creds id_doHost id_bvDomainCredential)
     (mem_cached_domain_creds id_doHost id_xDomainCredential)
     (mem_cached_domain_creds id_dvHost id_bjDomainCredential)
     (mem_cached_domain_creds id_dvHost id_pDomainCredential)
     (mem_cached_domain_creds id_dvHost id_tDomainCredential)
     (mem_cached_domain_creds id_ecHost id_bbDomainCredential)
     (mem_cached_domain_creds id_ecHost id_hDomainCredential)
     (mem_cached_domain_creds id_ecHost id_xDomainCredential)
     (mem_cached_domain_creds id_ejHost id_bbDomainCredential)
     (mem_cached_domain_creds id_ejHost id_brDomainCredential)
     (mem_cached_domain_creds id_ejHost id_pDomainCredential)
     (mem_cached_domain_creds id_eqHost id_bnDomainCredential)
     (mem_cached_domain_creds id_eqHost id_bvDomainCredential)
     (mem_cached_domain_creds id_eqHost id_dDomainCredential)
     (mem_cached_domain_creds id_exHost id_bbDomainCredential)
     (mem_cached_domain_creds id_exHost id_bvDomainCredential)
     (mem_cached_domain_creds id_exHost id_dDomainCredential)
     (mem_domain_user_admins id_byHost id_kDomainUser)
     (mem_domain_user_admins id_cfHost id_sDomainUser)
     (mem_domain_user_admins id_cmHost id_gDomainUser)
     (mem_domain_user_admins id_ctHost id_bmDomainUser)
     (mem_domain_user_admins id_daHost id_biDomainUser)
     (mem_domain_user_admins id_dhHost id_bmDomainUser)
     (mem_domain_user_admins id_doHost id_sDomainUser)
     (mem_domain_user_admins id_dvHost id_sDomainUser)
     (mem_domain_user_admins id_ecHost id_bqDomainUser)
     (mem_domain_user_admins id_ejHost id_bqDomainUser)
     (mem_domain_user_admins id_eqHost id_oDomainUser)
     (mem_domain_user_admins id_exHost id_buDomainUser)
     (mem_hosts id_aDomain id_byHost)
     (mem_hosts id_aDomain id_cfHost)
     (mem_hosts id_aDomain id_cmHost)
     (mem_hosts id_aDomain id_ctHost)
     (mem_hosts id_aDomain id_daHost)
     (mem_hosts id_aDomain id_dhHost)
     (mem_hosts id_aDomain id_doHost)
     (mem_hosts id_aDomain id_dvHost)
     (mem_hosts id_aDomain id_ecHost)
     (mem_hosts id_aDomain id_ejHost)
     (mem_hosts id_aDomain id_eqHost)
     (mem_hosts id_aDomain id_exHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_byHost yes)
     (prop_dc id_cfHost no)
     (prop_dc id_cmHost no)
     (prop_dc id_ctHost yes)
     (prop_dc id_daHost yes)
     (prop_dc id_dhHost no)
     (prop_dc id_doHost no)
     (prop_dc id_dvHost no)
     (prop_dc id_ecHost no)
     (prop_dc id_ejHost no)
     (prop_dc id_eqHost no)
     (prop_dc id_exHost no)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_byHost str__cd)
     (prop_dns_domain_name id_cfHost str__ck)
     (prop_dns_domain_name id_cmHost str__cr)
     (prop_dns_domain_name id_ctHost str__cy)
     (prop_dns_domain_name id_daHost str__df)
     (prop_dns_domain_name id_dhHost str__dm)
     (prop_dns_domain_name id_doHost str__dt)
     (prop_dns_domain_name id_dvHost str__ea)
     (prop_dns_domain_name id_ecHost str__eh)
     (prop_dns_domain_name id_ejHost str__eo)
     (prop_dns_domain_name id_eqHost str__ev)
     (prop_dns_domain_name id_exHost str__fc)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byHost id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_cfHost id_aDomain)
     (prop_domain id_cmHost id_aDomain)
     (prop_domain id_ctHost id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_daHost id_aDomain)
     (prop_domain id_dhHost id_aDomain)
     (prop_domain id_doHost id_aDomain)
     (prop_domain id_dvHost id_aDomain)
     (prop_domain id_ecHost id_aDomain)
     (prop_domain id_ejHost id_aDomain)
     (prop_domain id_eqHost id_aDomain)
     (prop_domain id_exHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_feRat yes)
     (prop_executable id_feRat str__ff)
     (prop_fqdn id_byHost str__ce)
     (prop_fqdn id_cfHost str__cl)
     (prop_fqdn id_cmHost str__cs)
     (prop_fqdn id_ctHost str__cz)
     (prop_fqdn id_daHost str__dg)
     (prop_fqdn id_dhHost str__dn)
     (prop_fqdn id_doHost str__du)
     (prop_fqdn id_dvHost str__eb)
     (prop_fqdn id_ecHost str__ei)
     (prop_fqdn id_ejHost str__ep)
     (prop_fqdn id_eqHost str__ew)
     (prop_fqdn id_exHost str__fd)
     (prop_host id_bzTimeDelta id_byHost)
     (prop_host id_cgTimeDelta id_cfHost)
     (prop_host id_cnTimeDelta id_cmHost)
     (prop_host id_cuTimeDelta id_ctHost)
     (prop_host id_dbTimeDelta id_daHost)
     (prop_host id_diTimeDelta id_dhHost)
     (prop_host id_dpTimeDelta id_doHost)
     (prop_host id_dwTimeDelta id_dvHost)
     (prop_host id_edTimeDelta id_ecHost)
     (prop_host id_ekTimeDelta id_ejHost)
     (prop_host id_erTimeDelta id_eqHost)
     (prop_host id_eyTimeDelta id_exHost)
     (prop_host id_feRat id_ctHost)
     (prop_hostname id_byHost str__cc)
     (prop_hostname id_cfHost str__cj)
     (prop_hostname id_cmHost str__cq)
     (prop_hostname id_ctHost str__cx)
     (prop_hostname id_daHost str__de)
     (prop_hostname id_dhHost str__dl)
     (prop_hostname id_doHost str__ds)
     (prop_hostname id_dvHost str__dz)
     (prop_hostname id_ecHost str__eg)
     (prop_hostname id_ejHost str__en)
     (prop_hostname id_eqHost str__eu)
     (prop_hostname id_exHost str__fb)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_bzTimeDelta num__53)
     (prop_microseconds id_cgTimeDelta num__60)
     (prop_microseconds id_cnTimeDelta num__67)
     (prop_microseconds id_cuTimeDelta num__74)
     (prop_microseconds id_dbTimeDelta num__81)
     (prop_microseconds id_diTimeDelta num__88)
     (prop_microseconds id_dpTimeDelta num__95)
     (prop_microseconds id_dwTimeDelta num__102)
     (prop_microseconds id_edTimeDelta num__109)
     (prop_microseconds id_ekTimeDelta num__116)
     (prop_microseconds id_erTimeDelta num__123)
     (prop_microseconds id_eyTimeDelta num__130)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_bzTimeDelta num__52)
     (prop_seconds id_cgTimeDelta num__59)
     (prop_seconds id_cnTimeDelta num__66)
     (prop_seconds id_cuTimeDelta num__73)
     (prop_seconds id_dbTimeDelta num__80)
     (prop_seconds id_diTimeDelta num__87)
     (prop_seconds id_dpTimeDelta num__94)
     (prop_seconds id_dwTimeDelta num__101)
     (prop_seconds id_edTimeDelta num__108)
     (prop_seconds id_ekTimeDelta num__115)
     (prop_seconds id_erTimeDelta num__122)
     (prop_seconds id_eyTimeDelta num__129)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_byHost id_bzTimeDelta)
     (prop_timedelta id_cfHost id_cgTimeDelta)
     (prop_timedelta id_cmHost id_cnTimeDelta)
     (prop_timedelta id_ctHost id_cuTimeDelta)
     (prop_timedelta id_daHost id_dbTimeDelta)
     (prop_timedelta id_dhHost id_diTimeDelta)
     (prop_timedelta id_doHost id_dpTimeDelta)
     (prop_timedelta id_dvHost id_dwTimeDelta)
     (prop_timedelta id_ecHost id_edTimeDelta)
     (prop_timedelta id_ejHost id_ekTimeDelta)
     (prop_timedelta id_eqHost id_erTimeDelta)
     (prop_timedelta id_exHost id_eyTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_cDomainUser str__james)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_fxRat id_ejHost)
     (prop_host id_fwRat id_dvHost)
     (prop_host id_fuRat id_exHost)
     (prop_host id_ftRat id_daHost)
     (prop_host id_fzRat id_cmHost)
     (prop_host id_gcRat id_cfHost)
     (prop_host id_gdRat id_byHost)
     (prop_host id_gbRat id_ecHost)
     (prop_host id_fsRat id_eqHost)
     (prop_host id_fvRat id_dhHost)
     (prop_host id_fyRat id_doHost)
)
)
)