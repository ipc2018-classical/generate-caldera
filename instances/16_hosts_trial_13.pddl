;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 16 hosts, 32 user, 2 admin per host, 3 active account per host
(define (problem p16_hosts_trial_13)
(:domain caldera)
(:objects id_hmTimeDelta id_fwTimeDelta id_hfTimeDelta id_iaTimeDelta id_gdTimeDelta id_ioTimeDelta id_fiTimeDelta id_fpTimeDelta id_ivTimeDelta id_jcTimeDelta id_grTimeDelta id_gkTimeDelta id_ihTimeDelta id_fbTimeDelta id_gyTimeDelta id_htTimeDelta - ObservedTimeDelta 
 id_ltRat id_lmRat id_lpRat id_lqRat id_lrRat id_loRat id_lgRat id_lvRat id_lhRat id_jiRat id_lkRat id_liRat id_llRat id_lsRat id_ljRat id_lnRat id_luRat - ObservedRat 
 id_aDomain - ObservedDomain 
 id_bfDomainCredential id_cxDomainCredential id_etDomainCredential id_pDomainCredential id_bvDomainCredential id_tDomainCredential id_bjDomainCredential id_elDomainCredential id_dDomainCredential id_epDomainCredential id_hDomainCredential id_cdDomainCredential id_clDomainCredential id_cpDomainCredential id_ctDomainCredential id_edDomainCredential id_dfDomainCredential id_xDomainCredential id_ehDomainCredential id_chDomainCredential id_bzDomainCredential id_bnDomainCredential id_dzDomainCredential id_djDomainCredential id_lDomainCredential id_dnDomainCredential id_dvDomainCredential id_brDomainCredential id_dbDomainCredential id_bbDomainCredential id_exDomainCredential id_drDomainCredential - ObservedDomainCredential 
 id_sDomainUser id_kDomainUser id_csDomainUser id_biDomainUser id_egDomainUser id_ekDomainUser id_oDomainUser id_deDomainUser id_duDomainUser id_dmDomainUser id_eoDomainUser id_baDomainUser id_ecDomainUser id_daDomainUser id_coDomainUser id_cgDomainUser id_cwDomainUser id_dyDomainUser id_ccDomainUser id_ewDomainUser id_dqDomainUser id_bqDomainUser id_cDomainUser id_buDomainUser id_diDomainUser id_ckDomainUser id_wDomainUser id_esDomainUser id_beDomainUser id_bmDomainUser id_byDomainUser id_gDomainUser - ObservedDomainUser 
 id_kqFile id_krFile id_lbFile id_kvFile id_ksFile id_kuFile id_leFile id_laFile id_kyFile id_ktFile id_ldFile id_lfFile id_kwFile id_kzFile id_lcFile id_kxFile - ObservedFile 
 id_kjShare id_kiShare id_kmShare id_koShare id_kdShare id_khShare id_klShare id_kkShare id_kbShare id_keShare id_kfShare id_kcShare id_kgShare id_kpShare id_knShare id_kaShare - ObservedShare 
 id_jwSchtask id_jkSchtask id_jqSchtask id_jlSchtask id_joSchtask id_jmSchtask id_jrSchtask id_jtSchtask id_jpSchtask id_jsSchtask id_jvSchtask id_jxSchtask id_jnSchtask id_juSchtask id_jzSchtask id_jySchtask - ObservedSchtask 
 num__154 num__238 num__175 num__210 num__139 num__160 num__188 num__217 num__147 num__182 num__209 num__189 num__146 num__168 num__203 num__153 num__161 num__202 num__132 num__195 num__223 num__216 num__224 num__231 num__167 num__133 num__181 num__237 num__174 num__196 num__230 num__140 - num 
 id_gjHost id_hzHost id_heHost id_fvHost id_foHost id_faHost id_jbHost id_hsHost id_inHost id_igHost id_iuHost id_gqHost id_hlHost id_fhHost id_gxHost id_gcHost - ObservedHost 
 str__ci str__v str__b str__dorothy str__hj str__r str__dl str__david str__hb str__hp str__bx str__cz str__fe str__bs str__elizabeth str__dd str__linda str__jj str__james str__hq str__if str__z str__bd str__dw str__alpha str__ev str__gb str__hx str__ik str__dp str__cq str__william str__helen str__ir str__hd str__jf str__hi str__e str__jh str__bw str__f str__lisa str__ee str__donald str__bo str__iz str__ft str__eb str__paul str__jg str__bk str__mary str__ez str__cy str__ie str__hk str__im str__richard str__it str__mark str__hr str__fl str__fu str__gn str__michael str__bt str__barbara str__m str__dc str__fg str__go str__gi str__christopher str__j str__gh str__is str__bh str__ca str__cr str__hw str__bl str__patricia str__gw str__cb str__eu str__karen str__ef str__bg str__ej str__sandra str__gg str__cf str__cv str__daniel str__fm str__y str__gp str__gv str__cm str__jennifer str__joseph str__en str__ey str__susan str__margaret str__ff str__cn str__i str__u str__iy str__fn str__ea str__dx str__dt str__betty str__charles str__cu str__ds str__do str__id str__maria str__ga str__q str__ja str__hy str__robert str__fs str__hc str__dh str__dg str__ce str__n str__bc str__eq str__nancy str__il str__em str__fz str__er str__gu str__thomas str__dk str__john str__cj str__ei str__george str__bp - string 
)
(:init     (knows id_heHost)
     (knows id_jiRat)
     (knows_property id_heHost pfqdn)
     (knows_property id_jiRat pexecutable)
     (knows_property id_jiRat phost)
     (mem_cached_domain_creds id_faHost id_bfDomainCredential)
     (mem_cached_domain_creds id_faHost id_drDomainCredential)
     (mem_cached_domain_creds id_faHost id_lDomainCredential)
     (mem_cached_domain_creds id_fhHost id_bjDomainCredential)
     (mem_cached_domain_creds id_fhHost id_dfDomainCredential)
     (mem_cached_domain_creds id_fhHost id_ehDomainCredential)
     (mem_cached_domain_creds id_foHost id_djDomainCredential)
     (mem_cached_domain_creds id_foHost id_pDomainCredential)
     (mem_cached_domain_creds id_foHost id_tDomainCredential)
     (mem_cached_domain_creds id_fvHost id_cpDomainCredential)
     (mem_cached_domain_creds id_fvHost id_dDomainCredential)
     (mem_cached_domain_creds id_fvHost id_dnDomainCredential)
     (mem_cached_domain_creds id_gcHost id_ctDomainCredential)
     (mem_cached_domain_creds id_gcHost id_cxDomainCredential)
     (mem_cached_domain_creds id_gcHost id_elDomainCredential)
     (mem_cached_domain_creds id_gjHost id_bvDomainCredential)
     (mem_cached_domain_creds id_gjHost id_cdDomainCredential)
     (mem_cached_domain_creds id_gjHost id_clDomainCredential)
     (mem_cached_domain_creds id_gqHost id_drDomainCredential)
     (mem_cached_domain_creds id_gqHost id_edDomainCredential)
     (mem_cached_domain_creds id_gqHost id_pDomainCredential)
     (mem_cached_domain_creds id_gxHost id_ctDomainCredential)
     (mem_cached_domain_creds id_gxHost id_djDomainCredential)
     (mem_cached_domain_creds id_gxHost id_lDomainCredential)
     (mem_cached_domain_creds id_heHost id_brDomainCredential)
     (mem_cached_domain_creds id_heHost id_ehDomainCredential)
     (mem_cached_domain_creds id_heHost id_etDomainCredential)
     (mem_cached_domain_creds id_hlHost id_chDomainCredential)
     (mem_cached_domain_creds id_hlHost id_dDomainCredential)
     (mem_cached_domain_creds id_hlHost id_dfDomainCredential)
     (mem_cached_domain_creds id_hsHost id_clDomainCredential)
     (mem_cached_domain_creds id_hsHost id_cpDomainCredential)
     (mem_cached_domain_creds id_hsHost id_hDomainCredential)
     (mem_cached_domain_creds id_hzHost id_bvDomainCredential)
     (mem_cached_domain_creds id_hzHost id_chDomainCredential)
     (mem_cached_domain_creds id_hzHost id_ehDomainCredential)
     (mem_cached_domain_creds id_igHost id_brDomainCredential)
     (mem_cached_domain_creds id_igHost id_cxDomainCredential)
     (mem_cached_domain_creds id_igHost id_tDomainCredential)
     (mem_cached_domain_creds id_inHost id_clDomainCredential)
     (mem_cached_domain_creds id_inHost id_dfDomainCredential)
     (mem_cached_domain_creds id_inHost id_dvDomainCredential)
     (mem_cached_domain_creds id_iuHost id_bvDomainCredential)
     (mem_cached_domain_creds id_iuHost id_clDomainCredential)
     (mem_cached_domain_creds id_iuHost id_hDomainCredential)
     (mem_cached_domain_creds id_jbHost id_chDomainCredential)
     (mem_cached_domain_creds id_jbHost id_epDomainCredential)
     (mem_cached_domain_creds id_jbHost id_etDomainCredential)
     (mem_domain_user_admins id_faHost id_gDomainUser)
     (mem_domain_user_admins id_faHost id_wDomainUser)
     (mem_domain_user_admins id_fhHost id_cwDomainUser)
     (mem_domain_user_admins id_fhHost id_esDomainUser)
     (mem_domain_user_admins id_foHost id_cgDomainUser)
     (mem_domain_user_admins id_foHost id_dyDomainUser)
     (mem_domain_user_admins id_fvHost id_byDomainUser)
     (mem_domain_user_admins id_fvHost id_dqDomainUser)
     (mem_domain_user_admins id_gcHost id_ccDomainUser)
     (mem_domain_user_admins id_gcHost id_esDomainUser)
     (mem_domain_user_admins id_gjHost id_beDomainUser)
     (mem_domain_user_admins id_gjHost id_eoDomainUser)
     (mem_domain_user_admins id_gqHost id_cwDomainUser)
     (mem_domain_user_admins id_gqHost id_eoDomainUser)
     (mem_domain_user_admins id_gxHost id_egDomainUser)
     (mem_domain_user_admins id_gxHost id_esDomainUser)
     (mem_domain_user_admins id_heHost id_cwDomainUser)
     (mem_domain_user_admins id_heHost id_eoDomainUser)
     (mem_domain_user_admins id_hlHost id_esDomainUser)
     (mem_domain_user_admins id_hlHost id_kDomainUser)
     (mem_domain_user_admins id_hsHost id_cgDomainUser)
     (mem_domain_user_admins id_hsHost id_eoDomainUser)
     (mem_domain_user_admins id_hzHost id_buDomainUser)
     (mem_domain_user_admins id_hzHost id_coDomainUser)
     (mem_domain_user_admins id_igHost id_cgDomainUser)
     (mem_domain_user_admins id_igHost id_dqDomainUser)
     (mem_domain_user_admins id_inHost id_daDomainUser)
     (mem_domain_user_admins id_inHost id_sDomainUser)
     (mem_domain_user_admins id_iuHost id_byDomainUser)
     (mem_domain_user_admins id_iuHost id_dqDomainUser)
     (mem_domain_user_admins id_jbHost id_bmDomainUser)
     (mem_domain_user_admins id_jbHost id_cwDomainUser)
     (mem_hosts id_aDomain id_faHost)
     (mem_hosts id_aDomain id_fhHost)
     (mem_hosts id_aDomain id_foHost)
     (mem_hosts id_aDomain id_fvHost)
     (mem_hosts id_aDomain id_gcHost)
     (mem_hosts id_aDomain id_gjHost)
     (mem_hosts id_aDomain id_gqHost)
     (mem_hosts id_aDomain id_gxHost)
     (mem_hosts id_aDomain id_heHost)
     (mem_hosts id_aDomain id_hlHost)
     (mem_hosts id_aDomain id_hsHost)
     (mem_hosts id_aDomain id_hzHost)
     (mem_hosts id_aDomain id_igHost)
     (mem_hosts id_aDomain id_inHost)
     (mem_hosts id_aDomain id_iuHost)
     (mem_hosts id_aDomain id_jbHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_byDomainUser id_bzDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_ccDomainUser id_cdDomainCredential)
     (prop_cred id_cgDomainUser id_chDomainCredential)
     (prop_cred id_ckDomainUser id_clDomainCredential)
     (prop_cred id_coDomainUser id_cpDomainCredential)
     (prop_cred id_csDomainUser id_ctDomainCredential)
     (prop_cred id_cwDomainUser id_cxDomainCredential)
     (prop_cred id_daDomainUser id_dbDomainCredential)
     (prop_cred id_deDomainUser id_dfDomainCredential)
     (prop_cred id_diDomainUser id_djDomainCredential)
     (prop_cred id_dmDomainUser id_dnDomainCredential)
     (prop_cred id_dqDomainUser id_drDomainCredential)
     (prop_cred id_duDomainUser id_dvDomainCredential)
     (prop_cred id_dyDomainUser id_dzDomainCredential)
     (prop_cred id_ecDomainUser id_edDomainCredential)
     (prop_cred id_egDomainUser id_ehDomainCredential)
     (prop_cred id_ekDomainUser id_elDomainCredential)
     (prop_cred id_eoDomainUser id_epDomainCredential)
     (prop_cred id_esDomainUser id_etDomainCredential)
     (prop_cred id_ewDomainUser id_exDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_faHost no)
     (prop_dc id_fhHost no)
     (prop_dc id_foHost no)
     (prop_dc id_fvHost no)
     (prop_dc id_gcHost no)
     (prop_dc id_gjHost no)
     (prop_dc id_gqHost no)
     (prop_dc id_gxHost no)
     (prop_dc id_heHost no)
     (prop_dc id_hlHost no)
     (prop_dc id_hsHost no)
     (prop_dc id_hzHost no)
     (prop_dc id_igHost no)
     (prop_dc id_inHost no)
     (prop_dc id_iuHost yes)
     (prop_dc id_jbHost no)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_faHost str__fg)
     (prop_dns_domain_name id_fhHost str__fn)
     (prop_dns_domain_name id_foHost str__fu)
     (prop_dns_domain_name id_fvHost str__gb)
     (prop_dns_domain_name id_gcHost str__gi)
     (prop_dns_domain_name id_gjHost str__gp)
     (prop_dns_domain_name id_gqHost str__gw)
     (prop_dns_domain_name id_gxHost str__hd)
     (prop_dns_domain_name id_heHost str__hk)
     (prop_dns_domain_name id_hlHost str__hr)
     (prop_dns_domain_name id_hsHost str__hy)
     (prop_dns_domain_name id_hzHost str__if)
     (prop_dns_domain_name id_igHost str__im)
     (prop_dns_domain_name id_inHost str__it)
     (prop_dns_domain_name id_iuHost str__ja)
     (prop_dns_domain_name id_jbHost str__jh)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byDomainUser id_aDomain)
     (prop_domain id_bzDomainCredential id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_ccDomainUser id_aDomain)
     (prop_domain id_cdDomainCredential id_aDomain)
     (prop_domain id_cgDomainUser id_aDomain)
     (prop_domain id_chDomainCredential id_aDomain)
     (prop_domain id_ckDomainUser id_aDomain)
     (prop_domain id_clDomainCredential id_aDomain)
     (prop_domain id_coDomainUser id_aDomain)
     (prop_domain id_cpDomainCredential id_aDomain)
     (prop_domain id_csDomainUser id_aDomain)
     (prop_domain id_ctDomainCredential id_aDomain)
     (prop_domain id_cwDomainUser id_aDomain)
     (prop_domain id_cxDomainCredential id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_daDomainUser id_aDomain)
     (prop_domain id_dbDomainCredential id_aDomain)
     (prop_domain id_deDomainUser id_aDomain)
     (prop_domain id_dfDomainCredential id_aDomain)
     (prop_domain id_diDomainUser id_aDomain)
     (prop_domain id_djDomainCredential id_aDomain)
     (prop_domain id_dmDomainUser id_aDomain)
     (prop_domain id_dnDomainCredential id_aDomain)
     (prop_domain id_dqDomainUser id_aDomain)
     (prop_domain id_drDomainCredential id_aDomain)
     (prop_domain id_duDomainUser id_aDomain)
     (prop_domain id_dvDomainCredential id_aDomain)
     (prop_domain id_dyDomainUser id_aDomain)
     (prop_domain id_dzDomainCredential id_aDomain)
     (prop_domain id_ecDomainUser id_aDomain)
     (prop_domain id_edDomainCredential id_aDomain)
     (prop_domain id_egDomainUser id_aDomain)
     (prop_domain id_ehDomainCredential id_aDomain)
     (prop_domain id_ekDomainUser id_aDomain)
     (prop_domain id_elDomainCredential id_aDomain)
     (prop_domain id_eoDomainUser id_aDomain)
     (prop_domain id_epDomainCredential id_aDomain)
     (prop_domain id_esDomainUser id_aDomain)
     (prop_domain id_etDomainCredential id_aDomain)
     (prop_domain id_ewDomainUser id_aDomain)
     (prop_domain id_exDomainCredential id_aDomain)
     (prop_domain id_faHost id_aDomain)
     (prop_domain id_fhHost id_aDomain)
     (prop_domain id_foHost id_aDomain)
     (prop_domain id_fvHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_gcHost id_aDomain)
     (prop_domain id_gjHost id_aDomain)
     (prop_domain id_gqHost id_aDomain)
     (prop_domain id_gxHost id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_heHost id_aDomain)
     (prop_domain id_hlHost id_aDomain)
     (prop_domain id_hsHost id_aDomain)
     (prop_domain id_hzHost id_aDomain)
     (prop_domain id_igHost id_aDomain)
     (prop_domain id_inHost id_aDomain)
     (prop_domain id_iuHost id_aDomain)
     (prop_domain id_jbHost id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_jiRat yes)
     (prop_executable id_jiRat str__jj)
     (prop_fqdn id_faHost str__ff)
     (prop_fqdn id_fhHost str__fm)
     (prop_fqdn id_foHost str__ft)
     (prop_fqdn id_fvHost str__ga)
     (prop_fqdn id_gcHost str__gh)
     (prop_fqdn id_gjHost str__go)
     (prop_fqdn id_gqHost str__gv)
     (prop_fqdn id_gxHost str__hc)
     (prop_fqdn id_heHost str__hj)
     (prop_fqdn id_hlHost str__hq)
     (prop_fqdn id_hsHost str__hx)
     (prop_fqdn id_hzHost str__ie)
     (prop_fqdn id_igHost str__il)
     (prop_fqdn id_inHost str__is)
     (prop_fqdn id_iuHost str__iz)
     (prop_fqdn id_jbHost str__jg)
     (prop_host id_fbTimeDelta id_faHost)
     (prop_host id_fiTimeDelta id_fhHost)
     (prop_host id_fpTimeDelta id_foHost)
     (prop_host id_fwTimeDelta id_fvHost)
     (prop_host id_gdTimeDelta id_gcHost)
     (prop_host id_gkTimeDelta id_gjHost)
     (prop_host id_grTimeDelta id_gqHost)
     (prop_host id_gyTimeDelta id_gxHost)
     (prop_host id_hfTimeDelta id_heHost)
     (prop_host id_hmTimeDelta id_hlHost)
     (prop_host id_htTimeDelta id_hsHost)
     (prop_host id_iaTimeDelta id_hzHost)
     (prop_host id_ihTimeDelta id_igHost)
     (prop_host id_ioTimeDelta id_inHost)
     (prop_host id_ivTimeDelta id_iuHost)
     (prop_host id_jcTimeDelta id_jbHost)
     (prop_host id_jiRat id_heHost)
     (prop_hostname id_faHost str__fe)
     (prop_hostname id_fhHost str__fl)
     (prop_hostname id_foHost str__fs)
     (prop_hostname id_fvHost str__fz)
     (prop_hostname id_gcHost str__gg)
     (prop_hostname id_gjHost str__gn)
     (prop_hostname id_gqHost str__gu)
     (prop_hostname id_gxHost str__hb)
     (prop_hostname id_heHost str__hi)
     (prop_hostname id_hlHost str__hp)
     (prop_hostname id_hsHost str__hw)
     (prop_hostname id_hzHost str__id)
     (prop_hostname id_igHost str__ik)
     (prop_hostname id_inHost str__ir)
     (prop_hostname id_iuHost str__iy)
     (prop_hostname id_jbHost str__jf)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_byDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_ccDomainUser no)
     (prop_is_group id_cgDomainUser no)
     (prop_is_group id_ckDomainUser no)
     (prop_is_group id_coDomainUser no)
     (prop_is_group id_csDomainUser no)
     (prop_is_group id_cwDomainUser no)
     (prop_is_group id_daDomainUser no)
     (prop_is_group id_deDomainUser no)
     (prop_is_group id_diDomainUser no)
     (prop_is_group id_dmDomainUser no)
     (prop_is_group id_dqDomainUser no)
     (prop_is_group id_duDomainUser no)
     (prop_is_group id_dyDomainUser no)
     (prop_is_group id_ecDomainUser no)
     (prop_is_group id_egDomainUser no)
     (prop_is_group id_ekDomainUser no)
     (prop_is_group id_eoDomainUser no)
     (prop_is_group id_esDomainUser no)
     (prop_is_group id_ewDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_fbTimeDelta num__133)
     (prop_microseconds id_fiTimeDelta num__140)
     (prop_microseconds id_fpTimeDelta num__147)
     (prop_microseconds id_fwTimeDelta num__154)
     (prop_microseconds id_gdTimeDelta num__161)
     (prop_microseconds id_gkTimeDelta num__168)
     (prop_microseconds id_grTimeDelta num__175)
     (prop_microseconds id_gyTimeDelta num__182)
     (prop_microseconds id_hfTimeDelta num__189)
     (prop_microseconds id_hmTimeDelta num__196)
     (prop_microseconds id_htTimeDelta num__203)
     (prop_microseconds id_iaTimeDelta num__210)
     (prop_microseconds id_ihTimeDelta num__217)
     (prop_microseconds id_ioTimeDelta num__224)
     (prop_microseconds id_ivTimeDelta num__231)
     (prop_microseconds id_jcTimeDelta num__238)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_bzDomainCredential str__ca)
     (prop_password id_cdDomainCredential str__ce)
     (prop_password id_chDomainCredential str__ci)
     (prop_password id_clDomainCredential str__cm)
     (prop_password id_cpDomainCredential str__cq)
     (prop_password id_ctDomainCredential str__cu)
     (prop_password id_cxDomainCredential str__cy)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_dbDomainCredential str__dc)
     (prop_password id_dfDomainCredential str__dg)
     (prop_password id_djDomainCredential str__dk)
     (prop_password id_dnDomainCredential str__do)
     (prop_password id_drDomainCredential str__ds)
     (prop_password id_dvDomainCredential str__dw)
     (prop_password id_dzDomainCredential str__ea)
     (prop_password id_edDomainCredential str__ee)
     (prop_password id_ehDomainCredential str__ei)
     (prop_password id_elDomainCredential str__em)
     (prop_password id_epDomainCredential str__eq)
     (prop_password id_etDomainCredential str__eu)
     (prop_password id_exDomainCredential str__ey)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_fbTimeDelta num__132)
     (prop_seconds id_fiTimeDelta num__139)
     (prop_seconds id_fpTimeDelta num__146)
     (prop_seconds id_fwTimeDelta num__153)
     (prop_seconds id_gdTimeDelta num__160)
     (prop_seconds id_gkTimeDelta num__167)
     (prop_seconds id_grTimeDelta num__174)
     (prop_seconds id_gyTimeDelta num__181)
     (prop_seconds id_hfTimeDelta num__188)
     (prop_seconds id_hmTimeDelta num__195)
     (prop_seconds id_htTimeDelta num__202)
     (prop_seconds id_iaTimeDelta num__209)
     (prop_seconds id_ihTimeDelta num__216)
     (prop_seconds id_ioTimeDelta num__223)
     (prop_seconds id_ivTimeDelta num__230)
     (prop_seconds id_jcTimeDelta num__237)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_byDomainUser str__cb)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_ccDomainUser str__cf)
     (prop_sid id_cgDomainUser str__cj)
     (prop_sid id_ckDomainUser str__cn)
     (prop_sid id_coDomainUser str__cr)
     (prop_sid id_csDomainUser str__cv)
     (prop_sid id_cwDomainUser str__cz)
     (prop_sid id_daDomainUser str__dd)
     (prop_sid id_deDomainUser str__dh)
     (prop_sid id_diDomainUser str__dl)
     (prop_sid id_dmDomainUser str__dp)
     (prop_sid id_dqDomainUser str__dt)
     (prop_sid id_duDomainUser str__dx)
     (prop_sid id_dyDomainUser str__eb)
     (prop_sid id_ecDomainUser str__ef)
     (prop_sid id_egDomainUser str__ej)
     (prop_sid id_ekDomainUser str__en)
     (prop_sid id_eoDomainUser str__er)
     (prop_sid id_esDomainUser str__ev)
     (prop_sid id_ewDomainUser str__ez)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_faHost id_fbTimeDelta)
     (prop_timedelta id_fhHost id_fiTimeDelta)
     (prop_timedelta id_foHost id_fpTimeDelta)
     (prop_timedelta id_fvHost id_fwTimeDelta)
     (prop_timedelta id_gcHost id_gdTimeDelta)
     (prop_timedelta id_gjHost id_gkTimeDelta)
     (prop_timedelta id_gqHost id_grTimeDelta)
     (prop_timedelta id_gxHost id_gyTimeDelta)
     (prop_timedelta id_heHost id_hfTimeDelta)
     (prop_timedelta id_hlHost id_hmTimeDelta)
     (prop_timedelta id_hsHost id_htTimeDelta)
     (prop_timedelta id_hzHost id_iaTimeDelta)
     (prop_timedelta id_igHost id_ihTimeDelta)
     (prop_timedelta id_inHost id_ioTimeDelta)
     (prop_timedelta id_iuHost id_ivTimeDelta)
     (prop_timedelta id_jbHost id_jcTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_bzDomainCredential id_byDomainUser)
     (prop_user id_cdDomainCredential id_ccDomainUser)
     (prop_user id_chDomainCredential id_cgDomainUser)
     (prop_user id_clDomainCredential id_ckDomainUser)
     (prop_user id_cpDomainCredential id_coDomainUser)
     (prop_user id_ctDomainCredential id_csDomainUser)
     (prop_user id_cxDomainCredential id_cwDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_dbDomainCredential id_daDomainUser)
     (prop_user id_dfDomainCredential id_deDomainUser)
     (prop_user id_djDomainCredential id_diDomainUser)
     (prop_user id_dnDomainCredential id_dmDomainUser)
     (prop_user id_drDomainCredential id_dqDomainUser)
     (prop_user id_dvDomainCredential id_duDomainUser)
     (prop_user id_dzDomainCredential id_dyDomainUser)
     (prop_user id_edDomainCredential id_ecDomainUser)
     (prop_user id_ehDomainCredential id_egDomainUser)
     (prop_user id_elDomainCredential id_ekDomainUser)
     (prop_user id_epDomainCredential id_eoDomainUser)
     (prop_user id_etDomainCredential id_esDomainUser)
     (prop_user id_exDomainCredential id_ewDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_byDomainUser str__richard)
     (prop_username id_cDomainUser str__james)
     (prop_username id_ccDomainUser str__maria)
     (prop_username id_cgDomainUser str__charles)
     (prop_username id_ckDomainUser str__susan)
     (prop_username id_coDomainUser str__joseph)
     (prop_username id_csDomainUser str__margaret)
     (prop_username id_cwDomainUser str__thomas)
     (prop_username id_daDomainUser str__dorothy)
     (prop_username id_deDomainUser str__christopher)
     (prop_username id_diDomainUser str__lisa)
     (prop_username id_dmDomainUser str__daniel)
     (prop_username id_dqDomainUser str__nancy)
     (prop_username id_duDomainUser str__paul)
     (prop_username id_dyDomainUser str__karen)
     (prop_username id_ecDomainUser str__mark)
     (prop_username id_egDomainUser str__betty)
     (prop_username id_ekDomainUser str__donald)
     (prop_username id_eoDomainUser str__helen)
     (prop_username id_esDomainUser str__george)
     (prop_username id_ewDomainUser str__sandra)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_luRat id_gxHost)
     (prop_host id_ltRat id_gjHost)
     (prop_host id_lpRat id_hzHost)
     (prop_host id_lhRat id_fvHost)
     (prop_host id_llRat id_foHost)
     (prop_host id_lsRat id_faHost)
     (prop_host id_ljRat id_jbHost)
     (prop_host id_lnRat id_igHost)
     (prop_host id_lmRat id_iuHost)
     (prop_host id_lqRat id_hlHost)
     (prop_host id_lrRat id_fhHost)
     (prop_host id_lgRat id_gcHost)
     (prop_host id_lvRat id_hsHost)
     (prop_host id_lkRat id_inHost)
     (prop_host id_liRat id_gqHost)
)
)
)