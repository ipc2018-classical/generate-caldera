;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 16 hosts, 13 user, 3 admin per host, 5 active account per host
(define (problem p16_hosts_trial_8)
(:domain caldera)
(:objects id_guSchtask id_gwSchtask id_gmSchtask id_gxSchtask id_gzSchtask id_haSchtask id_gvSchtask id_gySchtask id_gqSchtask id_grSchtask id_hbSchtask id_gpSchtask id_goSchtask id_gtSchtask id_gnSchtask id_gsSchtask - ObservedSchtask 
 id_fjTimeDelta id_ckTimeDelta id_dmTimeDelta id_crTimeDelta id_eaTimeDelta id_ehTimeDelta id_dfTimeDelta id_fqTimeDelta id_dtTimeDelta id_evTimeDelta id_eoTimeDelta id_fcTimeDelta id_cdTimeDelta id_geTimeDelta id_fxTimeDelta id_cyTimeDelta - ObservedTimeDelta 
 id_aDomain - ObservedDomain 
 id_baDomainUser id_wDomainUser id_gDomainUser id_buDomainUser id_bmDomainUser id_byDomainUser id_cDomainUser id_kDomainUser id_biDomainUser id_bqDomainUser id_sDomainUser id_beDomainUser id_oDomainUser - ObservedDomainUser 
 id_hnShare id_hiShare id_hcShare id_heShare id_hdShare id_hmShare id_hlShare id_hjShare id_hfShare id_hgShare id_hqShare id_hoShare id_hrShare id_hhShare id_hpShare id_hkShare - ObservedShare 
 str__bh str__ef str__john str__bd str__dq str__ch str__mary str__cn str__ez str__bt str__fn str__el str__v str__richard str__cu str__e str__y str__fv str__dp str__z str__dy str__f str__bo str__dx str__bc str__cw str__linda str__ca str__ed str__james str__dd str__em str__fo str__bs str__dj str__fu str__david str__cv str__ee str__ey str__i str__ff str__dc str__patricia str__michael str__alpha str__co str__william str__u str__j str__ek str__cp str__fg str__cg str__gl str__jennifer str__fh str__fa str__elizabeth str__fm str__bl str__gb str__gc str__q str__dr str__m str__cb str__ft str__n str__bp str__db str__gh str__er str__et str__bk str__r str__bx str__ci str__ga str__b str__di str__gj str__dw str__robert str__es str__bg str__dk str__bw str__gi str__barbara - string 
 id_bzDomainCredential id_bfDomainCredential id_brDomainCredential id_xDomainCredential id_hDomainCredential id_bvDomainCredential id_bjDomainCredential id_dDomainCredential id_tDomainCredential id_bnDomainCredential id_pDomainCredential id_bbDomainCredential id_lDomainCredential - ObservedDomainCredential 
 num__106 num__113 num__140 num__134 num__112 num__92 num__64 num__126 num__119 num__63 num__147 num__70 num__154 num__99 num__85 num__71 num__78 num__133 num__161 num__162 num__56 num__120 num__105 num__91 num__77 num__155 num__127 num__98 num__84 num__57 num__141 num__148 - num 
 id_hzRat id_iaRat id_gkRat id_ieRat id_hvRat id_icRat id_ifRat id_hsRat id_htRat id_ihRat id_huRat id_hyRat id_ibRat id_hxRat id_idRat id_hwRat id_igRat - ObservedRat 
 id_ikFile id_iqFile id_iuFile id_ixFile id_ijFile id_inFile id_imFile id_ivFile id_iwFile id_ilFile id_itFile id_ioFile id_iiFile id_ipFile id_irFile id_isFile - ObservedFile 
 id_ccHost id_dlHost id_fiHost id_dsHost id_fpHost id_euHost id_fwHost id_egHost id_cxHost id_cqHost id_fbHost id_dzHost id_cjHost id_enHost id_gdHost id_deHost - ObservedHost 
)
(:init     (knows id_cqHost)
     (knows id_gkRat)
     (knows_property id_cqHost pfqdn)
     (knows_property id_gkRat pexecutable)
     (knows_property id_gkRat phost)
     (mem_cached_domain_creds id_ccHost id_bbDomainCredential)
     (mem_cached_domain_creds id_ccHost id_bfDomainCredential)
     (mem_cached_domain_creds id_ccHost id_bvDomainCredential)
     (mem_cached_domain_creds id_ccHost id_dDomainCredential)
     (mem_cached_domain_creds id_ccHost id_xDomainCredential)
     (mem_cached_domain_creds id_cjHost id_bnDomainCredential)
     (mem_cached_domain_creds id_cjHost id_bvDomainCredential)
     (mem_cached_domain_creds id_cjHost id_dDomainCredential)
     (mem_cached_domain_creds id_cjHost id_hDomainCredential)
     (mem_cached_domain_creds id_cjHost id_pDomainCredential)
     (mem_cached_domain_creds id_cqHost id_bbDomainCredential)
     (mem_cached_domain_creds id_cqHost id_bnDomainCredential)
     (mem_cached_domain_creds id_cqHost id_bvDomainCredential)
     (mem_cached_domain_creds id_cqHost id_lDomainCredential)
     (mem_cached_domain_creds id_cqHost id_tDomainCredential)
     (mem_cached_domain_creds id_cxHost id_bbDomainCredential)
     (mem_cached_domain_creds id_cxHost id_dDomainCredential)
     (mem_cached_domain_creds id_cxHost id_lDomainCredential)
     (mem_cached_domain_creds id_cxHost id_pDomainCredential)
     (mem_cached_domain_creds id_cxHost id_xDomainCredential)
     (mem_cached_domain_creds id_deHost id_bfDomainCredential)
     (mem_cached_domain_creds id_deHost id_bnDomainCredential)
     (mem_cached_domain_creds id_deHost id_brDomainCredential)
     (mem_cached_domain_creds id_deHost id_bvDomainCredential)
     (mem_cached_domain_creds id_deHost id_lDomainCredential)
     (mem_cached_domain_creds id_dlHost id_bfDomainCredential)
     (mem_cached_domain_creds id_dlHost id_bzDomainCredential)
     (mem_cached_domain_creds id_dlHost id_hDomainCredential)
     (mem_cached_domain_creds id_dlHost id_lDomainCredential)
     (mem_cached_domain_creds id_dlHost id_pDomainCredential)
     (mem_cached_domain_creds id_dsHost id_bbDomainCredential)
     (mem_cached_domain_creds id_dsHost id_bnDomainCredential)
     (mem_cached_domain_creds id_dsHost id_bzDomainCredential)
     (mem_cached_domain_creds id_dsHost id_dDomainCredential)
     (mem_cached_domain_creds id_dsHost id_pDomainCredential)
     (mem_cached_domain_creds id_dzHost id_bfDomainCredential)
     (mem_cached_domain_creds id_dzHost id_bnDomainCredential)
     (mem_cached_domain_creds id_dzHost id_bvDomainCredential)
     (mem_cached_domain_creds id_dzHost id_dDomainCredential)
     (mem_cached_domain_creds id_dzHost id_tDomainCredential)
     (mem_cached_domain_creds id_egHost id_bnDomainCredential)
     (mem_cached_domain_creds id_egHost id_brDomainCredential)
     (mem_cached_domain_creds id_egHost id_bzDomainCredential)
     (mem_cached_domain_creds id_egHost id_hDomainCredential)
     (mem_cached_domain_creds id_egHost id_xDomainCredential)
     (mem_cached_domain_creds id_enHost id_bfDomainCredential)
     (mem_cached_domain_creds id_enHost id_bjDomainCredential)
     (mem_cached_domain_creds id_enHost id_dDomainCredential)
     (mem_cached_domain_creds id_enHost id_lDomainCredential)
     (mem_cached_domain_creds id_enHost id_xDomainCredential)
     (mem_cached_domain_creds id_euHost id_bbDomainCredential)
     (mem_cached_domain_creds id_euHost id_bjDomainCredential)
     (mem_cached_domain_creds id_euHost id_bnDomainCredential)
     (mem_cached_domain_creds id_euHost id_lDomainCredential)
     (mem_cached_domain_creds id_euHost id_pDomainCredential)
     (mem_cached_domain_creds id_fbHost id_bfDomainCredential)
     (mem_cached_domain_creds id_fbHost id_bzDomainCredential)
     (mem_cached_domain_creds id_fbHost id_hDomainCredential)
     (mem_cached_domain_creds id_fbHost id_lDomainCredential)
     (mem_cached_domain_creds id_fbHost id_tDomainCredential)
     (mem_cached_domain_creds id_fiHost id_bnDomainCredential)
     (mem_cached_domain_creds id_fiHost id_bvDomainCredential)
     (mem_cached_domain_creds id_fiHost id_dDomainCredential)
     (mem_cached_domain_creds id_fiHost id_hDomainCredential)
     (mem_cached_domain_creds id_fiHost id_pDomainCredential)
     (mem_cached_domain_creds id_fpHost id_brDomainCredential)
     (mem_cached_domain_creds id_fpHost id_bvDomainCredential)
     (mem_cached_domain_creds id_fpHost id_dDomainCredential)
     (mem_cached_domain_creds id_fpHost id_lDomainCredential)
     (mem_cached_domain_creds id_fpHost id_pDomainCredential)
     (mem_cached_domain_creds id_fwHost id_bbDomainCredential)
     (mem_cached_domain_creds id_fwHost id_bvDomainCredential)
     (mem_cached_domain_creds id_fwHost id_dDomainCredential)
     (mem_cached_domain_creds id_fwHost id_hDomainCredential)
     (mem_cached_domain_creds id_fwHost id_xDomainCredential)
     (mem_cached_domain_creds id_gdHost id_bbDomainCredential)
     (mem_cached_domain_creds id_gdHost id_bfDomainCredential)
     (mem_cached_domain_creds id_gdHost id_bnDomainCredential)
     (mem_cached_domain_creds id_gdHost id_lDomainCredential)
     (mem_cached_domain_creds id_gdHost id_xDomainCredential)
     (mem_domain_user_admins id_ccHost id_cDomainUser)
     (mem_domain_user_admins id_ccHost id_gDomainUser)
     (mem_domain_user_admins id_ccHost id_oDomainUser)
     (mem_domain_user_admins id_cjHost id_bqDomainUser)
     (mem_domain_user_admins id_cjHost id_gDomainUser)
     (mem_domain_user_admins id_cjHost id_wDomainUser)
     (mem_domain_user_admins id_cqHost id_bmDomainUser)
     (mem_domain_user_admins id_cqHost id_byDomainUser)
     (mem_domain_user_admins id_cqHost id_oDomainUser)
     (mem_domain_user_admins id_cxHost id_buDomainUser)
     (mem_domain_user_admins id_cxHost id_byDomainUser)
     (mem_domain_user_admins id_cxHost id_cDomainUser)
     (mem_domain_user_admins id_deHost id_beDomainUser)
     (mem_domain_user_admins id_deHost id_gDomainUser)
     (mem_domain_user_admins id_deHost id_oDomainUser)
     (mem_domain_user_admins id_dlHost id_bmDomainUser)
     (mem_domain_user_admins id_dlHost id_byDomainUser)
     (mem_domain_user_admins id_dlHost id_wDomainUser)
     (mem_domain_user_admins id_dsHost id_biDomainUser)
     (mem_domain_user_admins id_dsHost id_bmDomainUser)
     (mem_domain_user_admins id_dsHost id_oDomainUser)
     (mem_domain_user_admins id_dzHost id_beDomainUser)
     (mem_domain_user_admins id_dzHost id_biDomainUser)
     (mem_domain_user_admins id_dzHost id_cDomainUser)
     (mem_domain_user_admins id_egHost id_baDomainUser)
     (mem_domain_user_admins id_egHost id_buDomainUser)
     (mem_domain_user_admins id_egHost id_gDomainUser)
     (mem_domain_user_admins id_enHost id_beDomainUser)
     (mem_domain_user_admins id_enHost id_oDomainUser)
     (mem_domain_user_admins id_enHost id_sDomainUser)
     (mem_domain_user_admins id_euHost id_baDomainUser)
     (mem_domain_user_admins id_euHost id_byDomainUser)
     (mem_domain_user_admins id_euHost id_kDomainUser)
     (mem_domain_user_admins id_fbHost id_baDomainUser)
     (mem_domain_user_admins id_fbHost id_bqDomainUser)
     (mem_domain_user_admins id_fbHost id_gDomainUser)
     (mem_domain_user_admins id_fiHost id_bmDomainUser)
     (mem_domain_user_admins id_fiHost id_oDomainUser)
     (mem_domain_user_admins id_fiHost id_wDomainUser)
     (mem_domain_user_admins id_fpHost id_buDomainUser)
     (mem_domain_user_admins id_fpHost id_kDomainUser)
     (mem_domain_user_admins id_fpHost id_oDomainUser)
     (mem_domain_user_admins id_fwHost id_gDomainUser)
     (mem_domain_user_admins id_fwHost id_oDomainUser)
     (mem_domain_user_admins id_fwHost id_wDomainUser)
     (mem_domain_user_admins id_gdHost id_cDomainUser)
     (mem_domain_user_admins id_gdHost id_kDomainUser)
     (mem_domain_user_admins id_gdHost id_sDomainUser)
     (mem_hosts id_aDomain id_ccHost)
     (mem_hosts id_aDomain id_cjHost)
     (mem_hosts id_aDomain id_cqHost)
     (mem_hosts id_aDomain id_cxHost)
     (mem_hosts id_aDomain id_deHost)
     (mem_hosts id_aDomain id_dlHost)
     (mem_hosts id_aDomain id_dsHost)
     (mem_hosts id_aDomain id_dzHost)
     (mem_hosts id_aDomain id_egHost)
     (mem_hosts id_aDomain id_enHost)
     (mem_hosts id_aDomain id_euHost)
     (mem_hosts id_aDomain id_fbHost)
     (mem_hosts id_aDomain id_fiHost)
     (mem_hosts id_aDomain id_fpHost)
     (mem_hosts id_aDomain id_fwHost)
     (mem_hosts id_aDomain id_gdHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_byDomainUser id_bzDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_ccHost no)
     (prop_dc id_cjHost no)
     (prop_dc id_cqHost no)
     (prop_dc id_cxHost no)
     (prop_dc id_deHost no)
     (prop_dc id_dlHost no)
     (prop_dc id_dsHost no)
     (prop_dc id_dzHost no)
     (prop_dc id_egHost no)
     (prop_dc id_enHost no)
     (prop_dc id_euHost yes)
     (prop_dc id_fbHost no)
     (prop_dc id_fiHost no)
     (prop_dc id_fpHost no)
     (prop_dc id_fwHost no)
     (prop_dc id_gdHost no)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_ccHost str__cg)
     (prop_dns_domain_name id_cjHost str__cn)
     (prop_dns_domain_name id_cqHost str__cu)
     (prop_dns_domain_name id_cxHost str__db)
     (prop_dns_domain_name id_deHost str__di)
     (prop_dns_domain_name id_dlHost str__dp)
     (prop_dns_domain_name id_dsHost str__dw)
     (prop_dns_domain_name id_dzHost str__ed)
     (prop_dns_domain_name id_egHost str__ek)
     (prop_dns_domain_name id_enHost str__er)
     (prop_dns_domain_name id_euHost str__ey)
     (prop_dns_domain_name id_fbHost str__ff)
     (prop_dns_domain_name id_fiHost str__fm)
     (prop_dns_domain_name id_fpHost str__ft)
     (prop_dns_domain_name id_fwHost str__ga)
     (prop_dns_domain_name id_gdHost str__gh)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byDomainUser id_aDomain)
     (prop_domain id_bzDomainCredential id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_ccHost id_aDomain)
     (prop_domain id_cjHost id_aDomain)
     (prop_domain id_cqHost id_aDomain)
     (prop_domain id_cxHost id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_deHost id_aDomain)
     (prop_domain id_dlHost id_aDomain)
     (prop_domain id_dsHost id_aDomain)
     (prop_domain id_dzHost id_aDomain)
     (prop_domain id_egHost id_aDomain)
     (prop_domain id_enHost id_aDomain)
     (prop_domain id_euHost id_aDomain)
     (prop_domain id_fbHost id_aDomain)
     (prop_domain id_fiHost id_aDomain)
     (prop_domain id_fpHost id_aDomain)
     (prop_domain id_fwHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_gdHost id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_gkRat yes)
     (prop_executable id_gkRat str__gl)
     (prop_fqdn id_ccHost str__ci)
     (prop_fqdn id_cjHost str__cp)
     (prop_fqdn id_cqHost str__cw)
     (prop_fqdn id_cxHost str__dd)
     (prop_fqdn id_deHost str__dk)
     (prop_fqdn id_dlHost str__dr)
     (prop_fqdn id_dsHost str__dy)
     (prop_fqdn id_dzHost str__ef)
     (prop_fqdn id_egHost str__em)
     (prop_fqdn id_enHost str__et)
     (prop_fqdn id_euHost str__fa)
     (prop_fqdn id_fbHost str__fh)
     (prop_fqdn id_fiHost str__fo)
     (prop_fqdn id_fpHost str__fv)
     (prop_fqdn id_fwHost str__gc)
     (prop_fqdn id_gdHost str__gj)
     (prop_host id_cdTimeDelta id_ccHost)
     (prop_host id_ckTimeDelta id_cjHost)
     (prop_host id_crTimeDelta id_cqHost)
     (prop_host id_cyTimeDelta id_cxHost)
     (prop_host id_dfTimeDelta id_deHost)
     (prop_host id_dmTimeDelta id_dlHost)
     (prop_host id_dtTimeDelta id_dsHost)
     (prop_host id_eaTimeDelta id_dzHost)
     (prop_host id_ehTimeDelta id_egHost)
     (prop_host id_eoTimeDelta id_enHost)
     (prop_host id_evTimeDelta id_euHost)
     (prop_host id_fcTimeDelta id_fbHost)
     (prop_host id_fjTimeDelta id_fiHost)
     (prop_host id_fqTimeDelta id_fpHost)
     (prop_host id_fxTimeDelta id_fwHost)
     (prop_host id_geTimeDelta id_gdHost)
     (prop_host id_gkRat id_cqHost)
     (prop_hostname id_ccHost str__ch)
     (prop_hostname id_cjHost str__co)
     (prop_hostname id_cqHost str__cv)
     (prop_hostname id_cxHost str__dc)
     (prop_hostname id_deHost str__dj)
     (prop_hostname id_dlHost str__dq)
     (prop_hostname id_dsHost str__dx)
     (prop_hostname id_dzHost str__ee)
     (prop_hostname id_egHost str__el)
     (prop_hostname id_enHost str__es)
     (prop_hostname id_euHost str__ez)
     (prop_hostname id_fbHost str__fg)
     (prop_hostname id_fiHost str__fn)
     (prop_hostname id_fpHost str__fu)
     (prop_hostname id_fwHost str__gb)
     (prop_hostname id_gdHost str__gi)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_byDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_cdTimeDelta num__56)
     (prop_microseconds id_ckTimeDelta num__63)
     (prop_microseconds id_crTimeDelta num__70)
     (prop_microseconds id_cyTimeDelta num__77)
     (prop_microseconds id_dfTimeDelta num__84)
     (prop_microseconds id_dmTimeDelta num__91)
     (prop_microseconds id_dtTimeDelta num__98)
     (prop_microseconds id_eaTimeDelta num__105)
     (prop_microseconds id_ehTimeDelta num__112)
     (prop_microseconds id_eoTimeDelta num__119)
     (prop_microseconds id_evTimeDelta num__126)
     (prop_microseconds id_fcTimeDelta num__133)
     (prop_microseconds id_fjTimeDelta num__140)
     (prop_microseconds id_fqTimeDelta num__147)
     (prop_microseconds id_fxTimeDelta num__154)
     (prop_microseconds id_geTimeDelta num__161)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_bzDomainCredential str__ca)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_cdTimeDelta num__57)
     (prop_seconds id_ckTimeDelta num__64)
     (prop_seconds id_crTimeDelta num__71)
     (prop_seconds id_cyTimeDelta num__78)
     (prop_seconds id_dfTimeDelta num__85)
     (prop_seconds id_dmTimeDelta num__92)
     (prop_seconds id_dtTimeDelta num__99)
     (prop_seconds id_eaTimeDelta num__106)
     (prop_seconds id_ehTimeDelta num__113)
     (prop_seconds id_eoTimeDelta num__120)
     (prop_seconds id_evTimeDelta num__127)
     (prop_seconds id_fcTimeDelta num__134)
     (prop_seconds id_fjTimeDelta num__141)
     (prop_seconds id_fqTimeDelta num__148)
     (prop_seconds id_fxTimeDelta num__155)
     (prop_seconds id_geTimeDelta num__162)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_byDomainUser str__cb)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_ccHost id_cdTimeDelta)
     (prop_timedelta id_cjHost id_ckTimeDelta)
     (prop_timedelta id_cqHost id_crTimeDelta)
     (prop_timedelta id_cxHost id_cyTimeDelta)
     (prop_timedelta id_deHost id_dfTimeDelta)
     (prop_timedelta id_dlHost id_dmTimeDelta)
     (prop_timedelta id_dsHost id_dtTimeDelta)
     (prop_timedelta id_dzHost id_eaTimeDelta)
     (prop_timedelta id_egHost id_ehTimeDelta)
     (prop_timedelta id_enHost id_eoTimeDelta)
     (prop_timedelta id_euHost id_evTimeDelta)
     (prop_timedelta id_fbHost id_fcTimeDelta)
     (prop_timedelta id_fiHost id_fjTimeDelta)
     (prop_timedelta id_fpHost id_fqTimeDelta)
     (prop_timedelta id_fwHost id_fxTimeDelta)
     (prop_timedelta id_gdHost id_geTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_bzDomainCredential id_byDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_byDomainUser str__richard)
     (prop_username id_cDomainUser str__james)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_igRat id_euHost)
     (prop_host id_iaRat id_fpHost)
     (prop_host id_idRat id_cxHost)
     (prop_host id_hvRat id_fbHost)
     (prop_host id_huRat id_dzHost)
     (prop_host id_hyRat id_cjHost)
     (prop_host id_ibRat id_enHost)
     (prop_host id_hxRat id_gdHost)
     (prop_host id_ieRat id_deHost)
     (prop_host id_icRat id_ccHost)
     (prop_host id_ifRat id_dlHost)
     (prop_host id_hsRat id_fiHost)
     (prop_host id_htRat id_dsHost)
     (prop_host id_hwRat id_fwHost)
     (prop_host id_ihRat id_egHost)
)
)
)