;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 16 hosts, 17 user, 3 admin per host, 1 active account per host
(define (problem p16_hosts_trial_9)
(:domain caldera)
(:objects id_tDomainCredential id_bzDomainCredential id_cpDomainCredential id_lDomainCredential id_xDomainCredential id_hDomainCredential id_bbDomainCredential id_bvDomainCredential id_dDomainCredential id_pDomainCredential id_bfDomainCredential id_bnDomainCredential id_chDomainCredential id_cdDomainCredential id_brDomainCredential id_clDomainCredential id_bjDomainCredential - ObservedDomainCredential 
 id_ifShare id_hvShare id_hwShare id_hsShare id_iaShare id_huShare id_icShare id_ibShare id_igShare id_hyShare id_htShare id_hzShare id_idShare id_ihShare id_hxShare id_ieShare - ObservedShare 
 num__108 num__73 num__115 num__171 num__122 num__157 num__101 num__79 num__86 num__170 num__128 num__121 num__143 num__177 num__87 num__150 num__129 num__163 num__80 num__100 num__178 num__164 num__72 num__149 num__93 num__94 num__114 num__107 num__156 num__136 num__135 num__142 - num 
 id_jeSchtask id_jmSchtask id_jcSchtask id_izSchtask id_iySchtask id_jfSchtask id_jiSchtask id_jgSchtask id_jnSchtask id_jjSchtask id_jaSchtask id_jkSchtask id_jlSchtask id_jbSchtask id_jhSchtask id_jdSchtask - ObservedSchtask 
 id_epHost id_eiHost id_duHost id_fdHost id_gtHost id_ebHost id_csHost id_gmHost id_czHost id_ewHost id_dgHost id_gfHost id_fkHost id_fyHost id_dnHost id_frHost - ObservedHost 
 str__ca str__bg str__cn str__cq str__v str__cb str__fb str__mary str__dt str__fw str__ds str__dz str__j str__bk str__william str__david str__q str__cf str__dd str__eh str__bd str__dr str__gd str__gj str__patricia str__y str__i str__eu str__ev str__gy str__r str__bs str__fh str__gx str__joseph str__ge str__fp str__bl str__michael str__cm str__ea str__bo str__gk str__fi str__fo str__fc str__fv str__gq str__charles str__fx str__gz str__df str__bx str__fa str__eg str__fj str__et str__dl str__n str__f str__maria str__cj str__gr str__gs str__alpha str__cw str__bt str__dm str__ci str__ef str__z str__ce str__m str__cx str__bc str__richard str__cr str__bh str__gc str__susan str__hb str__bp str__eo str__dy str__gl str__barbara str__de str__jennifer str__fq str__b str__elizabeth str__robert str__linda str__en str__dk str__bw str__cy str__james str__em str__e str__john str__u - string 
 id_flTimeDelta id_ggTimeDelta id_ecTimeDelta id_guTimeDelta id_exTimeDelta id_ejTimeDelta id_ctTimeDelta id_dvTimeDelta id_daTimeDelta id_eqTimeDelta id_dhTimeDelta id_fsTimeDelta id_feTimeDelta id_gnTimeDelta id_doTimeDelta id_fzTimeDelta - ObservedTimeDelta 
 id_gDomainUser id_ccDomainUser id_bqDomainUser id_byDomainUser id_buDomainUser id_wDomainUser id_oDomainUser id_cDomainUser id_ckDomainUser id_baDomainUser id_cgDomainUser id_beDomainUser id_bmDomainUser id_sDomainUser id_kDomainUser id_biDomainUser id_coDomainUser - ObservedDomainUser 
 id_itFile id_imFile id_ikFile id_iiFile id_isFile id_iuFile id_irFile id_ilFile id_ipFile id_iqFile id_inFile id_ijFile id_ivFile id_ixFile id_iwFile id_ioFile - ObservedFile 
 id_aDomain - ObservedDomain 
 id_hiRat id_hpRat id_hcRat id_hkRat id_hfRat id_heRat id_haRat id_hmRat id_hoRat id_hnRat id_hgRat id_hlRat id_hjRat id_hrRat id_hhRat id_hqRat id_hdRat - ObservedRat 
)
(:init     (knows id_csHost)
     (knows id_haRat)
     (knows_property id_csHost pfqdn)
     (knows_property id_haRat pexecutable)
     (knows_property id_haRat phost)
     (mem_cached_domain_creds id_csHost id_lDomainCredential)
     (mem_cached_domain_creds id_czHost id_cpDomainCredential)
     (mem_cached_domain_creds id_dgHost id_cdDomainCredential)
     (mem_cached_domain_creds id_dnHost id_xDomainCredential)
     (mem_cached_domain_creds id_duHost id_xDomainCredential)
     (mem_cached_domain_creds id_ebHost id_bvDomainCredential)
     (mem_cached_domain_creds id_eiHost id_bbDomainCredential)
     (mem_cached_domain_creds id_epHost id_bvDomainCredential)
     (mem_cached_domain_creds id_ewHost id_tDomainCredential)
     (mem_cached_domain_creds id_fdHost id_tDomainCredential)
     (mem_cached_domain_creds id_fkHost id_clDomainCredential)
     (mem_cached_domain_creds id_frHost id_chDomainCredential)
     (mem_cached_domain_creds id_fyHost id_bnDomainCredential)
     (mem_cached_domain_creds id_gfHost id_tDomainCredential)
     (mem_cached_domain_creds id_gmHost id_dDomainCredential)
     (mem_cached_domain_creds id_gtHost id_lDomainCredential)
     (mem_domain_user_admins id_csHost id_biDomainUser)
     (mem_domain_user_admins id_csHost id_bmDomainUser)
     (mem_domain_user_admins id_csHost id_wDomainUser)
     (mem_domain_user_admins id_czHost id_buDomainUser)
     (mem_domain_user_admins id_czHost id_cgDomainUser)
     (mem_domain_user_admins id_czHost id_coDomainUser)
     (mem_domain_user_admins id_dgHost id_coDomainUser)
     (mem_domain_user_admins id_dgHost id_gDomainUser)
     (mem_domain_user_admins id_dgHost id_kDomainUser)
     (mem_domain_user_admins id_dnHost id_beDomainUser)
     (mem_domain_user_admins id_dnHost id_buDomainUser)
     (mem_domain_user_admins id_dnHost id_cgDomainUser)
     (mem_domain_user_admins id_duHost id_baDomainUser)
     (mem_domain_user_admins id_duHost id_cgDomainUser)
     (mem_domain_user_admins id_duHost id_oDomainUser)
     (mem_domain_user_admins id_ebHost id_buDomainUser)
     (mem_domain_user_admins id_ebHost id_coDomainUser)
     (mem_domain_user_admins id_ebHost id_wDomainUser)
     (mem_domain_user_admins id_eiHost id_cgDomainUser)
     (mem_domain_user_admins id_eiHost id_coDomainUser)
     (mem_domain_user_admins id_eiHost id_gDomainUser)
     (mem_domain_user_admins id_epHost id_biDomainUser)
     (mem_domain_user_admins id_epHost id_coDomainUser)
     (mem_domain_user_admins id_epHost id_gDomainUser)
     (mem_domain_user_admins id_ewHost id_biDomainUser)
     (mem_domain_user_admins id_ewHost id_buDomainUser)
     (mem_domain_user_admins id_ewHost id_cgDomainUser)
     (mem_domain_user_admins id_fdHost id_byDomainUser)
     (mem_domain_user_admins id_fdHost id_cDomainUser)
     (mem_domain_user_admins id_fdHost id_kDomainUser)
     (mem_domain_user_admins id_fkHost id_bmDomainUser)
     (mem_domain_user_admins id_fkHost id_byDomainUser)
     (mem_domain_user_admins id_fkHost id_coDomainUser)
     (mem_domain_user_admins id_frHost id_buDomainUser)
     (mem_domain_user_admins id_frHost id_cDomainUser)
     (mem_domain_user_admins id_frHost id_oDomainUser)
     (mem_domain_user_admins id_fyHost id_baDomainUser)
     (mem_domain_user_admins id_fyHost id_cDomainUser)
     (mem_domain_user_admins id_fyHost id_gDomainUser)
     (mem_domain_user_admins id_gfHost id_beDomainUser)
     (mem_domain_user_admins id_gfHost id_buDomainUser)
     (mem_domain_user_admins id_gfHost id_sDomainUser)
     (mem_domain_user_admins id_gmHost id_baDomainUser)
     (mem_domain_user_admins id_gmHost id_byDomainUser)
     (mem_domain_user_admins id_gmHost id_ccDomainUser)
     (mem_domain_user_admins id_gtHost id_ccDomainUser)
     (mem_domain_user_admins id_gtHost id_cgDomainUser)
     (mem_domain_user_admins id_gtHost id_ckDomainUser)
     (mem_hosts id_aDomain id_csHost)
     (mem_hosts id_aDomain id_czHost)
     (mem_hosts id_aDomain id_dgHost)
     (mem_hosts id_aDomain id_dnHost)
     (mem_hosts id_aDomain id_duHost)
     (mem_hosts id_aDomain id_ebHost)
     (mem_hosts id_aDomain id_eiHost)
     (mem_hosts id_aDomain id_epHost)
     (mem_hosts id_aDomain id_ewHost)
     (mem_hosts id_aDomain id_fdHost)
     (mem_hosts id_aDomain id_fkHost)
     (mem_hosts id_aDomain id_frHost)
     (mem_hosts id_aDomain id_fyHost)
     (mem_hosts id_aDomain id_gfHost)
     (mem_hosts id_aDomain id_gmHost)
     (mem_hosts id_aDomain id_gtHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_byDomainUser id_bzDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_ccDomainUser id_cdDomainCredential)
     (prop_cred id_cgDomainUser id_chDomainCredential)
     (prop_cred id_ckDomainUser id_clDomainCredential)
     (prop_cred id_coDomainUser id_cpDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_csHost no)
     (prop_dc id_czHost no)
     (prop_dc id_dgHost no)
     (prop_dc id_dnHost no)
     (prop_dc id_duHost no)
     (prop_dc id_ebHost no)
     (prop_dc id_eiHost yes)
     (prop_dc id_epHost no)
     (prop_dc id_ewHost no)
     (prop_dc id_fdHost no)
     (prop_dc id_fkHost no)
     (prop_dc id_frHost no)
     (prop_dc id_fyHost no)
     (prop_dc id_gfHost yes)
     (prop_dc id_gmHost no)
     (prop_dc id_gtHost no)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_csHost str__cy)
     (prop_dns_domain_name id_czHost str__df)
     (prop_dns_domain_name id_dgHost str__dm)
     (prop_dns_domain_name id_dnHost str__dt)
     (prop_dns_domain_name id_duHost str__ea)
     (prop_dns_domain_name id_ebHost str__eh)
     (prop_dns_domain_name id_eiHost str__eo)
     (prop_dns_domain_name id_epHost str__ev)
     (prop_dns_domain_name id_ewHost str__fc)
     (prop_dns_domain_name id_fdHost str__fj)
     (prop_dns_domain_name id_fkHost str__fq)
     (prop_dns_domain_name id_frHost str__fx)
     (prop_dns_domain_name id_fyHost str__ge)
     (prop_dns_domain_name id_gfHost str__gl)
     (prop_dns_domain_name id_gmHost str__gs)
     (prop_dns_domain_name id_gtHost str__gz)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byDomainUser id_aDomain)
     (prop_domain id_bzDomainCredential id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_ccDomainUser id_aDomain)
     (prop_domain id_cdDomainCredential id_aDomain)
     (prop_domain id_cgDomainUser id_aDomain)
     (prop_domain id_chDomainCredential id_aDomain)
     (prop_domain id_ckDomainUser id_aDomain)
     (prop_domain id_clDomainCredential id_aDomain)
     (prop_domain id_coDomainUser id_aDomain)
     (prop_domain id_cpDomainCredential id_aDomain)
     (prop_domain id_csHost id_aDomain)
     (prop_domain id_czHost id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_dgHost id_aDomain)
     (prop_domain id_dnHost id_aDomain)
     (prop_domain id_duHost id_aDomain)
     (prop_domain id_ebHost id_aDomain)
     (prop_domain id_eiHost id_aDomain)
     (prop_domain id_epHost id_aDomain)
     (prop_domain id_ewHost id_aDomain)
     (prop_domain id_fdHost id_aDomain)
     (prop_domain id_fkHost id_aDomain)
     (prop_domain id_frHost id_aDomain)
     (prop_domain id_fyHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_gfHost id_aDomain)
     (prop_domain id_gmHost id_aDomain)
     (prop_domain id_gtHost id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_haRat yes)
     (prop_executable id_haRat str__hb)
     (prop_fqdn id_csHost str__cx)
     (prop_fqdn id_czHost str__de)
     (prop_fqdn id_dgHost str__dl)
     (prop_fqdn id_dnHost str__ds)
     (prop_fqdn id_duHost str__dz)
     (prop_fqdn id_ebHost str__eg)
     (prop_fqdn id_eiHost str__en)
     (prop_fqdn id_epHost str__eu)
     (prop_fqdn id_ewHost str__fb)
     (prop_fqdn id_fdHost str__fi)
     (prop_fqdn id_fkHost str__fp)
     (prop_fqdn id_frHost str__fw)
     (prop_fqdn id_fyHost str__gd)
     (prop_fqdn id_gfHost str__gk)
     (prop_fqdn id_gmHost str__gr)
     (prop_fqdn id_gtHost str__gy)
     (prop_host id_ctTimeDelta id_csHost)
     (prop_host id_daTimeDelta id_czHost)
     (prop_host id_dhTimeDelta id_dgHost)
     (prop_host id_doTimeDelta id_dnHost)
     (prop_host id_dvTimeDelta id_duHost)
     (prop_host id_ecTimeDelta id_ebHost)
     (prop_host id_ejTimeDelta id_eiHost)
     (prop_host id_eqTimeDelta id_epHost)
     (prop_host id_exTimeDelta id_ewHost)
     (prop_host id_feTimeDelta id_fdHost)
     (prop_host id_flTimeDelta id_fkHost)
     (prop_host id_fsTimeDelta id_frHost)
     (prop_host id_fzTimeDelta id_fyHost)
     (prop_host id_ggTimeDelta id_gfHost)
     (prop_host id_gnTimeDelta id_gmHost)
     (prop_host id_guTimeDelta id_gtHost)
     (prop_host id_haRat id_csHost)
     (prop_hostname id_csHost str__cw)
     (prop_hostname id_czHost str__dd)
     (prop_hostname id_dgHost str__dk)
     (prop_hostname id_dnHost str__dr)
     (prop_hostname id_duHost str__dy)
     (prop_hostname id_ebHost str__ef)
     (prop_hostname id_eiHost str__em)
     (prop_hostname id_epHost str__et)
     (prop_hostname id_ewHost str__fa)
     (prop_hostname id_fdHost str__fh)
     (prop_hostname id_fkHost str__fo)
     (prop_hostname id_frHost str__fv)
     (prop_hostname id_fyHost str__gc)
     (prop_hostname id_gfHost str__gj)
     (prop_hostname id_gmHost str__gq)
     (prop_hostname id_gtHost str__gx)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_byDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_ccDomainUser no)
     (prop_is_group id_cgDomainUser no)
     (prop_is_group id_ckDomainUser no)
     (prop_is_group id_coDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_ctTimeDelta num__73)
     (prop_microseconds id_daTimeDelta num__80)
     (prop_microseconds id_dhTimeDelta num__87)
     (prop_microseconds id_doTimeDelta num__94)
     (prop_microseconds id_dvTimeDelta num__101)
     (prop_microseconds id_ecTimeDelta num__108)
     (prop_microseconds id_ejTimeDelta num__115)
     (prop_microseconds id_eqTimeDelta num__122)
     (prop_microseconds id_exTimeDelta num__129)
     (prop_microseconds id_feTimeDelta num__136)
     (prop_microseconds id_flTimeDelta num__143)
     (prop_microseconds id_fsTimeDelta num__150)
     (prop_microseconds id_fzTimeDelta num__157)
     (prop_microseconds id_ggTimeDelta num__164)
     (prop_microseconds id_gnTimeDelta num__171)
     (prop_microseconds id_guTimeDelta num__178)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_bzDomainCredential str__ca)
     (prop_password id_cdDomainCredential str__ce)
     (prop_password id_chDomainCredential str__ci)
     (prop_password id_clDomainCredential str__cm)
     (prop_password id_cpDomainCredential str__cq)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_ctTimeDelta num__72)
     (prop_seconds id_daTimeDelta num__79)
     (prop_seconds id_dhTimeDelta num__86)
     (prop_seconds id_doTimeDelta num__93)
     (prop_seconds id_dvTimeDelta num__100)
     (prop_seconds id_ecTimeDelta num__107)
     (prop_seconds id_ejTimeDelta num__114)
     (prop_seconds id_eqTimeDelta num__121)
     (prop_seconds id_exTimeDelta num__128)
     (prop_seconds id_feTimeDelta num__135)
     (prop_seconds id_flTimeDelta num__142)
     (prop_seconds id_fsTimeDelta num__149)
     (prop_seconds id_fzTimeDelta num__156)
     (prop_seconds id_ggTimeDelta num__163)
     (prop_seconds id_gnTimeDelta num__170)
     (prop_seconds id_guTimeDelta num__177)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_byDomainUser str__cb)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_ccDomainUser str__cf)
     (prop_sid id_cgDomainUser str__cj)
     (prop_sid id_ckDomainUser str__cn)
     (prop_sid id_coDomainUser str__cr)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_csHost id_ctTimeDelta)
     (prop_timedelta id_czHost id_daTimeDelta)
     (prop_timedelta id_dgHost id_dhTimeDelta)
     (prop_timedelta id_dnHost id_doTimeDelta)
     (prop_timedelta id_duHost id_dvTimeDelta)
     (prop_timedelta id_ebHost id_ecTimeDelta)
     (prop_timedelta id_eiHost id_ejTimeDelta)
     (prop_timedelta id_epHost id_eqTimeDelta)
     (prop_timedelta id_ewHost id_exTimeDelta)
     (prop_timedelta id_fdHost id_feTimeDelta)
     (prop_timedelta id_fkHost id_flTimeDelta)
     (prop_timedelta id_frHost id_fsTimeDelta)
     (prop_timedelta id_fyHost id_fzTimeDelta)
     (prop_timedelta id_gfHost id_ggTimeDelta)
     (prop_timedelta id_gmHost id_gnTimeDelta)
     (prop_timedelta id_gtHost id_guTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_bzDomainCredential id_byDomainUser)
     (prop_user id_cdDomainCredential id_ccDomainUser)
     (prop_user id_chDomainCredential id_cgDomainUser)
     (prop_user id_clDomainCredential id_ckDomainUser)
     (prop_user id_cpDomainCredential id_coDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_byDomainUser str__richard)
     (prop_username id_cDomainUser str__james)
     (prop_username id_ccDomainUser str__maria)
     (prop_username id_cgDomainUser str__charles)
     (prop_username id_ckDomainUser str__susan)
     (prop_username id_coDomainUser str__joseph)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_hdRat id_fkHost)
     (prop_host id_hiRat id_duHost)
     (prop_host id_hhRat id_ebHost)
     (prop_host id_hcRat id_czHost)
     (prop_host id_hfRat id_gfHost)
     (prop_host id_hmRat id_fyHost)
     (prop_host id_hoRat id_dnHost)
     (prop_host id_hnRat id_frHost)
     (prop_host id_hjRat id_epHost)
     (prop_host id_hrRat id_eiHost)
     (prop_host id_hqRat id_gtHost)
     (prop_host id_heRat id_fdHost)
     (prop_host id_hlRat id_gmHost)
     (prop_host id_hgRat id_ewHost)
     (prop_host id_hpRat id_dgHost)
)
)
)